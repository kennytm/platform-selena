package selena.common.facade.facade;

import java.lang.System;

import selena.common.facade.Facade;
import selena.common.facade.FacadeManager;
import selena.common.facade.annotations.Command;

public class SystemFacade extends Facade {
    public SystemFacade(final FacadeManager manager) {
        super(manager);
    }

    @Override
    public void shutdown() {
    }

    @Command(ns = "system", description = "Indicates to the virtual machine that it would be a good time to run the garbage collector.")
    static public void gc() {
        System.gc();
    }

    @Command(ns = "system", description = "Gets the value of a particular system property.")
    static public String getProperty(final String name) {
        return System.getProperty(name);
    }

    @Command(ns = "system", description = "Sets the value of a particular system property.")
    static public String setProperty(final String name, final String val) {
        return System.setProperty(name, val);
    }
}
