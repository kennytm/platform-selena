// AUTO-GENERATED DOT NOT EDIT
package selena.common.math;

import ni.types.Vector2f;
import ni.types.Vector2l;
import ni.types.Vector3f;
import ni.types.Vector3l;
import ni.types.Vector4f;
import ni.types.Vector4l;

/**
 * Vector Swizzling operators.
 */
public class Swz {
    protected Swz() {}

    public static Vector2l ww(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.w; o.y = v.w;
        return o;
    }
    public static Vector2l ww(final Vector4l v) {
        return ww(null, v);
    }

    public static Vector2f ww(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.w; o.y = v.w;
        return o;
    }
    public static Vector2f ww(final Vector4f v) {
        return ww(null, v);
    }

    public static Vector2l wx(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.w; o.y = v.x;
        return o;
    }
    public static Vector2l wx(final Vector4l v) {
        return wx(null, v);
    }

    public static Vector2f wx(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.w; o.y = v.x;
        return o;
    }
    public static Vector2f wx(final Vector4f v) {
        return wx(null, v);
    }

    public static Vector2l wy(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.w; o.y = v.y;
        return o;
    }
    public static Vector2l wy(final Vector4l v) {
        return wy(null, v);
    }

    public static Vector2f wy(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.w; o.y = v.y;
        return o;
    }
    public static Vector2f wy(final Vector4f v) {
        return wy(null, v);
    }

    public static Vector2l wz(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.w; o.y = v.z;
        return o;
    }
    public static Vector2l wz(final Vector4l v) {
        return wz(null, v);
    }

    public static Vector2f wz(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.w; o.y = v.z;
        return o;
    }
    public static Vector2f wz(final Vector4f v) {
        return wz(null, v);
    }

    public static Vector2l xw(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.w;
        return o;
    }
    public static Vector2l xw(final Vector4l v) {
        return xw(null, v);
    }

    public static Vector2f xw(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.w;
        return o;
    }
    public static Vector2f xw(final Vector4f v) {
        return xw(null, v);
    }

    public static Vector2l xx(Vector2l o, final Vector2l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.x;
        return o;
    }
    public static Vector2l xx(final Vector2l v) {
        return xx(null, v);
    }

    public static Vector2l xx(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.x;
        return o;
    }
    public static Vector2l xx(final Vector3l v) {
        return xx(null, v);
    }

    public static Vector2l xx(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.x;
        return o;
    }
    public static Vector2l xx(final Vector4l v) {
        return xx(null, v);
    }

    public static Vector2f xx(Vector2f o, final Vector2f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.x;
        return o;
    }
    public static Vector2f xx(final Vector2f v) {
        return xx(null, v);
    }

    public static Vector2f xx(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.x;
        return o;
    }
    public static Vector2f xx(final Vector3f v) {
        return xx(null, v);
    }

    public static Vector2f xx(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.x;
        return o;
    }
    public static Vector2f xx(final Vector4f v) {
        return xx(null, v);
    }

    public static Vector2l xy(Vector2l o, final Vector2l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.y;
        return o;
    }
    public static Vector2l xy(final Vector2l v) {
        return xy(null, v);
    }

    public static Vector2l xy(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.y;
        return o;
    }
    public static Vector2l xy(final Vector3l v) {
        return xy(null, v);
    }

    public static Vector2l xy(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.y;
        return o;
    }
    public static Vector2l xy(final Vector4l v) {
        return xy(null, v);
    }

    public static Vector2f xy(Vector2f o, final Vector2f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.y;
        return o;
    }
    public static Vector2f xy(final Vector2f v) {
        return xy(null, v);
    }

    public static Vector2f xy(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.y;
        return o;
    }
    public static Vector2f xy(final Vector3f v) {
        return xy(null, v);
    }

    public static Vector2f xy(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.y;
        return o;
    }
    public static Vector2f xy(final Vector4f v) {
        return xy(null, v);
    }

    public static Vector2l xz(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.z;
        return o;
    }
    public static Vector2l xz(final Vector3l v) {
        return xz(null, v);
    }

    public static Vector2l xz(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.x; o.y = v.z;
        return o;
    }
    public static Vector2l xz(final Vector4l v) {
        return xz(null, v);
    }

    public static Vector2f xz(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.z;
        return o;
    }
    public static Vector2f xz(final Vector3f v) {
        return xz(null, v);
    }

    public static Vector2f xz(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.x; o.y = v.z;
        return o;
    }
    public static Vector2f xz(final Vector4f v) {
        return xz(null, v);
    }

    public static Vector2l yw(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.w;
        return o;
    }
    public static Vector2l yw(final Vector4l v) {
        return yw(null, v);
    }

    public static Vector2f yw(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.w;
        return o;
    }
    public static Vector2f yw(final Vector4f v) {
        return yw(null, v);
    }

    public static Vector2l yx(Vector2l o, final Vector2l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.x;
        return o;
    }
    public static Vector2l yx(final Vector2l v) {
        return yx(null, v);
    }

    public static Vector2l yx(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.x;
        return o;
    }
    public static Vector2l yx(final Vector3l v) {
        return yx(null, v);
    }

    public static Vector2l yx(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.x;
        return o;
    }
    public static Vector2l yx(final Vector4l v) {
        return yx(null, v);
    }

    public static Vector2f yx(Vector2f o, final Vector2f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.x;
        return o;
    }
    public static Vector2f yx(final Vector2f v) {
        return yx(null, v);
    }

    public static Vector2f yx(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.x;
        return o;
    }
    public static Vector2f yx(final Vector3f v) {
        return yx(null, v);
    }

    public static Vector2f yx(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.x;
        return o;
    }
    public static Vector2f yx(final Vector4f v) {
        return yx(null, v);
    }

    public static Vector2l yy(Vector2l o, final Vector2l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.y;
        return o;
    }
    public static Vector2l yy(final Vector2l v) {
        return yy(null, v);
    }

    public static Vector2l yy(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.y;
        return o;
    }
    public static Vector2l yy(final Vector3l v) {
        return yy(null, v);
    }

    public static Vector2l yy(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.y;
        return o;
    }
    public static Vector2l yy(final Vector4l v) {
        return yy(null, v);
    }

    public static Vector2f yy(Vector2f o, final Vector2f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.y;
        return o;
    }
    public static Vector2f yy(final Vector2f v) {
        return yy(null, v);
    }

    public static Vector2f yy(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.y;
        return o;
    }
    public static Vector2f yy(final Vector3f v) {
        return yy(null, v);
    }

    public static Vector2f yy(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.y;
        return o;
    }
    public static Vector2f yy(final Vector4f v) {
        return yy(null, v);
    }

    public static Vector2l yz(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.z;
        return o;
    }
    public static Vector2l yz(final Vector3l v) {
        return yz(null, v);
    }

    public static Vector2l yz(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.y; o.y = v.z;
        return o;
    }
    public static Vector2l yz(final Vector4l v) {
        return yz(null, v);
    }

    public static Vector2f yz(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.z;
        return o;
    }
    public static Vector2f yz(final Vector3f v) {
        return yz(null, v);
    }

    public static Vector2f yz(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.y; o.y = v.z;
        return o;
    }
    public static Vector2f yz(final Vector4f v) {
        return yz(null, v);
    }

    public static Vector2l zw(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.w;
        return o;
    }
    public static Vector2l zw(final Vector4l v) {
        return zw(null, v);
    }

    public static Vector2f zw(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.w;
        return o;
    }
    public static Vector2f zw(final Vector4f v) {
        return zw(null, v);
    }

    public static Vector2l zx(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.x;
        return o;
    }
    public static Vector2l zx(final Vector3l v) {
        return zx(null, v);
    }

    public static Vector2l zx(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.x;
        return o;
    }
    public static Vector2l zx(final Vector4l v) {
        return zx(null, v);
    }

    public static Vector2f zx(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.x;
        return o;
    }
    public static Vector2f zx(final Vector3f v) {
        return zx(null, v);
    }

    public static Vector2f zx(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.x;
        return o;
    }
    public static Vector2f zx(final Vector4f v) {
        return zx(null, v);
    }

    public static Vector2l zy(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.y;
        return o;
    }
    public static Vector2l zy(final Vector3l v) {
        return zy(null, v);
    }

    public static Vector2l zy(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.y;
        return o;
    }
    public static Vector2l zy(final Vector4l v) {
        return zy(null, v);
    }

    public static Vector2f zy(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.y;
        return o;
    }
    public static Vector2f zy(final Vector3f v) {
        return zy(null, v);
    }

    public static Vector2f zy(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.y;
        return o;
    }
    public static Vector2f zy(final Vector4f v) {
        return zy(null, v);
    }

    public static Vector2l zz(Vector2l o, final Vector3l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.z;
        return o;
    }
    public static Vector2l zz(final Vector3l v) {
        return zz(null, v);
    }

    public static Vector2l zz(Vector2l o, final Vector4l v) {
        if (o == null) { o = new Vector2l(); }
        o.x = v.z; o.y = v.z;
        return o;
    }
    public static Vector2l zz(final Vector4l v) {
        return zz(null, v);
    }

    public static Vector2f zz(Vector2f o, final Vector3f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.z;
        return o;
    }
    public static Vector2f zz(final Vector3f v) {
        return zz(null, v);
    }

    public static Vector2f zz(Vector2f o, final Vector4f v) {
        if (o == null) { o = new Vector2f(); }
        o.x = v.z; o.y = v.z;
        return o;
    }
    public static Vector2f zz(final Vector4f v) {
        return zz(null, v);
    }

    public static Vector3l xxx(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l xxx(final Vector2l v) {
        return xxx(null, v);
    }

    public static Vector3l xxx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l xxx(final Vector3l v) {
        return xxx(null, v);
    }

    public static Vector3l xxx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l xxx(final Vector4l v) {
        return xxx(null, v);
    }

    public static Vector3f xxx(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f xxx(final Vector2f v) {
        return xxx(null, v);
    }

    public static Vector3f xxx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f xxx(final Vector3f v) {
        return xxx(null, v);
    }

    public static Vector3f xxx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f xxx(final Vector4f v) {
        return xxx(null, v);
    }

    public static Vector3l xxy(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l xxy(final Vector2l v) {
        return xxy(null, v);
    }

    public static Vector3l xxy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l xxy(final Vector3l v) {
        return xxy(null, v);
    }

    public static Vector3l xxy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l xxy(final Vector4l v) {
        return xxy(null, v);
    }

    public static Vector3f xxy(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f xxy(final Vector2f v) {
        return xxy(null, v);
    }

    public static Vector3f xxy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f xxy(final Vector3f v) {
        return xxy(null, v);
    }

    public static Vector3f xxy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f xxy(final Vector4f v) {
        return xxy(null, v);
    }

    public static Vector3l xxz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3l xxz(final Vector3l v) {
        return xxz(null, v);
    }

    public static Vector3l xxz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3l xxz(final Vector4l v) {
        return xxz(null, v);
    }

    public static Vector3f xxz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3f xxz(final Vector3f v) {
        return xxz(null, v);
    }

    public static Vector3f xxz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3f xxz(final Vector4f v) {
        return xxz(null, v);
    }

    public static Vector3l xyx(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l xyx(final Vector2l v) {
        return xyx(null, v);
    }

    public static Vector3l xyx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l xyx(final Vector3l v) {
        return xyx(null, v);
    }

    public static Vector3l xyx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l xyx(final Vector4l v) {
        return xyx(null, v);
    }

    public static Vector3f xyx(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f xyx(final Vector2f v) {
        return xyx(null, v);
    }

    public static Vector3f xyx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f xyx(final Vector3f v) {
        return xyx(null, v);
    }

    public static Vector3f xyx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f xyx(final Vector4f v) {
        return xyx(null, v);
    }

    public static Vector3l xyy(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l xyy(final Vector2l v) {
        return xyy(null, v);
    }

    public static Vector3l xyy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l xyy(final Vector3l v) {
        return xyy(null, v);
    }

    public static Vector3l xyy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l xyy(final Vector4l v) {
        return xyy(null, v);
    }

    public static Vector3f xyy(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f xyy(final Vector2f v) {
        return xyy(null, v);
    }

    public static Vector3f xyy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f xyy(final Vector3f v) {
        return xyy(null, v);
    }

    public static Vector3f xyy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f xyy(final Vector4f v) {
        return xyy(null, v);
    }

    public static Vector3l xyz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3l xyz(final Vector3l v) {
        return xyz(null, v);
    }

    public static Vector3l xyz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3l xyz(final Vector4l v) {
        return xyz(null, v);
    }

    public static Vector3f xyz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3f xyz(final Vector3f v) {
        return xyz(null, v);
    }

    public static Vector3f xyz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3f xyz(final Vector4f v) {
        return xyz(null, v);
    }

    public static Vector3l xzx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3l xzx(final Vector3l v) {
        return xzx(null, v);
    }

    public static Vector3l xzx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3l xzx(final Vector4l v) {
        return xzx(null, v);
    }

    public static Vector3f xzx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3f xzx(final Vector3f v) {
        return xzx(null, v);
    }

    public static Vector3f xzx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3f xzx(final Vector4f v) {
        return xzx(null, v);
    }

    public static Vector3l xzy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3l xzy(final Vector3l v) {
        return xzy(null, v);
    }

    public static Vector3l xzy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3l xzy(final Vector4l v) {
        return xzy(null, v);
    }

    public static Vector3f xzy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3f xzy(final Vector3f v) {
        return xzy(null, v);
    }

    public static Vector3f xzy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3f xzy(final Vector4f v) {
        return xzy(null, v);
    }

    public static Vector3l xzz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3l xzz(final Vector3l v) {
        return xzz(null, v);
    }

    public static Vector3l xzz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.x; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3l xzz(final Vector4l v) {
        return xzz(null, v);
    }

    public static Vector3f xzz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3f xzz(final Vector3f v) {
        return xzz(null, v);
    }

    public static Vector3f xzz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.x; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3f xzz(final Vector4f v) {
        return xzz(null, v);
    }

    public static Vector3l yxx(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l yxx(final Vector2l v) {
        return yxx(null, v);
    }

    public static Vector3l yxx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l yxx(final Vector3l v) {
        return yxx(null, v);
    }

    public static Vector3l yxx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l yxx(final Vector4l v) {
        return yxx(null, v);
    }

    public static Vector3f yxx(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f yxx(final Vector2f v) {
        return yxx(null, v);
    }

    public static Vector3f yxx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f yxx(final Vector3f v) {
        return yxx(null, v);
    }

    public static Vector3f yxx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f yxx(final Vector4f v) {
        return yxx(null, v);
    }

    public static Vector3l yxy(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l yxy(final Vector2l v) {
        return yxy(null, v);
    }

    public static Vector3l yxy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l yxy(final Vector3l v) {
        return yxy(null, v);
    }

    public static Vector3l yxy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l yxy(final Vector4l v) {
        return yxy(null, v);
    }

    public static Vector3f yxy(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f yxy(final Vector2f v) {
        return yxy(null, v);
    }

    public static Vector3f yxy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f yxy(final Vector3f v) {
        return yxy(null, v);
    }

    public static Vector3f yxy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f yxy(final Vector4f v) {
        return yxy(null, v);
    }

    public static Vector3l yxz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3l yxz(final Vector3l v) {
        return yxz(null, v);
    }

    public static Vector3l yxz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3l yxz(final Vector4l v) {
        return yxz(null, v);
    }

    public static Vector3f yxz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3f yxz(final Vector3f v) {
        return yxz(null, v);
    }

    public static Vector3f yxz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3f yxz(final Vector4f v) {
        return yxz(null, v);
    }

    public static Vector3l yyx(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l yyx(final Vector2l v) {
        return yyx(null, v);
    }

    public static Vector3l yyx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l yyx(final Vector3l v) {
        return yyx(null, v);
    }

    public static Vector3l yyx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l yyx(final Vector4l v) {
        return yyx(null, v);
    }

    public static Vector3f yyx(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f yyx(final Vector2f v) {
        return yyx(null, v);
    }

    public static Vector3f yyx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f yyx(final Vector3f v) {
        return yyx(null, v);
    }

    public static Vector3f yyx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f yyx(final Vector4f v) {
        return yyx(null, v);
    }

    public static Vector3l yyy(Vector3l o, final Vector2l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l yyy(final Vector2l v) {
        return yyy(null, v);
    }

    public static Vector3l yyy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l yyy(final Vector3l v) {
        return yyy(null, v);
    }

    public static Vector3l yyy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l yyy(final Vector4l v) {
        return yyy(null, v);
    }

    public static Vector3f yyy(Vector3f o, final Vector2f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f yyy(final Vector2f v) {
        return yyy(null, v);
    }

    public static Vector3f yyy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f yyy(final Vector3f v) {
        return yyy(null, v);
    }

    public static Vector3f yyy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f yyy(final Vector4f v) {
        return yyy(null, v);
    }

    public static Vector3l yyz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3l yyz(final Vector3l v) {
        return yyz(null, v);
    }

    public static Vector3l yyz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3l yyz(final Vector4l v) {
        return yyz(null, v);
    }

    public static Vector3f yyz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3f yyz(final Vector3f v) {
        return yyz(null, v);
    }

    public static Vector3f yyz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3f yyz(final Vector4f v) {
        return yyz(null, v);
    }

    public static Vector3l yzx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3l yzx(final Vector3l v) {
        return yzx(null, v);
    }

    public static Vector3l yzx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3l yzx(final Vector4l v) {
        return yzx(null, v);
    }

    public static Vector3f yzx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3f yzx(final Vector3f v) {
        return yzx(null, v);
    }

    public static Vector3f yzx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3f yzx(final Vector4f v) {
        return yzx(null, v);
    }

    public static Vector3l yzy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3l yzy(final Vector3l v) {
        return yzy(null, v);
    }

    public static Vector3l yzy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3l yzy(final Vector4l v) {
        return yzy(null, v);
    }

    public static Vector3f yzy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3f yzy(final Vector3f v) {
        return yzy(null, v);
    }

    public static Vector3f yzy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3f yzy(final Vector4f v) {
        return yzy(null, v);
    }

    public static Vector3l yzz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3l yzz(final Vector3l v) {
        return yzz(null, v);
    }

    public static Vector3l yzz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.y; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3l yzz(final Vector4l v) {
        return yzz(null, v);
    }

    public static Vector3f yzz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3f yzz(final Vector3f v) {
        return yzz(null, v);
    }

    public static Vector3f yzz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.y; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3f yzz(final Vector4f v) {
        return yzz(null, v);
    }

    public static Vector3l zxx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l zxx(final Vector3l v) {
        return zxx(null, v);
    }

    public static Vector3l zxx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3l zxx(final Vector4l v) {
        return zxx(null, v);
    }

    public static Vector3f zxx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f zxx(final Vector3f v) {
        return zxx(null, v);
    }

    public static Vector3f zxx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.x; o.z = v.x;
        return o;
    }
    public static Vector3f zxx(final Vector4f v) {
        return zxx(null, v);
    }

    public static Vector3l zxy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l zxy(final Vector3l v) {
        return zxy(null, v);
    }

    public static Vector3l zxy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3l zxy(final Vector4l v) {
        return zxy(null, v);
    }

    public static Vector3f zxy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f zxy(final Vector3f v) {
        return zxy(null, v);
    }

    public static Vector3f zxy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.x; o.z = v.y;
        return o;
    }
    public static Vector3f zxy(final Vector4f v) {
        return zxy(null, v);
    }

    public static Vector3l zxz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3l zxz(final Vector3l v) {
        return zxz(null, v);
    }

    public static Vector3l zxz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3l zxz(final Vector4l v) {
        return zxz(null, v);
    }

    public static Vector3f zxz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3f zxz(final Vector3f v) {
        return zxz(null, v);
    }

    public static Vector3f zxz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.x; o.z = v.z;
        return o;
    }
    public static Vector3f zxz(final Vector4f v) {
        return zxz(null, v);
    }

    public static Vector3l zyx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l zyx(final Vector3l v) {
        return zyx(null, v);
    }

    public static Vector3l zyx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3l zyx(final Vector4l v) {
        return zyx(null, v);
    }

    public static Vector3f zyx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f zyx(final Vector3f v) {
        return zyx(null, v);
    }

    public static Vector3f zyx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.y; o.z = v.x;
        return o;
    }
    public static Vector3f zyx(final Vector4f v) {
        return zyx(null, v);
    }

    public static Vector3l zyy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l zyy(final Vector3l v) {
        return zyy(null, v);
    }

    public static Vector3l zyy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3l zyy(final Vector4l v) {
        return zyy(null, v);
    }

    public static Vector3f zyy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f zyy(final Vector3f v) {
        return zyy(null, v);
    }

    public static Vector3f zyy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.y; o.z = v.y;
        return o;
    }
    public static Vector3f zyy(final Vector4f v) {
        return zyy(null, v);
    }

    public static Vector3l zyz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3l zyz(final Vector3l v) {
        return zyz(null, v);
    }

    public static Vector3l zyz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3l zyz(final Vector4l v) {
        return zyz(null, v);
    }

    public static Vector3f zyz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3f zyz(final Vector3f v) {
        return zyz(null, v);
    }

    public static Vector3f zyz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.y; o.z = v.z;
        return o;
    }
    public static Vector3f zyz(final Vector4f v) {
        return zyz(null, v);
    }

    public static Vector3l zzx(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3l zzx(final Vector3l v) {
        return zzx(null, v);
    }

    public static Vector3l zzx(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3l zzx(final Vector4l v) {
        return zzx(null, v);
    }

    public static Vector3f zzx(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3f zzx(final Vector3f v) {
        return zzx(null, v);
    }

    public static Vector3f zzx(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.z; o.z = v.x;
        return o;
    }
    public static Vector3f zzx(final Vector4f v) {
        return zzx(null, v);
    }

    public static Vector3l zzy(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3l zzy(final Vector3l v) {
        return zzy(null, v);
    }

    public static Vector3l zzy(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3l zzy(final Vector4l v) {
        return zzy(null, v);
    }

    public static Vector3f zzy(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3f zzy(final Vector3f v) {
        return zzy(null, v);
    }

    public static Vector3f zzy(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.z; o.z = v.y;
        return o;
    }
    public static Vector3f zzy(final Vector4f v) {
        return zzy(null, v);
    }

    public static Vector3l zzz(Vector3l o, final Vector3l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3l zzz(final Vector3l v) {
        return zzz(null, v);
    }

    public static Vector3l zzz(Vector3l o, final Vector4l v) {
        if (o == null) { o = new Vector3l(); }
        o.x = v.z; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3l zzz(final Vector4l v) {
        return zzz(null, v);
    }

    public static Vector3f zzz(Vector3f o, final Vector3f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3f zzz(final Vector3f v) {
        return zzz(null, v);
    }

    public static Vector3f zzz(Vector3f o, final Vector4f v) {
        if (o == null) { o = new Vector3f(); }
        o.x = v.z; o.y = v.z; o.z = v.z;
        return o;
    }
    public static Vector3f zzz(final Vector4f v) {
        return zzz(null, v);
    }

    public static Vector4l wwww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l wwww(final Vector4l v) {
        return wwww(null, v);
    }

    public static Vector4f wwww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f wwww(final Vector4f v) {
        return wwww(null, v);
    }

    public static Vector4l wwwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l wwwx(final Vector4l v) {
        return wwwx(null, v);
    }

    public static Vector4f wwwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f wwwx(final Vector4f v) {
        return wwwx(null, v);
    }

    public static Vector4l wwwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l wwwy(final Vector4l v) {
        return wwwy(null, v);
    }

    public static Vector4f wwwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f wwwy(final Vector4f v) {
        return wwwy(null, v);
    }

    public static Vector4l wwwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l wwwz(final Vector4l v) {
        return wwwz(null, v);
    }

    public static Vector4f wwwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f wwwz(final Vector4f v) {
        return wwwz(null, v);
    }

    public static Vector4l wwxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l wwxw(final Vector4l v) {
        return wwxw(null, v);
    }

    public static Vector4f wwxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f wwxw(final Vector4f v) {
        return wwxw(null, v);
    }

    public static Vector4l wwxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l wwxx(final Vector4l v) {
        return wwxx(null, v);
    }

    public static Vector4f wwxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f wwxx(final Vector4f v) {
        return wwxx(null, v);
    }

    public static Vector4l wwxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l wwxy(final Vector4l v) {
        return wwxy(null, v);
    }

    public static Vector4f wwxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f wwxy(final Vector4f v) {
        return wwxy(null, v);
    }

    public static Vector4l wwxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l wwxz(final Vector4l v) {
        return wwxz(null, v);
    }

    public static Vector4f wwxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f wwxz(final Vector4f v) {
        return wwxz(null, v);
    }

    public static Vector4l wwyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l wwyw(final Vector4l v) {
        return wwyw(null, v);
    }

    public static Vector4f wwyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f wwyw(final Vector4f v) {
        return wwyw(null, v);
    }

    public static Vector4l wwyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l wwyx(final Vector4l v) {
        return wwyx(null, v);
    }

    public static Vector4f wwyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f wwyx(final Vector4f v) {
        return wwyx(null, v);
    }

    public static Vector4l wwyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l wwyy(final Vector4l v) {
        return wwyy(null, v);
    }

    public static Vector4f wwyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f wwyy(final Vector4f v) {
        return wwyy(null, v);
    }

    public static Vector4l wwyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l wwyz(final Vector4l v) {
        return wwyz(null, v);
    }

    public static Vector4f wwyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f wwyz(final Vector4f v) {
        return wwyz(null, v);
    }

    public static Vector4l wwzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l wwzw(final Vector4l v) {
        return wwzw(null, v);
    }

    public static Vector4f wwzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f wwzw(final Vector4f v) {
        return wwzw(null, v);
    }

    public static Vector4l wwzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l wwzx(final Vector4l v) {
        return wwzx(null, v);
    }

    public static Vector4f wwzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f wwzx(final Vector4f v) {
        return wwzx(null, v);
    }

    public static Vector4l wwzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l wwzy(final Vector4l v) {
        return wwzy(null, v);
    }

    public static Vector4f wwzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f wwzy(final Vector4f v) {
        return wwzy(null, v);
    }

    public static Vector4l wwzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l wwzz(final Vector4l v) {
        return wwzz(null, v);
    }

    public static Vector4f wwzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f wwzz(final Vector4f v) {
        return wwzz(null, v);
    }

    public static Vector4l wxww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l wxww(final Vector4l v) {
        return wxww(null, v);
    }

    public static Vector4f wxww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f wxww(final Vector4f v) {
        return wxww(null, v);
    }

    public static Vector4l wxwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l wxwx(final Vector4l v) {
        return wxwx(null, v);
    }

    public static Vector4f wxwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f wxwx(final Vector4f v) {
        return wxwx(null, v);
    }

    public static Vector4l wxwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l wxwy(final Vector4l v) {
        return wxwy(null, v);
    }

    public static Vector4f wxwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f wxwy(final Vector4f v) {
        return wxwy(null, v);
    }

    public static Vector4l wxwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l wxwz(final Vector4l v) {
        return wxwz(null, v);
    }

    public static Vector4f wxwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f wxwz(final Vector4f v) {
        return wxwz(null, v);
    }

    public static Vector4l wxxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l wxxw(final Vector4l v) {
        return wxxw(null, v);
    }

    public static Vector4f wxxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f wxxw(final Vector4f v) {
        return wxxw(null, v);
    }

    public static Vector4l wxxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l wxxx(final Vector4l v) {
        return wxxx(null, v);
    }

    public static Vector4f wxxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f wxxx(final Vector4f v) {
        return wxxx(null, v);
    }

    public static Vector4l wxxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l wxxy(final Vector4l v) {
        return wxxy(null, v);
    }

    public static Vector4f wxxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f wxxy(final Vector4f v) {
        return wxxy(null, v);
    }

    public static Vector4l wxxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l wxxz(final Vector4l v) {
        return wxxz(null, v);
    }

    public static Vector4f wxxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f wxxz(final Vector4f v) {
        return wxxz(null, v);
    }

    public static Vector4l wxyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l wxyw(final Vector4l v) {
        return wxyw(null, v);
    }

    public static Vector4f wxyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f wxyw(final Vector4f v) {
        return wxyw(null, v);
    }

    public static Vector4l wxyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l wxyx(final Vector4l v) {
        return wxyx(null, v);
    }

    public static Vector4f wxyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f wxyx(final Vector4f v) {
        return wxyx(null, v);
    }

    public static Vector4l wxyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l wxyy(final Vector4l v) {
        return wxyy(null, v);
    }

    public static Vector4f wxyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f wxyy(final Vector4f v) {
        return wxyy(null, v);
    }

    public static Vector4l wxyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l wxyz(final Vector4l v) {
        return wxyz(null, v);
    }

    public static Vector4f wxyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f wxyz(final Vector4f v) {
        return wxyz(null, v);
    }

    public static Vector4l wxzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l wxzw(final Vector4l v) {
        return wxzw(null, v);
    }

    public static Vector4f wxzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f wxzw(final Vector4f v) {
        return wxzw(null, v);
    }

    public static Vector4l wxzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l wxzx(final Vector4l v) {
        return wxzx(null, v);
    }

    public static Vector4f wxzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f wxzx(final Vector4f v) {
        return wxzx(null, v);
    }

    public static Vector4l wxzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l wxzy(final Vector4l v) {
        return wxzy(null, v);
    }

    public static Vector4f wxzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f wxzy(final Vector4f v) {
        return wxzy(null, v);
    }

    public static Vector4l wxzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l wxzz(final Vector4l v) {
        return wxzz(null, v);
    }

    public static Vector4f wxzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f wxzz(final Vector4f v) {
        return wxzz(null, v);
    }

    public static Vector4l wyww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l wyww(final Vector4l v) {
        return wyww(null, v);
    }

    public static Vector4f wyww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f wyww(final Vector4f v) {
        return wyww(null, v);
    }

    public static Vector4l wywx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l wywx(final Vector4l v) {
        return wywx(null, v);
    }

    public static Vector4f wywx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f wywx(final Vector4f v) {
        return wywx(null, v);
    }

    public static Vector4l wywy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l wywy(final Vector4l v) {
        return wywy(null, v);
    }

    public static Vector4f wywy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f wywy(final Vector4f v) {
        return wywy(null, v);
    }

    public static Vector4l wywz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l wywz(final Vector4l v) {
        return wywz(null, v);
    }

    public static Vector4f wywz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f wywz(final Vector4f v) {
        return wywz(null, v);
    }

    public static Vector4l wyxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l wyxw(final Vector4l v) {
        return wyxw(null, v);
    }

    public static Vector4f wyxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f wyxw(final Vector4f v) {
        return wyxw(null, v);
    }

    public static Vector4l wyxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l wyxx(final Vector4l v) {
        return wyxx(null, v);
    }

    public static Vector4f wyxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f wyxx(final Vector4f v) {
        return wyxx(null, v);
    }

    public static Vector4l wyxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l wyxy(final Vector4l v) {
        return wyxy(null, v);
    }

    public static Vector4f wyxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f wyxy(final Vector4f v) {
        return wyxy(null, v);
    }

    public static Vector4l wyxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l wyxz(final Vector4l v) {
        return wyxz(null, v);
    }

    public static Vector4f wyxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f wyxz(final Vector4f v) {
        return wyxz(null, v);
    }

    public static Vector4l wyyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l wyyw(final Vector4l v) {
        return wyyw(null, v);
    }

    public static Vector4f wyyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f wyyw(final Vector4f v) {
        return wyyw(null, v);
    }

    public static Vector4l wyyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l wyyx(final Vector4l v) {
        return wyyx(null, v);
    }

    public static Vector4f wyyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f wyyx(final Vector4f v) {
        return wyyx(null, v);
    }

    public static Vector4l wyyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l wyyy(final Vector4l v) {
        return wyyy(null, v);
    }

    public static Vector4f wyyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f wyyy(final Vector4f v) {
        return wyyy(null, v);
    }

    public static Vector4l wyyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l wyyz(final Vector4l v) {
        return wyyz(null, v);
    }

    public static Vector4f wyyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f wyyz(final Vector4f v) {
        return wyyz(null, v);
    }

    public static Vector4l wyzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l wyzw(final Vector4l v) {
        return wyzw(null, v);
    }

    public static Vector4f wyzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f wyzw(final Vector4f v) {
        return wyzw(null, v);
    }

    public static Vector4l wyzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l wyzx(final Vector4l v) {
        return wyzx(null, v);
    }

    public static Vector4f wyzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f wyzx(final Vector4f v) {
        return wyzx(null, v);
    }

    public static Vector4l wyzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l wyzy(final Vector4l v) {
        return wyzy(null, v);
    }

    public static Vector4f wyzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f wyzy(final Vector4f v) {
        return wyzy(null, v);
    }

    public static Vector4l wyzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l wyzz(final Vector4l v) {
        return wyzz(null, v);
    }

    public static Vector4f wyzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f wyzz(final Vector4f v) {
        return wyzz(null, v);
    }

    public static Vector4l wzww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l wzww(final Vector4l v) {
        return wzww(null, v);
    }

    public static Vector4f wzww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f wzww(final Vector4f v) {
        return wzww(null, v);
    }

    public static Vector4l wzwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l wzwx(final Vector4l v) {
        return wzwx(null, v);
    }

    public static Vector4f wzwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f wzwx(final Vector4f v) {
        return wzwx(null, v);
    }

    public static Vector4l wzwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l wzwy(final Vector4l v) {
        return wzwy(null, v);
    }

    public static Vector4f wzwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f wzwy(final Vector4f v) {
        return wzwy(null, v);
    }

    public static Vector4l wzwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l wzwz(final Vector4l v) {
        return wzwz(null, v);
    }

    public static Vector4f wzwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f wzwz(final Vector4f v) {
        return wzwz(null, v);
    }

    public static Vector4l wzxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l wzxw(final Vector4l v) {
        return wzxw(null, v);
    }

    public static Vector4f wzxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f wzxw(final Vector4f v) {
        return wzxw(null, v);
    }

    public static Vector4l wzxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l wzxx(final Vector4l v) {
        return wzxx(null, v);
    }

    public static Vector4f wzxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f wzxx(final Vector4f v) {
        return wzxx(null, v);
    }

    public static Vector4l wzxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l wzxy(final Vector4l v) {
        return wzxy(null, v);
    }

    public static Vector4f wzxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f wzxy(final Vector4f v) {
        return wzxy(null, v);
    }

    public static Vector4l wzxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l wzxz(final Vector4l v) {
        return wzxz(null, v);
    }

    public static Vector4f wzxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f wzxz(final Vector4f v) {
        return wzxz(null, v);
    }

    public static Vector4l wzyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l wzyw(final Vector4l v) {
        return wzyw(null, v);
    }

    public static Vector4f wzyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f wzyw(final Vector4f v) {
        return wzyw(null, v);
    }

    public static Vector4l wzyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l wzyx(final Vector4l v) {
        return wzyx(null, v);
    }

    public static Vector4f wzyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f wzyx(final Vector4f v) {
        return wzyx(null, v);
    }

    public static Vector4l wzyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l wzyy(final Vector4l v) {
        return wzyy(null, v);
    }

    public static Vector4f wzyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f wzyy(final Vector4f v) {
        return wzyy(null, v);
    }

    public static Vector4l wzyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l wzyz(final Vector4l v) {
        return wzyz(null, v);
    }

    public static Vector4f wzyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f wzyz(final Vector4f v) {
        return wzyz(null, v);
    }

    public static Vector4l wzzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l wzzw(final Vector4l v) {
        return wzzw(null, v);
    }

    public static Vector4f wzzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f wzzw(final Vector4f v) {
        return wzzw(null, v);
    }

    public static Vector4l wzzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l wzzx(final Vector4l v) {
        return wzzx(null, v);
    }

    public static Vector4f wzzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f wzzx(final Vector4f v) {
        return wzzx(null, v);
    }

    public static Vector4l wzzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l wzzy(final Vector4l v) {
        return wzzy(null, v);
    }

    public static Vector4f wzzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f wzzy(final Vector4f v) {
        return wzzy(null, v);
    }

    public static Vector4l wzzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l wzzz(final Vector4l v) {
        return wzzz(null, v);
    }

    public static Vector4f wzzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.w; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f wzzz(final Vector4f v) {
        return wzzz(null, v);
    }

    public static Vector4l xwww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l xwww(final Vector4l v) {
        return xwww(null, v);
    }

    public static Vector4f xwww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f xwww(final Vector4f v) {
        return xwww(null, v);
    }

    public static Vector4l xwwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l xwwx(final Vector4l v) {
        return xwwx(null, v);
    }

    public static Vector4f xwwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f xwwx(final Vector4f v) {
        return xwwx(null, v);
    }

    public static Vector4l xwwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l xwwy(final Vector4l v) {
        return xwwy(null, v);
    }

    public static Vector4f xwwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f xwwy(final Vector4f v) {
        return xwwy(null, v);
    }

    public static Vector4l xwwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l xwwz(final Vector4l v) {
        return xwwz(null, v);
    }

    public static Vector4f xwwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f xwwz(final Vector4f v) {
        return xwwz(null, v);
    }

    public static Vector4l xwxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l xwxw(final Vector4l v) {
        return xwxw(null, v);
    }

    public static Vector4f xwxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f xwxw(final Vector4f v) {
        return xwxw(null, v);
    }

    public static Vector4l xwxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xwxx(final Vector4l v) {
        return xwxx(null, v);
    }

    public static Vector4f xwxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xwxx(final Vector4f v) {
        return xwxx(null, v);
    }

    public static Vector4l xwxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xwxy(final Vector4l v) {
        return xwxy(null, v);
    }

    public static Vector4f xwxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xwxy(final Vector4f v) {
        return xwxy(null, v);
    }

    public static Vector4l xwxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xwxz(final Vector4l v) {
        return xwxz(null, v);
    }

    public static Vector4f xwxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xwxz(final Vector4f v) {
        return xwxz(null, v);
    }

    public static Vector4l xwyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l xwyw(final Vector4l v) {
        return xwyw(null, v);
    }

    public static Vector4f xwyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f xwyw(final Vector4f v) {
        return xwyw(null, v);
    }

    public static Vector4l xwyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xwyx(final Vector4l v) {
        return xwyx(null, v);
    }

    public static Vector4f xwyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xwyx(final Vector4f v) {
        return xwyx(null, v);
    }

    public static Vector4l xwyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xwyy(final Vector4l v) {
        return xwyy(null, v);
    }

    public static Vector4f xwyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xwyy(final Vector4f v) {
        return xwyy(null, v);
    }

    public static Vector4l xwyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xwyz(final Vector4l v) {
        return xwyz(null, v);
    }

    public static Vector4f xwyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xwyz(final Vector4f v) {
        return xwyz(null, v);
    }

    public static Vector4l xwzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l xwzw(final Vector4l v) {
        return xwzw(null, v);
    }

    public static Vector4f xwzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f xwzw(final Vector4f v) {
        return xwzw(null, v);
    }

    public static Vector4l xwzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xwzx(final Vector4l v) {
        return xwzx(null, v);
    }

    public static Vector4f xwzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xwzx(final Vector4f v) {
        return xwzx(null, v);
    }

    public static Vector4l xwzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xwzy(final Vector4l v) {
        return xwzy(null, v);
    }

    public static Vector4f xwzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xwzy(final Vector4f v) {
        return xwzy(null, v);
    }

    public static Vector4l xwzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xwzz(final Vector4l v) {
        return xwzz(null, v);
    }

    public static Vector4f xwzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xwzz(final Vector4f v) {
        return xwzz(null, v);
    }

    public static Vector4l xxww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l xxww(final Vector4l v) {
        return xxww(null, v);
    }

    public static Vector4f xxww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f xxww(final Vector4f v) {
        return xxww(null, v);
    }

    public static Vector4l xxwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l xxwx(final Vector4l v) {
        return xxwx(null, v);
    }

    public static Vector4f xxwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f xxwx(final Vector4f v) {
        return xxwx(null, v);
    }

    public static Vector4l xxwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l xxwy(final Vector4l v) {
        return xxwy(null, v);
    }

    public static Vector4f xxwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f xxwy(final Vector4f v) {
        return xxwy(null, v);
    }

    public static Vector4l xxwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l xxwz(final Vector4l v) {
        return xxwz(null, v);
    }

    public static Vector4f xxwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f xxwz(final Vector4f v) {
        return xxwz(null, v);
    }

    public static Vector4l xxxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l xxxw(final Vector4l v) {
        return xxxw(null, v);
    }

    public static Vector4f xxxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f xxxw(final Vector4f v) {
        return xxxw(null, v);
    }

    public static Vector4l xxxx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xxxx(final Vector2l v) {
        return xxxx(null, v);
    }

    public static Vector4l xxxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xxxx(final Vector3l v) {
        return xxxx(null, v);
    }

    public static Vector4l xxxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xxxx(final Vector4l v) {
        return xxxx(null, v);
    }

    public static Vector4f xxxx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xxxx(final Vector2f v) {
        return xxxx(null, v);
    }

    public static Vector4f xxxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xxxx(final Vector3f v) {
        return xxxx(null, v);
    }

    public static Vector4f xxxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xxxx(final Vector4f v) {
        return xxxx(null, v);
    }

    public static Vector4l xxxy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xxxy(final Vector2l v) {
        return xxxy(null, v);
    }

    public static Vector4l xxxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xxxy(final Vector3l v) {
        return xxxy(null, v);
    }

    public static Vector4l xxxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xxxy(final Vector4l v) {
        return xxxy(null, v);
    }

    public static Vector4f xxxy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xxxy(final Vector2f v) {
        return xxxy(null, v);
    }

    public static Vector4f xxxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xxxy(final Vector3f v) {
        return xxxy(null, v);
    }

    public static Vector4f xxxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xxxy(final Vector4f v) {
        return xxxy(null, v);
    }

    public static Vector4l xxxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xxxz(final Vector3l v) {
        return xxxz(null, v);
    }

    public static Vector4l xxxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xxxz(final Vector4l v) {
        return xxxz(null, v);
    }

    public static Vector4f xxxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xxxz(final Vector3f v) {
        return xxxz(null, v);
    }

    public static Vector4f xxxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xxxz(final Vector4f v) {
        return xxxz(null, v);
    }

    public static Vector4l xxyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l xxyw(final Vector4l v) {
        return xxyw(null, v);
    }

    public static Vector4f xxyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f xxyw(final Vector4f v) {
        return xxyw(null, v);
    }

    public static Vector4l xxyx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xxyx(final Vector2l v) {
        return xxyx(null, v);
    }

    public static Vector4l xxyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xxyx(final Vector3l v) {
        return xxyx(null, v);
    }

    public static Vector4l xxyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xxyx(final Vector4l v) {
        return xxyx(null, v);
    }

    public static Vector4f xxyx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xxyx(final Vector2f v) {
        return xxyx(null, v);
    }

    public static Vector4f xxyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xxyx(final Vector3f v) {
        return xxyx(null, v);
    }

    public static Vector4f xxyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xxyx(final Vector4f v) {
        return xxyx(null, v);
    }

    public static Vector4l xxyy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xxyy(final Vector2l v) {
        return xxyy(null, v);
    }

    public static Vector4l xxyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xxyy(final Vector3l v) {
        return xxyy(null, v);
    }

    public static Vector4l xxyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xxyy(final Vector4l v) {
        return xxyy(null, v);
    }

    public static Vector4f xxyy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xxyy(final Vector2f v) {
        return xxyy(null, v);
    }

    public static Vector4f xxyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xxyy(final Vector3f v) {
        return xxyy(null, v);
    }

    public static Vector4f xxyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xxyy(final Vector4f v) {
        return xxyy(null, v);
    }

    public static Vector4l xxyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xxyz(final Vector3l v) {
        return xxyz(null, v);
    }

    public static Vector4l xxyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xxyz(final Vector4l v) {
        return xxyz(null, v);
    }

    public static Vector4f xxyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xxyz(final Vector3f v) {
        return xxyz(null, v);
    }

    public static Vector4f xxyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xxyz(final Vector4f v) {
        return xxyz(null, v);
    }

    public static Vector4l xxzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l xxzw(final Vector4l v) {
        return xxzw(null, v);
    }

    public static Vector4f xxzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f xxzw(final Vector4f v) {
        return xxzw(null, v);
    }

    public static Vector4l xxzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xxzx(final Vector3l v) {
        return xxzx(null, v);
    }

    public static Vector4l xxzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xxzx(final Vector4l v) {
        return xxzx(null, v);
    }

    public static Vector4f xxzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xxzx(final Vector3f v) {
        return xxzx(null, v);
    }

    public static Vector4f xxzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xxzx(final Vector4f v) {
        return xxzx(null, v);
    }

    public static Vector4l xxzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xxzy(final Vector3l v) {
        return xxzy(null, v);
    }

    public static Vector4l xxzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xxzy(final Vector4l v) {
        return xxzy(null, v);
    }

    public static Vector4f xxzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xxzy(final Vector3f v) {
        return xxzy(null, v);
    }

    public static Vector4f xxzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xxzy(final Vector4f v) {
        return xxzy(null, v);
    }

    public static Vector4l xxzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xxzz(final Vector3l v) {
        return xxzz(null, v);
    }

    public static Vector4l xxzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xxzz(final Vector4l v) {
        return xxzz(null, v);
    }

    public static Vector4f xxzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xxzz(final Vector3f v) {
        return xxzz(null, v);
    }

    public static Vector4f xxzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xxzz(final Vector4f v) {
        return xxzz(null, v);
    }

    public static Vector4l xyww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l xyww(final Vector4l v) {
        return xyww(null, v);
    }

    public static Vector4f xyww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f xyww(final Vector4f v) {
        return xyww(null, v);
    }

    public static Vector4l xywx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l xywx(final Vector4l v) {
        return xywx(null, v);
    }

    public static Vector4f xywx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f xywx(final Vector4f v) {
        return xywx(null, v);
    }

    public static Vector4l xywy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l xywy(final Vector4l v) {
        return xywy(null, v);
    }

    public static Vector4f xywy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f xywy(final Vector4f v) {
        return xywy(null, v);
    }

    public static Vector4l xywz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l xywz(final Vector4l v) {
        return xywz(null, v);
    }

    public static Vector4f xywz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f xywz(final Vector4f v) {
        return xywz(null, v);
    }

    public static Vector4l xyxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l xyxw(final Vector4l v) {
        return xyxw(null, v);
    }

    public static Vector4f xyxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f xyxw(final Vector4f v) {
        return xyxw(null, v);
    }

    public static Vector4l xyxx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xyxx(final Vector2l v) {
        return xyxx(null, v);
    }

    public static Vector4l xyxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xyxx(final Vector3l v) {
        return xyxx(null, v);
    }

    public static Vector4l xyxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xyxx(final Vector4l v) {
        return xyxx(null, v);
    }

    public static Vector4f xyxx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xyxx(final Vector2f v) {
        return xyxx(null, v);
    }

    public static Vector4f xyxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xyxx(final Vector3f v) {
        return xyxx(null, v);
    }

    public static Vector4f xyxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xyxx(final Vector4f v) {
        return xyxx(null, v);
    }

    public static Vector4l xyxy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xyxy(final Vector2l v) {
        return xyxy(null, v);
    }

    public static Vector4l xyxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xyxy(final Vector3l v) {
        return xyxy(null, v);
    }

    public static Vector4l xyxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xyxy(final Vector4l v) {
        return xyxy(null, v);
    }

    public static Vector4f xyxy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xyxy(final Vector2f v) {
        return xyxy(null, v);
    }

    public static Vector4f xyxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xyxy(final Vector3f v) {
        return xyxy(null, v);
    }

    public static Vector4f xyxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xyxy(final Vector4f v) {
        return xyxy(null, v);
    }

    public static Vector4l xyxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xyxz(final Vector3l v) {
        return xyxz(null, v);
    }

    public static Vector4l xyxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xyxz(final Vector4l v) {
        return xyxz(null, v);
    }

    public static Vector4f xyxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xyxz(final Vector3f v) {
        return xyxz(null, v);
    }

    public static Vector4f xyxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xyxz(final Vector4f v) {
        return xyxz(null, v);
    }

    public static Vector4l xyyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l xyyw(final Vector4l v) {
        return xyyw(null, v);
    }

    public static Vector4f xyyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f xyyw(final Vector4f v) {
        return xyyw(null, v);
    }

    public static Vector4l xyyx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xyyx(final Vector2l v) {
        return xyyx(null, v);
    }

    public static Vector4l xyyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xyyx(final Vector3l v) {
        return xyyx(null, v);
    }

    public static Vector4l xyyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xyyx(final Vector4l v) {
        return xyyx(null, v);
    }

    public static Vector4f xyyx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xyyx(final Vector2f v) {
        return xyyx(null, v);
    }

    public static Vector4f xyyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xyyx(final Vector3f v) {
        return xyyx(null, v);
    }

    public static Vector4f xyyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xyyx(final Vector4f v) {
        return xyyx(null, v);
    }

    public static Vector4l xyyy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xyyy(final Vector2l v) {
        return xyyy(null, v);
    }

    public static Vector4l xyyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xyyy(final Vector3l v) {
        return xyyy(null, v);
    }

    public static Vector4l xyyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xyyy(final Vector4l v) {
        return xyyy(null, v);
    }

    public static Vector4f xyyy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xyyy(final Vector2f v) {
        return xyyy(null, v);
    }

    public static Vector4f xyyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xyyy(final Vector3f v) {
        return xyyy(null, v);
    }

    public static Vector4f xyyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xyyy(final Vector4f v) {
        return xyyy(null, v);
    }

    public static Vector4l xyyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xyyz(final Vector3l v) {
        return xyyz(null, v);
    }

    public static Vector4l xyyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xyyz(final Vector4l v) {
        return xyyz(null, v);
    }

    public static Vector4f xyyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xyyz(final Vector3f v) {
        return xyyz(null, v);
    }

    public static Vector4f xyyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xyyz(final Vector4f v) {
        return xyyz(null, v);
    }

    public static Vector4l xyzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l xyzw(final Vector4l v) {
        return xyzw(null, v);
    }

    public static Vector4f xyzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f xyzw(final Vector4f v) {
        return xyzw(null, v);
    }

    public static Vector4l xyzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xyzx(final Vector3l v) {
        return xyzx(null, v);
    }

    public static Vector4l xyzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xyzx(final Vector4l v) {
        return xyzx(null, v);
    }

    public static Vector4f xyzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xyzx(final Vector3f v) {
        return xyzx(null, v);
    }

    public static Vector4f xyzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xyzx(final Vector4f v) {
        return xyzx(null, v);
    }

    public static Vector4l xyzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xyzy(final Vector3l v) {
        return xyzy(null, v);
    }

    public static Vector4l xyzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xyzy(final Vector4l v) {
        return xyzy(null, v);
    }

    public static Vector4f xyzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xyzy(final Vector3f v) {
        return xyzy(null, v);
    }

    public static Vector4f xyzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xyzy(final Vector4f v) {
        return xyzy(null, v);
    }

    public static Vector4l xyzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xyzz(final Vector3l v) {
        return xyzz(null, v);
    }

    public static Vector4l xyzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xyzz(final Vector4l v) {
        return xyzz(null, v);
    }

    public static Vector4f xyzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xyzz(final Vector3f v) {
        return xyzz(null, v);
    }

    public static Vector4f xyzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xyzz(final Vector4f v) {
        return xyzz(null, v);
    }

    public static Vector4l xzww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l xzww(final Vector4l v) {
        return xzww(null, v);
    }

    public static Vector4f xzww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f xzww(final Vector4f v) {
        return xzww(null, v);
    }

    public static Vector4l xzwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l xzwx(final Vector4l v) {
        return xzwx(null, v);
    }

    public static Vector4f xzwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f xzwx(final Vector4f v) {
        return xzwx(null, v);
    }

    public static Vector4l xzwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l xzwy(final Vector4l v) {
        return xzwy(null, v);
    }

    public static Vector4f xzwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f xzwy(final Vector4f v) {
        return xzwy(null, v);
    }

    public static Vector4l xzwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l xzwz(final Vector4l v) {
        return xzwz(null, v);
    }

    public static Vector4f xzwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f xzwz(final Vector4f v) {
        return xzwz(null, v);
    }

    public static Vector4l xzxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l xzxw(final Vector4l v) {
        return xzxw(null, v);
    }

    public static Vector4f xzxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f xzxw(final Vector4f v) {
        return xzxw(null, v);
    }

    public static Vector4l xzxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xzxx(final Vector3l v) {
        return xzxx(null, v);
    }

    public static Vector4l xzxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l xzxx(final Vector4l v) {
        return xzxx(null, v);
    }

    public static Vector4f xzxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xzxx(final Vector3f v) {
        return xzxx(null, v);
    }

    public static Vector4f xzxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f xzxx(final Vector4f v) {
        return xzxx(null, v);
    }

    public static Vector4l xzxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xzxy(final Vector3l v) {
        return xzxy(null, v);
    }

    public static Vector4l xzxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l xzxy(final Vector4l v) {
        return xzxy(null, v);
    }

    public static Vector4f xzxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xzxy(final Vector3f v) {
        return xzxy(null, v);
    }

    public static Vector4f xzxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f xzxy(final Vector4f v) {
        return xzxy(null, v);
    }

    public static Vector4l xzxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xzxz(final Vector3l v) {
        return xzxz(null, v);
    }

    public static Vector4l xzxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l xzxz(final Vector4l v) {
        return xzxz(null, v);
    }

    public static Vector4f xzxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xzxz(final Vector3f v) {
        return xzxz(null, v);
    }

    public static Vector4f xzxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f xzxz(final Vector4f v) {
        return xzxz(null, v);
    }

    public static Vector4l xzyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l xzyw(final Vector4l v) {
        return xzyw(null, v);
    }

    public static Vector4f xzyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f xzyw(final Vector4f v) {
        return xzyw(null, v);
    }

    public static Vector4l xzyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xzyx(final Vector3l v) {
        return xzyx(null, v);
    }

    public static Vector4l xzyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l xzyx(final Vector4l v) {
        return xzyx(null, v);
    }

    public static Vector4f xzyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xzyx(final Vector3f v) {
        return xzyx(null, v);
    }

    public static Vector4f xzyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f xzyx(final Vector4f v) {
        return xzyx(null, v);
    }

    public static Vector4l xzyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xzyy(final Vector3l v) {
        return xzyy(null, v);
    }

    public static Vector4l xzyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l xzyy(final Vector4l v) {
        return xzyy(null, v);
    }

    public static Vector4f xzyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xzyy(final Vector3f v) {
        return xzyy(null, v);
    }

    public static Vector4f xzyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f xzyy(final Vector4f v) {
        return xzyy(null, v);
    }

    public static Vector4l xzyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xzyz(final Vector3l v) {
        return xzyz(null, v);
    }

    public static Vector4l xzyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l xzyz(final Vector4l v) {
        return xzyz(null, v);
    }

    public static Vector4f xzyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xzyz(final Vector3f v) {
        return xzyz(null, v);
    }

    public static Vector4f xzyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f xzyz(final Vector4f v) {
        return xzyz(null, v);
    }

    public static Vector4l xzzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l xzzw(final Vector4l v) {
        return xzzw(null, v);
    }

    public static Vector4f xzzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f xzzw(final Vector4f v) {
        return xzzw(null, v);
    }

    public static Vector4l xzzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xzzx(final Vector3l v) {
        return xzzx(null, v);
    }

    public static Vector4l xzzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l xzzx(final Vector4l v) {
        return xzzx(null, v);
    }

    public static Vector4f xzzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xzzx(final Vector3f v) {
        return xzzx(null, v);
    }

    public static Vector4f xzzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f xzzx(final Vector4f v) {
        return xzzx(null, v);
    }

    public static Vector4l xzzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xzzy(final Vector3l v) {
        return xzzy(null, v);
    }

    public static Vector4l xzzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l xzzy(final Vector4l v) {
        return xzzy(null, v);
    }

    public static Vector4f xzzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xzzy(final Vector3f v) {
        return xzzy(null, v);
    }

    public static Vector4f xzzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f xzzy(final Vector4f v) {
        return xzzy(null, v);
    }

    public static Vector4l xzzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xzzz(final Vector3l v) {
        return xzzz(null, v);
    }

    public static Vector4l xzzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l xzzz(final Vector4l v) {
        return xzzz(null, v);
    }

    public static Vector4f xzzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xzzz(final Vector3f v) {
        return xzzz(null, v);
    }

    public static Vector4f xzzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.x; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f xzzz(final Vector4f v) {
        return xzzz(null, v);
    }

    public static Vector4l ywww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l ywww(final Vector4l v) {
        return ywww(null, v);
    }

    public static Vector4f ywww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f ywww(final Vector4f v) {
        return ywww(null, v);
    }

    public static Vector4l ywwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l ywwx(final Vector4l v) {
        return ywwx(null, v);
    }

    public static Vector4f ywwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f ywwx(final Vector4f v) {
        return ywwx(null, v);
    }

    public static Vector4l ywwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l ywwy(final Vector4l v) {
        return ywwy(null, v);
    }

    public static Vector4f ywwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f ywwy(final Vector4f v) {
        return ywwy(null, v);
    }

    public static Vector4l ywwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l ywwz(final Vector4l v) {
        return ywwz(null, v);
    }

    public static Vector4f ywwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f ywwz(final Vector4f v) {
        return ywwz(null, v);
    }

    public static Vector4l ywxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l ywxw(final Vector4l v) {
        return ywxw(null, v);
    }

    public static Vector4f ywxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f ywxw(final Vector4f v) {
        return ywxw(null, v);
    }

    public static Vector4l ywxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l ywxx(final Vector4l v) {
        return ywxx(null, v);
    }

    public static Vector4f ywxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f ywxx(final Vector4f v) {
        return ywxx(null, v);
    }

    public static Vector4l ywxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l ywxy(final Vector4l v) {
        return ywxy(null, v);
    }

    public static Vector4f ywxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f ywxy(final Vector4f v) {
        return ywxy(null, v);
    }

    public static Vector4l ywxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l ywxz(final Vector4l v) {
        return ywxz(null, v);
    }

    public static Vector4f ywxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f ywxz(final Vector4f v) {
        return ywxz(null, v);
    }

    public static Vector4l ywyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l ywyw(final Vector4l v) {
        return ywyw(null, v);
    }

    public static Vector4f ywyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f ywyw(final Vector4f v) {
        return ywyw(null, v);
    }

    public static Vector4l ywyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l ywyx(final Vector4l v) {
        return ywyx(null, v);
    }

    public static Vector4f ywyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f ywyx(final Vector4f v) {
        return ywyx(null, v);
    }

    public static Vector4l ywyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l ywyy(final Vector4l v) {
        return ywyy(null, v);
    }

    public static Vector4f ywyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f ywyy(final Vector4f v) {
        return ywyy(null, v);
    }

    public static Vector4l ywyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l ywyz(final Vector4l v) {
        return ywyz(null, v);
    }

    public static Vector4f ywyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f ywyz(final Vector4f v) {
        return ywyz(null, v);
    }

    public static Vector4l ywzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l ywzw(final Vector4l v) {
        return ywzw(null, v);
    }

    public static Vector4f ywzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f ywzw(final Vector4f v) {
        return ywzw(null, v);
    }

    public static Vector4l ywzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l ywzx(final Vector4l v) {
        return ywzx(null, v);
    }

    public static Vector4f ywzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f ywzx(final Vector4f v) {
        return ywzx(null, v);
    }

    public static Vector4l ywzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l ywzy(final Vector4l v) {
        return ywzy(null, v);
    }

    public static Vector4f ywzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f ywzy(final Vector4f v) {
        return ywzy(null, v);
    }

    public static Vector4l ywzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l ywzz(final Vector4l v) {
        return ywzz(null, v);
    }

    public static Vector4f ywzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f ywzz(final Vector4f v) {
        return ywzz(null, v);
    }

    public static Vector4l yxww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l yxww(final Vector4l v) {
        return yxww(null, v);
    }

    public static Vector4f yxww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f yxww(final Vector4f v) {
        return yxww(null, v);
    }

    public static Vector4l yxwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l yxwx(final Vector4l v) {
        return yxwx(null, v);
    }

    public static Vector4f yxwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f yxwx(final Vector4f v) {
        return yxwx(null, v);
    }

    public static Vector4l yxwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l yxwy(final Vector4l v) {
        return yxwy(null, v);
    }

    public static Vector4f yxwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f yxwy(final Vector4f v) {
        return yxwy(null, v);
    }

    public static Vector4l yxwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l yxwz(final Vector4l v) {
        return yxwz(null, v);
    }

    public static Vector4f yxwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f yxwz(final Vector4f v) {
        return yxwz(null, v);
    }

    public static Vector4l yxxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l yxxw(final Vector4l v) {
        return yxxw(null, v);
    }

    public static Vector4f yxxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f yxxw(final Vector4f v) {
        return yxxw(null, v);
    }

    public static Vector4l yxxx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yxxx(final Vector2l v) {
        return yxxx(null, v);
    }

    public static Vector4l yxxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yxxx(final Vector3l v) {
        return yxxx(null, v);
    }

    public static Vector4l yxxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yxxx(final Vector4l v) {
        return yxxx(null, v);
    }

    public static Vector4f yxxx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yxxx(final Vector2f v) {
        return yxxx(null, v);
    }

    public static Vector4f yxxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yxxx(final Vector3f v) {
        return yxxx(null, v);
    }

    public static Vector4f yxxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yxxx(final Vector4f v) {
        return yxxx(null, v);
    }

    public static Vector4l yxxy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yxxy(final Vector2l v) {
        return yxxy(null, v);
    }

    public static Vector4l yxxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yxxy(final Vector3l v) {
        return yxxy(null, v);
    }

    public static Vector4l yxxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yxxy(final Vector4l v) {
        return yxxy(null, v);
    }

    public static Vector4f yxxy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yxxy(final Vector2f v) {
        return yxxy(null, v);
    }

    public static Vector4f yxxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yxxy(final Vector3f v) {
        return yxxy(null, v);
    }

    public static Vector4f yxxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yxxy(final Vector4f v) {
        return yxxy(null, v);
    }

    public static Vector4l yxxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l yxxz(final Vector3l v) {
        return yxxz(null, v);
    }

    public static Vector4l yxxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l yxxz(final Vector4l v) {
        return yxxz(null, v);
    }

    public static Vector4f yxxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f yxxz(final Vector3f v) {
        return yxxz(null, v);
    }

    public static Vector4f yxxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f yxxz(final Vector4f v) {
        return yxxz(null, v);
    }

    public static Vector4l yxyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l yxyw(final Vector4l v) {
        return yxyw(null, v);
    }

    public static Vector4f yxyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f yxyw(final Vector4f v) {
        return yxyw(null, v);
    }

    public static Vector4l yxyx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yxyx(final Vector2l v) {
        return yxyx(null, v);
    }

    public static Vector4l yxyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yxyx(final Vector3l v) {
        return yxyx(null, v);
    }

    public static Vector4l yxyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yxyx(final Vector4l v) {
        return yxyx(null, v);
    }

    public static Vector4f yxyx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yxyx(final Vector2f v) {
        return yxyx(null, v);
    }

    public static Vector4f yxyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yxyx(final Vector3f v) {
        return yxyx(null, v);
    }

    public static Vector4f yxyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yxyx(final Vector4f v) {
        return yxyx(null, v);
    }

    public static Vector4l yxyy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yxyy(final Vector2l v) {
        return yxyy(null, v);
    }

    public static Vector4l yxyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yxyy(final Vector3l v) {
        return yxyy(null, v);
    }

    public static Vector4l yxyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yxyy(final Vector4l v) {
        return yxyy(null, v);
    }

    public static Vector4f yxyy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yxyy(final Vector2f v) {
        return yxyy(null, v);
    }

    public static Vector4f yxyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yxyy(final Vector3f v) {
        return yxyy(null, v);
    }

    public static Vector4f yxyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yxyy(final Vector4f v) {
        return yxyy(null, v);
    }

    public static Vector4l yxyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l yxyz(final Vector3l v) {
        return yxyz(null, v);
    }

    public static Vector4l yxyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l yxyz(final Vector4l v) {
        return yxyz(null, v);
    }

    public static Vector4f yxyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f yxyz(final Vector3f v) {
        return yxyz(null, v);
    }

    public static Vector4f yxyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f yxyz(final Vector4f v) {
        return yxyz(null, v);
    }

    public static Vector4l yxzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l yxzw(final Vector4l v) {
        return yxzw(null, v);
    }

    public static Vector4f yxzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f yxzw(final Vector4f v) {
        return yxzw(null, v);
    }

    public static Vector4l yxzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l yxzx(final Vector3l v) {
        return yxzx(null, v);
    }

    public static Vector4l yxzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l yxzx(final Vector4l v) {
        return yxzx(null, v);
    }

    public static Vector4f yxzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f yxzx(final Vector3f v) {
        return yxzx(null, v);
    }

    public static Vector4f yxzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f yxzx(final Vector4f v) {
        return yxzx(null, v);
    }

    public static Vector4l yxzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l yxzy(final Vector3l v) {
        return yxzy(null, v);
    }

    public static Vector4l yxzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l yxzy(final Vector4l v) {
        return yxzy(null, v);
    }

    public static Vector4f yxzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f yxzy(final Vector3f v) {
        return yxzy(null, v);
    }

    public static Vector4f yxzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f yxzy(final Vector4f v) {
        return yxzy(null, v);
    }

    public static Vector4l yxzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l yxzz(final Vector3l v) {
        return yxzz(null, v);
    }

    public static Vector4l yxzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l yxzz(final Vector4l v) {
        return yxzz(null, v);
    }

    public static Vector4f yxzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f yxzz(final Vector3f v) {
        return yxzz(null, v);
    }

    public static Vector4f yxzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f yxzz(final Vector4f v) {
        return yxzz(null, v);
    }

    public static Vector4l yyww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l yyww(final Vector4l v) {
        return yyww(null, v);
    }

    public static Vector4f yyww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f yyww(final Vector4f v) {
        return yyww(null, v);
    }

    public static Vector4l yywx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l yywx(final Vector4l v) {
        return yywx(null, v);
    }

    public static Vector4f yywx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f yywx(final Vector4f v) {
        return yywx(null, v);
    }

    public static Vector4l yywy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l yywy(final Vector4l v) {
        return yywy(null, v);
    }

    public static Vector4f yywy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f yywy(final Vector4f v) {
        return yywy(null, v);
    }

    public static Vector4l yywz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l yywz(final Vector4l v) {
        return yywz(null, v);
    }

    public static Vector4f yywz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f yywz(final Vector4f v) {
        return yywz(null, v);
    }

    public static Vector4l yyxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l yyxw(final Vector4l v) {
        return yyxw(null, v);
    }

    public static Vector4f yyxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f yyxw(final Vector4f v) {
        return yyxw(null, v);
    }

    public static Vector4l yyxx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yyxx(final Vector2l v) {
        return yyxx(null, v);
    }

    public static Vector4l yyxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yyxx(final Vector3l v) {
        return yyxx(null, v);
    }

    public static Vector4l yyxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yyxx(final Vector4l v) {
        return yyxx(null, v);
    }

    public static Vector4f yyxx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yyxx(final Vector2f v) {
        return yyxx(null, v);
    }

    public static Vector4f yyxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yyxx(final Vector3f v) {
        return yyxx(null, v);
    }

    public static Vector4f yyxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yyxx(final Vector4f v) {
        return yyxx(null, v);
    }

    public static Vector4l yyxy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yyxy(final Vector2l v) {
        return yyxy(null, v);
    }

    public static Vector4l yyxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yyxy(final Vector3l v) {
        return yyxy(null, v);
    }

    public static Vector4l yyxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yyxy(final Vector4l v) {
        return yyxy(null, v);
    }

    public static Vector4f yyxy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yyxy(final Vector2f v) {
        return yyxy(null, v);
    }

    public static Vector4f yyxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yyxy(final Vector3f v) {
        return yyxy(null, v);
    }

    public static Vector4f yyxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yyxy(final Vector4f v) {
        return yyxy(null, v);
    }

    public static Vector4l yyxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l yyxz(final Vector3l v) {
        return yyxz(null, v);
    }

    public static Vector4l yyxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l yyxz(final Vector4l v) {
        return yyxz(null, v);
    }

    public static Vector4f yyxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f yyxz(final Vector3f v) {
        return yyxz(null, v);
    }

    public static Vector4f yyxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f yyxz(final Vector4f v) {
        return yyxz(null, v);
    }

    public static Vector4l yyyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l yyyw(final Vector4l v) {
        return yyyw(null, v);
    }

    public static Vector4f yyyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f yyyw(final Vector4f v) {
        return yyyw(null, v);
    }

    public static Vector4l yyyx(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yyyx(final Vector2l v) {
        return yyyx(null, v);
    }

    public static Vector4l yyyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yyyx(final Vector3l v) {
        return yyyx(null, v);
    }

    public static Vector4l yyyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yyyx(final Vector4l v) {
        return yyyx(null, v);
    }

    public static Vector4f yyyx(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yyyx(final Vector2f v) {
        return yyyx(null, v);
    }

    public static Vector4f yyyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yyyx(final Vector3f v) {
        return yyyx(null, v);
    }

    public static Vector4f yyyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yyyx(final Vector4f v) {
        return yyyx(null, v);
    }

    public static Vector4l yyyy(Vector4l o, final Vector2l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yyyy(final Vector2l v) {
        return yyyy(null, v);
    }

    public static Vector4l yyyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yyyy(final Vector3l v) {
        return yyyy(null, v);
    }

    public static Vector4l yyyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yyyy(final Vector4l v) {
        return yyyy(null, v);
    }

    public static Vector4f yyyy(Vector4f o, final Vector2f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yyyy(final Vector2f v) {
        return yyyy(null, v);
    }

    public static Vector4f yyyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yyyy(final Vector3f v) {
        return yyyy(null, v);
    }

    public static Vector4f yyyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yyyy(final Vector4f v) {
        return yyyy(null, v);
    }

    public static Vector4l yyyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l yyyz(final Vector3l v) {
        return yyyz(null, v);
    }

    public static Vector4l yyyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l yyyz(final Vector4l v) {
        return yyyz(null, v);
    }

    public static Vector4f yyyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f yyyz(final Vector3f v) {
        return yyyz(null, v);
    }

    public static Vector4f yyyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f yyyz(final Vector4f v) {
        return yyyz(null, v);
    }

    public static Vector4l yyzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l yyzw(final Vector4l v) {
        return yyzw(null, v);
    }

    public static Vector4f yyzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f yyzw(final Vector4f v) {
        return yyzw(null, v);
    }

    public static Vector4l yyzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l yyzx(final Vector3l v) {
        return yyzx(null, v);
    }

    public static Vector4l yyzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l yyzx(final Vector4l v) {
        return yyzx(null, v);
    }

    public static Vector4f yyzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f yyzx(final Vector3f v) {
        return yyzx(null, v);
    }

    public static Vector4f yyzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f yyzx(final Vector4f v) {
        return yyzx(null, v);
    }

    public static Vector4l yyzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l yyzy(final Vector3l v) {
        return yyzy(null, v);
    }

    public static Vector4l yyzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l yyzy(final Vector4l v) {
        return yyzy(null, v);
    }

    public static Vector4f yyzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f yyzy(final Vector3f v) {
        return yyzy(null, v);
    }

    public static Vector4f yyzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f yyzy(final Vector4f v) {
        return yyzy(null, v);
    }

    public static Vector4l yyzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l yyzz(final Vector3l v) {
        return yyzz(null, v);
    }

    public static Vector4l yyzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l yyzz(final Vector4l v) {
        return yyzz(null, v);
    }

    public static Vector4f yyzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f yyzz(final Vector3f v) {
        return yyzz(null, v);
    }

    public static Vector4f yyzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f yyzz(final Vector4f v) {
        return yyzz(null, v);
    }

    public static Vector4l yzww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l yzww(final Vector4l v) {
        return yzww(null, v);
    }

    public static Vector4f yzww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f yzww(final Vector4f v) {
        return yzww(null, v);
    }

    public static Vector4l yzwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l yzwx(final Vector4l v) {
        return yzwx(null, v);
    }

    public static Vector4f yzwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f yzwx(final Vector4f v) {
        return yzwx(null, v);
    }

    public static Vector4l yzwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l yzwy(final Vector4l v) {
        return yzwy(null, v);
    }

    public static Vector4f yzwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f yzwy(final Vector4f v) {
        return yzwy(null, v);
    }

    public static Vector4l yzwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l yzwz(final Vector4l v) {
        return yzwz(null, v);
    }

    public static Vector4f yzwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f yzwz(final Vector4f v) {
        return yzwz(null, v);
    }

    public static Vector4l yzxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l yzxw(final Vector4l v) {
        return yzxw(null, v);
    }

    public static Vector4f yzxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f yzxw(final Vector4f v) {
        return yzxw(null, v);
    }

    public static Vector4l yzxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yzxx(final Vector3l v) {
        return yzxx(null, v);
    }

    public static Vector4l yzxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l yzxx(final Vector4l v) {
        return yzxx(null, v);
    }

    public static Vector4f yzxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yzxx(final Vector3f v) {
        return yzxx(null, v);
    }

    public static Vector4f yzxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f yzxx(final Vector4f v) {
        return yzxx(null, v);
    }

    public static Vector4l yzxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yzxy(final Vector3l v) {
        return yzxy(null, v);
    }

    public static Vector4l yzxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l yzxy(final Vector4l v) {
        return yzxy(null, v);
    }

    public static Vector4f yzxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yzxy(final Vector3f v) {
        return yzxy(null, v);
    }

    public static Vector4f yzxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f yzxy(final Vector4f v) {
        return yzxy(null, v);
    }

    public static Vector4l yzxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l yzxz(final Vector3l v) {
        return yzxz(null, v);
    }

    public static Vector4l yzxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l yzxz(final Vector4l v) {
        return yzxz(null, v);
    }

    public static Vector4f yzxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f yzxz(final Vector3f v) {
        return yzxz(null, v);
    }

    public static Vector4f yzxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f yzxz(final Vector4f v) {
        return yzxz(null, v);
    }

    public static Vector4l yzyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l yzyw(final Vector4l v) {
        return yzyw(null, v);
    }

    public static Vector4f yzyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f yzyw(final Vector4f v) {
        return yzyw(null, v);
    }

    public static Vector4l yzyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yzyx(final Vector3l v) {
        return yzyx(null, v);
    }

    public static Vector4l yzyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l yzyx(final Vector4l v) {
        return yzyx(null, v);
    }

    public static Vector4f yzyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yzyx(final Vector3f v) {
        return yzyx(null, v);
    }

    public static Vector4f yzyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f yzyx(final Vector4f v) {
        return yzyx(null, v);
    }

    public static Vector4l yzyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yzyy(final Vector3l v) {
        return yzyy(null, v);
    }

    public static Vector4l yzyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l yzyy(final Vector4l v) {
        return yzyy(null, v);
    }

    public static Vector4f yzyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yzyy(final Vector3f v) {
        return yzyy(null, v);
    }

    public static Vector4f yzyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f yzyy(final Vector4f v) {
        return yzyy(null, v);
    }

    public static Vector4l yzyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l yzyz(final Vector3l v) {
        return yzyz(null, v);
    }

    public static Vector4l yzyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l yzyz(final Vector4l v) {
        return yzyz(null, v);
    }

    public static Vector4f yzyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f yzyz(final Vector3f v) {
        return yzyz(null, v);
    }

    public static Vector4f yzyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f yzyz(final Vector4f v) {
        return yzyz(null, v);
    }

    public static Vector4l yzzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l yzzw(final Vector4l v) {
        return yzzw(null, v);
    }

    public static Vector4f yzzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f yzzw(final Vector4f v) {
        return yzzw(null, v);
    }

    public static Vector4l yzzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l yzzx(final Vector3l v) {
        return yzzx(null, v);
    }

    public static Vector4l yzzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l yzzx(final Vector4l v) {
        return yzzx(null, v);
    }

    public static Vector4f yzzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f yzzx(final Vector3f v) {
        return yzzx(null, v);
    }

    public static Vector4f yzzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f yzzx(final Vector4f v) {
        return yzzx(null, v);
    }

    public static Vector4l yzzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l yzzy(final Vector3l v) {
        return yzzy(null, v);
    }

    public static Vector4l yzzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l yzzy(final Vector4l v) {
        return yzzy(null, v);
    }

    public static Vector4f yzzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f yzzy(final Vector3f v) {
        return yzzy(null, v);
    }

    public static Vector4f yzzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f yzzy(final Vector4f v) {
        return yzzy(null, v);
    }

    public static Vector4l yzzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l yzzz(final Vector3l v) {
        return yzzz(null, v);
    }

    public static Vector4l yzzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l yzzz(final Vector4l v) {
        return yzzz(null, v);
    }

    public static Vector4f yzzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f yzzz(final Vector3f v) {
        return yzzz(null, v);
    }

    public static Vector4f yzzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.y; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f yzzz(final Vector4f v) {
        return yzzz(null, v);
    }

    public static Vector4l zwww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l zwww(final Vector4l v) {
        return zwww(null, v);
    }

    public static Vector4f zwww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f zwww(final Vector4f v) {
        return zwww(null, v);
    }

    public static Vector4l zwwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l zwwx(final Vector4l v) {
        return zwwx(null, v);
    }

    public static Vector4f zwwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f zwwx(final Vector4f v) {
        return zwwx(null, v);
    }

    public static Vector4l zwwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l zwwy(final Vector4l v) {
        return zwwy(null, v);
    }

    public static Vector4f zwwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f zwwy(final Vector4f v) {
        return zwwy(null, v);
    }

    public static Vector4l zwwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l zwwz(final Vector4l v) {
        return zwwz(null, v);
    }

    public static Vector4f zwwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f zwwz(final Vector4f v) {
        return zwwz(null, v);
    }

    public static Vector4l zwxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l zwxw(final Vector4l v) {
        return zwxw(null, v);
    }

    public static Vector4f zwxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f zwxw(final Vector4f v) {
        return zwxw(null, v);
    }

    public static Vector4l zwxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zwxx(final Vector4l v) {
        return zwxx(null, v);
    }

    public static Vector4f zwxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zwxx(final Vector4f v) {
        return zwxx(null, v);
    }

    public static Vector4l zwxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zwxy(final Vector4l v) {
        return zwxy(null, v);
    }

    public static Vector4f zwxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zwxy(final Vector4f v) {
        return zwxy(null, v);
    }

    public static Vector4l zwxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zwxz(final Vector4l v) {
        return zwxz(null, v);
    }

    public static Vector4f zwxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zwxz(final Vector4f v) {
        return zwxz(null, v);
    }

    public static Vector4l zwyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l zwyw(final Vector4l v) {
        return zwyw(null, v);
    }

    public static Vector4f zwyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f zwyw(final Vector4f v) {
        return zwyw(null, v);
    }

    public static Vector4l zwyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zwyx(final Vector4l v) {
        return zwyx(null, v);
    }

    public static Vector4f zwyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zwyx(final Vector4f v) {
        return zwyx(null, v);
    }

    public static Vector4l zwyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zwyy(final Vector4l v) {
        return zwyy(null, v);
    }

    public static Vector4f zwyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zwyy(final Vector4f v) {
        return zwyy(null, v);
    }

    public static Vector4l zwyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zwyz(final Vector4l v) {
        return zwyz(null, v);
    }

    public static Vector4f zwyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zwyz(final Vector4f v) {
        return zwyz(null, v);
    }

    public static Vector4l zwzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l zwzw(final Vector4l v) {
        return zwzw(null, v);
    }

    public static Vector4f zwzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f zwzw(final Vector4f v) {
        return zwzw(null, v);
    }

    public static Vector4l zwzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zwzx(final Vector4l v) {
        return zwzx(null, v);
    }

    public static Vector4f zwzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zwzx(final Vector4f v) {
        return zwzx(null, v);
    }

    public static Vector4l zwzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zwzy(final Vector4l v) {
        return zwzy(null, v);
    }

    public static Vector4f zwzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zwzy(final Vector4f v) {
        return zwzy(null, v);
    }

    public static Vector4l zwzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zwzz(final Vector4l v) {
        return zwzz(null, v);
    }

    public static Vector4f zwzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.w; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zwzz(final Vector4f v) {
        return zwzz(null, v);
    }

    public static Vector4l zxww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l zxww(final Vector4l v) {
        return zxww(null, v);
    }

    public static Vector4f zxww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f zxww(final Vector4f v) {
        return zxww(null, v);
    }

    public static Vector4l zxwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l zxwx(final Vector4l v) {
        return zxwx(null, v);
    }

    public static Vector4f zxwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f zxwx(final Vector4f v) {
        return zxwx(null, v);
    }

    public static Vector4l zxwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l zxwy(final Vector4l v) {
        return zxwy(null, v);
    }

    public static Vector4f zxwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f zxwy(final Vector4f v) {
        return zxwy(null, v);
    }

    public static Vector4l zxwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l zxwz(final Vector4l v) {
        return zxwz(null, v);
    }

    public static Vector4f zxwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f zxwz(final Vector4f v) {
        return zxwz(null, v);
    }

    public static Vector4l zxxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l zxxw(final Vector4l v) {
        return zxxw(null, v);
    }

    public static Vector4f zxxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f zxxw(final Vector4f v) {
        return zxxw(null, v);
    }

    public static Vector4l zxxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zxxx(final Vector3l v) {
        return zxxx(null, v);
    }

    public static Vector4l zxxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zxxx(final Vector4l v) {
        return zxxx(null, v);
    }

    public static Vector4f zxxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zxxx(final Vector3f v) {
        return zxxx(null, v);
    }

    public static Vector4f zxxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zxxx(final Vector4f v) {
        return zxxx(null, v);
    }

    public static Vector4l zxxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zxxy(final Vector3l v) {
        return zxxy(null, v);
    }

    public static Vector4l zxxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zxxy(final Vector4l v) {
        return zxxy(null, v);
    }

    public static Vector4f zxxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zxxy(final Vector3f v) {
        return zxxy(null, v);
    }

    public static Vector4f zxxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zxxy(final Vector4f v) {
        return zxxy(null, v);
    }

    public static Vector4l zxxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zxxz(final Vector3l v) {
        return zxxz(null, v);
    }

    public static Vector4l zxxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zxxz(final Vector4l v) {
        return zxxz(null, v);
    }

    public static Vector4f zxxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zxxz(final Vector3f v) {
        return zxxz(null, v);
    }

    public static Vector4f zxxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zxxz(final Vector4f v) {
        return zxxz(null, v);
    }

    public static Vector4l zxyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l zxyw(final Vector4l v) {
        return zxyw(null, v);
    }

    public static Vector4f zxyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f zxyw(final Vector4f v) {
        return zxyw(null, v);
    }

    public static Vector4l zxyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zxyx(final Vector3l v) {
        return zxyx(null, v);
    }

    public static Vector4l zxyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zxyx(final Vector4l v) {
        return zxyx(null, v);
    }

    public static Vector4f zxyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zxyx(final Vector3f v) {
        return zxyx(null, v);
    }

    public static Vector4f zxyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zxyx(final Vector4f v) {
        return zxyx(null, v);
    }

    public static Vector4l zxyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zxyy(final Vector3l v) {
        return zxyy(null, v);
    }

    public static Vector4l zxyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zxyy(final Vector4l v) {
        return zxyy(null, v);
    }

    public static Vector4f zxyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zxyy(final Vector3f v) {
        return zxyy(null, v);
    }

    public static Vector4f zxyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zxyy(final Vector4f v) {
        return zxyy(null, v);
    }

    public static Vector4l zxyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zxyz(final Vector3l v) {
        return zxyz(null, v);
    }

    public static Vector4l zxyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zxyz(final Vector4l v) {
        return zxyz(null, v);
    }

    public static Vector4f zxyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zxyz(final Vector3f v) {
        return zxyz(null, v);
    }

    public static Vector4f zxyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zxyz(final Vector4f v) {
        return zxyz(null, v);
    }

    public static Vector4l zxzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l zxzw(final Vector4l v) {
        return zxzw(null, v);
    }

    public static Vector4f zxzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f zxzw(final Vector4f v) {
        return zxzw(null, v);
    }

    public static Vector4l zxzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zxzx(final Vector3l v) {
        return zxzx(null, v);
    }

    public static Vector4l zxzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zxzx(final Vector4l v) {
        return zxzx(null, v);
    }

    public static Vector4f zxzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zxzx(final Vector3f v) {
        return zxzx(null, v);
    }

    public static Vector4f zxzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zxzx(final Vector4f v) {
        return zxzx(null, v);
    }

    public static Vector4l zxzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zxzy(final Vector3l v) {
        return zxzy(null, v);
    }

    public static Vector4l zxzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zxzy(final Vector4l v) {
        return zxzy(null, v);
    }

    public static Vector4f zxzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zxzy(final Vector3f v) {
        return zxzy(null, v);
    }

    public static Vector4f zxzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zxzy(final Vector4f v) {
        return zxzy(null, v);
    }

    public static Vector4l zxzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zxzz(final Vector3l v) {
        return zxzz(null, v);
    }

    public static Vector4l zxzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zxzz(final Vector4l v) {
        return zxzz(null, v);
    }

    public static Vector4f zxzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zxzz(final Vector3f v) {
        return zxzz(null, v);
    }

    public static Vector4f zxzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.x; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zxzz(final Vector4f v) {
        return zxzz(null, v);
    }

    public static Vector4l zyww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l zyww(final Vector4l v) {
        return zyww(null, v);
    }

    public static Vector4f zyww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f zyww(final Vector4f v) {
        return zyww(null, v);
    }

    public static Vector4l zywx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l zywx(final Vector4l v) {
        return zywx(null, v);
    }

    public static Vector4f zywx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f zywx(final Vector4f v) {
        return zywx(null, v);
    }

    public static Vector4l zywy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l zywy(final Vector4l v) {
        return zywy(null, v);
    }

    public static Vector4f zywy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f zywy(final Vector4f v) {
        return zywy(null, v);
    }

    public static Vector4l zywz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l zywz(final Vector4l v) {
        return zywz(null, v);
    }

    public static Vector4f zywz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f zywz(final Vector4f v) {
        return zywz(null, v);
    }

    public static Vector4l zyxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l zyxw(final Vector4l v) {
        return zyxw(null, v);
    }

    public static Vector4f zyxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f zyxw(final Vector4f v) {
        return zyxw(null, v);
    }

    public static Vector4l zyxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zyxx(final Vector3l v) {
        return zyxx(null, v);
    }

    public static Vector4l zyxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zyxx(final Vector4l v) {
        return zyxx(null, v);
    }

    public static Vector4f zyxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zyxx(final Vector3f v) {
        return zyxx(null, v);
    }

    public static Vector4f zyxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zyxx(final Vector4f v) {
        return zyxx(null, v);
    }

    public static Vector4l zyxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zyxy(final Vector3l v) {
        return zyxy(null, v);
    }

    public static Vector4l zyxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zyxy(final Vector4l v) {
        return zyxy(null, v);
    }

    public static Vector4f zyxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zyxy(final Vector3f v) {
        return zyxy(null, v);
    }

    public static Vector4f zyxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zyxy(final Vector4f v) {
        return zyxy(null, v);
    }

    public static Vector4l zyxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zyxz(final Vector3l v) {
        return zyxz(null, v);
    }

    public static Vector4l zyxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zyxz(final Vector4l v) {
        return zyxz(null, v);
    }

    public static Vector4f zyxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zyxz(final Vector3f v) {
        return zyxz(null, v);
    }

    public static Vector4f zyxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zyxz(final Vector4f v) {
        return zyxz(null, v);
    }

    public static Vector4l zyyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l zyyw(final Vector4l v) {
        return zyyw(null, v);
    }

    public static Vector4f zyyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f zyyw(final Vector4f v) {
        return zyyw(null, v);
    }

    public static Vector4l zyyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zyyx(final Vector3l v) {
        return zyyx(null, v);
    }

    public static Vector4l zyyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zyyx(final Vector4l v) {
        return zyyx(null, v);
    }

    public static Vector4f zyyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zyyx(final Vector3f v) {
        return zyyx(null, v);
    }

    public static Vector4f zyyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zyyx(final Vector4f v) {
        return zyyx(null, v);
    }

    public static Vector4l zyyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zyyy(final Vector3l v) {
        return zyyy(null, v);
    }

    public static Vector4l zyyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zyyy(final Vector4l v) {
        return zyyy(null, v);
    }

    public static Vector4f zyyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zyyy(final Vector3f v) {
        return zyyy(null, v);
    }

    public static Vector4f zyyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zyyy(final Vector4f v) {
        return zyyy(null, v);
    }

    public static Vector4l zyyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zyyz(final Vector3l v) {
        return zyyz(null, v);
    }

    public static Vector4l zyyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zyyz(final Vector4l v) {
        return zyyz(null, v);
    }

    public static Vector4f zyyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zyyz(final Vector3f v) {
        return zyyz(null, v);
    }

    public static Vector4f zyyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zyyz(final Vector4f v) {
        return zyyz(null, v);
    }

    public static Vector4l zyzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l zyzw(final Vector4l v) {
        return zyzw(null, v);
    }

    public static Vector4f zyzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f zyzw(final Vector4f v) {
        return zyzw(null, v);
    }

    public static Vector4l zyzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zyzx(final Vector3l v) {
        return zyzx(null, v);
    }

    public static Vector4l zyzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zyzx(final Vector4l v) {
        return zyzx(null, v);
    }

    public static Vector4f zyzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zyzx(final Vector3f v) {
        return zyzx(null, v);
    }

    public static Vector4f zyzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zyzx(final Vector4f v) {
        return zyzx(null, v);
    }

    public static Vector4l zyzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zyzy(final Vector3l v) {
        return zyzy(null, v);
    }

    public static Vector4l zyzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zyzy(final Vector4l v) {
        return zyzy(null, v);
    }

    public static Vector4f zyzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zyzy(final Vector3f v) {
        return zyzy(null, v);
    }

    public static Vector4f zyzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zyzy(final Vector4f v) {
        return zyzy(null, v);
    }

    public static Vector4l zyzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zyzz(final Vector3l v) {
        return zyzz(null, v);
    }

    public static Vector4l zyzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zyzz(final Vector4l v) {
        return zyzz(null, v);
    }

    public static Vector4f zyzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zyzz(final Vector3f v) {
        return zyzz(null, v);
    }

    public static Vector4f zyzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.y; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zyzz(final Vector4f v) {
        return zyzz(null, v);
    }

    public static Vector4l zzww(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4l zzww(final Vector4l v) {
        return zzww(null, v);
    }

    public static Vector4f zzww(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.w;
        return o;
    }
    public static Vector4f zzww(final Vector4f v) {
        return zzww(null, v);
    }

    public static Vector4l zzwx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4l zzwx(final Vector4l v) {
        return zzwx(null, v);
    }

    public static Vector4f zzwx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.x;
        return o;
    }
    public static Vector4f zzwx(final Vector4f v) {
        return zzwx(null, v);
    }

    public static Vector4l zzwy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4l zzwy(final Vector4l v) {
        return zzwy(null, v);
    }

    public static Vector4f zzwy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.y;
        return o;
    }
    public static Vector4f zzwy(final Vector4f v) {
        return zzwy(null, v);
    }

    public static Vector4l zzwz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4l zzwz(final Vector4l v) {
        return zzwz(null, v);
    }

    public static Vector4f zzwz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.w; o.w = v.z;
        return o;
    }
    public static Vector4f zzwz(final Vector4f v) {
        return zzwz(null, v);
    }

    public static Vector4l zzxw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4l zzxw(final Vector4l v) {
        return zzxw(null, v);
    }

    public static Vector4f zzxw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.w;
        return o;
    }
    public static Vector4f zzxw(final Vector4f v) {
        return zzxw(null, v);
    }

    public static Vector4l zzxx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zzxx(final Vector3l v) {
        return zzxx(null, v);
    }

    public static Vector4l zzxx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4l zzxx(final Vector4l v) {
        return zzxx(null, v);
    }

    public static Vector4f zzxx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zzxx(final Vector3f v) {
        return zzxx(null, v);
    }

    public static Vector4f zzxx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.x;
        return o;
    }
    public static Vector4f zzxx(final Vector4f v) {
        return zzxx(null, v);
    }

    public static Vector4l zzxy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zzxy(final Vector3l v) {
        return zzxy(null, v);
    }

    public static Vector4l zzxy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4l zzxy(final Vector4l v) {
        return zzxy(null, v);
    }

    public static Vector4f zzxy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zzxy(final Vector3f v) {
        return zzxy(null, v);
    }

    public static Vector4f zzxy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.y;
        return o;
    }
    public static Vector4f zzxy(final Vector4f v) {
        return zzxy(null, v);
    }

    public static Vector4l zzxz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zzxz(final Vector3l v) {
        return zzxz(null, v);
    }

    public static Vector4l zzxz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4l zzxz(final Vector4l v) {
        return zzxz(null, v);
    }

    public static Vector4f zzxz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zzxz(final Vector3f v) {
        return zzxz(null, v);
    }

    public static Vector4f zzxz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.x; o.w = v.z;
        return o;
    }
    public static Vector4f zzxz(final Vector4f v) {
        return zzxz(null, v);
    }

    public static Vector4l zzyw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4l zzyw(final Vector4l v) {
        return zzyw(null, v);
    }

    public static Vector4f zzyw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.w;
        return o;
    }
    public static Vector4f zzyw(final Vector4f v) {
        return zzyw(null, v);
    }

    public static Vector4l zzyx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zzyx(final Vector3l v) {
        return zzyx(null, v);
    }

    public static Vector4l zzyx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4l zzyx(final Vector4l v) {
        return zzyx(null, v);
    }

    public static Vector4f zzyx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zzyx(final Vector3f v) {
        return zzyx(null, v);
    }

    public static Vector4f zzyx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.x;
        return o;
    }
    public static Vector4f zzyx(final Vector4f v) {
        return zzyx(null, v);
    }

    public static Vector4l zzyy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zzyy(final Vector3l v) {
        return zzyy(null, v);
    }

    public static Vector4l zzyy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4l zzyy(final Vector4l v) {
        return zzyy(null, v);
    }

    public static Vector4f zzyy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zzyy(final Vector3f v) {
        return zzyy(null, v);
    }

    public static Vector4f zzyy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.y;
        return o;
    }
    public static Vector4f zzyy(final Vector4f v) {
        return zzyy(null, v);
    }

    public static Vector4l zzyz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zzyz(final Vector3l v) {
        return zzyz(null, v);
    }

    public static Vector4l zzyz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4l zzyz(final Vector4l v) {
        return zzyz(null, v);
    }

    public static Vector4f zzyz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zzyz(final Vector3f v) {
        return zzyz(null, v);
    }

    public static Vector4f zzyz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.y; o.w = v.z;
        return o;
    }
    public static Vector4f zzyz(final Vector4f v) {
        return zzyz(null, v);
    }

    public static Vector4l zzzw(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4l zzzw(final Vector4l v) {
        return zzzw(null, v);
    }

    public static Vector4f zzzw(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.w;
        return o;
    }
    public static Vector4f zzzw(final Vector4f v) {
        return zzzw(null, v);
    }

    public static Vector4l zzzx(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zzzx(final Vector3l v) {
        return zzzx(null, v);
    }

    public static Vector4l zzzx(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4l zzzx(final Vector4l v) {
        return zzzx(null, v);
    }

    public static Vector4f zzzx(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zzzx(final Vector3f v) {
        return zzzx(null, v);
    }

    public static Vector4f zzzx(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.x;
        return o;
    }
    public static Vector4f zzzx(final Vector4f v) {
        return zzzx(null, v);
    }

    public static Vector4l zzzy(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zzzy(final Vector3l v) {
        return zzzy(null, v);
    }

    public static Vector4l zzzy(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4l zzzy(final Vector4l v) {
        return zzzy(null, v);
    }

    public static Vector4f zzzy(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zzzy(final Vector3f v) {
        return zzzy(null, v);
    }

    public static Vector4f zzzy(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.y;
        return o;
    }
    public static Vector4f zzzy(final Vector4f v) {
        return zzzy(null, v);
    }

    public static Vector4l zzzz(Vector4l o, final Vector3l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zzzz(final Vector3l v) {
        return zzzz(null, v);
    }

    public static Vector4l zzzz(Vector4l o, final Vector4l v) {
        if (o == null) { o = new Vector4l(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4l zzzz(final Vector4l v) {
        return zzzz(null, v);
    }

    public static Vector4f zzzz(Vector4f o, final Vector3f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zzzz(final Vector3f v) {
        return zzzz(null, v);
    }

    public static Vector4f zzzz(Vector4f o, final Vector4f v) {
        if (o == null) { o = new Vector4f(); }
        o.x = v.z; o.y = v.z; o.z = v.z; o.w = v.z;
        return o;
    }
    public static Vector4f zzzz(final Vector4f v) {
        return zzzz(null, v);
    }

}
