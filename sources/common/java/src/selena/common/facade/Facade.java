package selena.common.facade;

public abstract class Facade {
    private final FacadeManager mManager;

    public Facade(final FacadeManager manager) {
        // To make reflection easier, we ensures that all the
        // subclasses agree on this common constructor.
        mManager = manager;
    }

    /** Invoked when the receiver is shut down. */
    public abstract void shutdown();

    /**
     * @return the facade manager which owns this facade
     */
    public FacadeManager getManager() {
        return mManager;
    }
}
