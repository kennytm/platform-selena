package selena.app;

import static selena.common.math.Vec.Vec4f;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentLinkedQueue;

import ni.Kronos.IKronosEntitySink;
import ni.Kronos.IKronosEntitySinkImpl;
import ni.aglGraphics.IGraphics;
import ni.aglGraphics.IGraphicsContext;
import ni.aglGraphics.aglGraphics;
import ni.aglMath.IMath;
import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.EOSWindowShowFlags;
import ni.aglSystem.IClock;
import ni.aglSystem.IConsole;
import ni.aglSystem.IDataTable;
import ni.aglSystem.IHString;
import ni.aglSystem.IMessage;
import ni.aglSystem.IMessageSink;
import ni.aglSystem.IMessageSinkImpl;
import ni.aglSystem.IOSWindow;
import ni.aglSystem.IResources;
import ni.aglSystem.IScriptingHost;
import ni.aglSystem.IScriptingHostImpl;
import ni.aglSystem.ISystem;
import ni.aglSystem.Lib;
import ni.aglUI.EFontFormatFlags;
import ni.aglUI.EUIMessage;
import ni.aglUI.IFont;
import ni.aglUI.IInputActionMap;
import ni.aglUI.IRmlNodeSink;
import ni.aglUI.IRmlNodeSinkImpl;
import ni.aglUI.IUI;
import ni.aglUI.IUIContext;
import ni.aglUI.IVGCanvas;
import ni.aglUI.IWidget;
import ni.aglUI.IWidgetSink;
import ni.aglUI.IWidgetSinkImpl;
import ni.aglUI.aglUI;
import ni.types.IUnknown;
import ni.types.ULColor;
import ni.types.UUID;
import ni.types.Vector4f;

import org.apache.commons.lang3.Validate;

import selena.SelenaException;
import selena.common.facade.FacadeException;
import selena.common.facade.FacadeManager;
import selena.common.facade.facade.SystemFacade;
import selena.common.util.Log;
import selena.script.VM;
import selena.util.DataTable;
import selena.util.NewInstances;
import selena.util.Strings;

public abstract class BaseAppWindow implements IMessageSinkImpl, IWidgetSinkImpl {
    protected static ISystem sys = Lib.getSystem();
    protected static IClock clock = sys.getClock();

    protected VM _vm;
    protected IConsole _console;
    protected IResources _resources;
    protected IMath _math;
    protected AppConfig _config;
    protected IOSWindow _wnd;
    protected IGraphics _graphics;
    protected IGraphicsContext _graphicsContext;
    protected IUI _ui;
    protected IUIContext _uiContext;
    protected IWidget _rootWidget;
    protected IWidgetSink _widgetSink;
    protected AppEventHandler _eventHandler;
    protected Map<String,IFont> _fonts;
    protected ConcurrentLinkedQueue<Runnable> _queue = new ConcurrentLinkedQueue<Runnable>();
    protected FacadeManager _facadeManager;

    protected boolean isDisposed = false;

    protected BaseAppWindow(final AppEventHandler eventHandler) {
        Validate.notNull(eventHandler);
        _eventHandler = eventHandler;
    }

    /**
     * Construct the AppWindow should be called in the constructor of the
     * implementation class
     * 
     * @throws SelenaException
     * @throws FileNotFoundException
     */
    protected final void construct() throws SelenaException, FileNotFoundException {
        // Init VM
        _vm = new VM();

        // Init config
        _config = createAppConfig();
        _eventHandler.onInitConfig(this, _config);

        // Init Resources       
        _resources = Lib.getResources();
        for (final String s : _config.dataSources) {
            _resources.addSource(s);
        }

        // Init console
        _console = Lib.getConsole();
        _console.addScriptingHost(Lib.hstr("aq"), _vm.getScriptingHost());

        // Init math
        _math = ni.aglMath.aglMath.get();
        if (_math == null)
            throw new SelenaException("Can't create the Math object !");

        // Init graphics
        _graphics = aglGraphics.createGraphics();
        if (_graphics == null)
            throw new SelenaException();

        // Init window
        initWindowAndGraphicsContext();
        assert _wnd != null;
        assert _graphicsContext != null;

        // Init UI      
        _ui = aglUI.createUI(_graphics);

        // Load fonts
        _fonts = new HashMap<String,IFont>();
        _ui.registerSystemFonts();
        for (final Entry<String,String> e : _config.fonts.entrySet()) {
            final IFont f = _ui.loadFont(Lib.hstr(e.getValue()));
            if (f != null) {
                _fonts.put(
                        e.getKey(),
                        f.createRegisteredInstance(Lib.hstr(e.getKey())));
            }
        }

        // Init UI context
        _uiContext = _ui.createUIContext(_graphicsContext);
        if (_config.hasTerminal) {
            _uiContext.initializeDefaultTerminal();
            for (final String s : _config.historyCommands) {
                _uiContext.getTerminal().addHistoryCommand(s);
            }
        }

        _widgetSink = IWidgetSink.impl(this);
        _rootWidget = _uiContext.getRootWidget();

        // Load action maps
        for (final String s : _config.inputActionMaps) {
            final IDataTable dt = DataTable.load("xml", s);
            for (int i = 0; i < dt.getNumChildren(); ++i) {
                final IDataTable cdt = dt.getChildFromIndex(i);
                final String amName = cdt.getString("name");
                if (_uiContext.getActionMapFromName(amName) != null) {
                    Log.d("# ActionMap [" +
                          amName +
                          "] already loaded (skipped) !");
                }
                else {
                    final IInputActionMap am = _uiContext.createActionMap(
                            "",
                            true);
                    if (!am.readDataTable(cdt))
                        throw new SelenaException(
                                "Can't read action map from datatable !");
                    Log.d("# ActionMap [" + amName + "] loaded !");
                }
            }
        }
        try {
            _uiContext.getActionMapFromName("Global").setEnabled(true);
        }
        catch (final Exception ex) {
            Log.w("Can't enable 'Global' input action map !", ex);
        }
        try {
            _uiContext.getActionMapFromName("UI_Standard").setEnabled(true);
        }
        catch (final Exception ex) {
            Log.w("Can't enable 'UI_Standard' input action map !", ex);
        }

        // Init window
        _wnd.getMessageTargets().addTargetSink(null, IMessageSink.impl(this));
        _wnd.setClearColor(0);
        _wnd.setRefreshTimer(_config.windowRefreshTimer);
        _wnd.setShow(EOSWindowShowFlags.Show | _config.windowShowFlags);
        _wnd.setFocus();
        _wnd.activateWindow();

        // Set the global instances        
        sys.setGlobalInstance("aglSystem.ScriptVM", _vm.getScriptingHost());
        sys.setGlobalInstance("aglMath.Math", _math);
        sys.setGlobalInstance("aglUI.UI", _ui);
        sys.setGlobalInstance("iUIContext", _uiContext);
        sys.setGlobalInstance("iOSWindow", _wnd);
        sys.setGlobalInstance("aglGraphics.Graphics", _graphics);
        sys.setGlobalInstance("iGraphicsContext", _graphicsContext);

        // Create the facade manager
        _facadeManager = createFacadeManager();

        // Register us as a facade scripting host
        {
            final IScriptingHost impl = IScriptingHost.impl(new ScriptingHost());
            _console.addScriptingHost(Lib.hstr("facade"), impl);
        }

        // Call the startup method
        if (!_eventHandler.onStartup(this)) {
            throw new SelenaException("Application onStartup failed !");
        }

        // Sink attached is the last thing that we do, so that the SinkAttached message is sent when everything
        // has been initialized.
        _rootWidget.addSink(_widgetSink);
        _uiContext.getDesktopWidget().addSink(_widgetSink);
    }

    @Override
    protected void finalize() {
        if (!isDisposed) {
            Log.e("Window should have been disposed !");
            dispose();
        }
    }

    /**
     * Dispose the window object, called when the window is destroyed.
     * <p>
     * You shouldn't call this method to close the application window instead
     * use {@link #close} to do that.
     */
    protected void dispose() {
        if (!isDisposed) {
            _eventHandler.onShutdown(this);
            _eventHandler = null;
            for (final IFont f : _fonts.values()) {
                if (f != null) {
                    f.dispose();
                }
            }
            _fonts.clear();
            if (_vm != null) {
                _vm.dispose();
                _vm = null;
            }
            if (_rootWidget != null) {
                _rootWidget.dispose();
                _rootWidget = null;
            }
            if (_widgetSink != null) {
                _widgetSink.invalidate();
                _widgetSink.dispose();
                _widgetSink = null;
            }
            if (_uiContext != null) {
                _uiContext.invalidate();
                _uiContext.dispose();
                _uiContext = null;
            }
            if (_ui != null) {
                _ui.invalidate();
                _ui.dispose();
                _ui = null;
            }
            if (_graphicsContext != null) {
                _graphicsContext.invalidate();
                _graphicsContext.dispose();
                _graphicsContext = null;
            }
            if (_graphics != null) {
                _graphics.invalidate();
                _graphics.dispose();
                _graphics = null;
            }
            if (_wnd != null) {
                _wnd.invalidate();
                _wnd.dispose();
                _wnd = null;
            }
            if (_console != null) {
                _console.invalidate();
                _console.dispose();
                _console = null;
            }
            if (_math != null) {
                _math.invalidate();
                _math.dispose();
                _math = null;
            }
            if (_resources != null) {
                _resources.invalidate();
                _resources.dispose();
                _resources = null;
            }
            isDisposed = true;
        }
    }

    /**
     * Request the OS Window to close the application.
     * <p>
     * This method is the one to use to cleanly 'exit' the application.
     */
    public void close() {
        _wnd.setRequestedClose(true);
    }

    /**
     * Create the default App config.
     * <p>
     * This is called from the constructor so you can't rely on your class's
     * members in there since they won't be initialized yet.
     */
    protected abstract AppConfig createAppConfig() throws SelenaException;

    /**
     * Create an OS dependent window.
     * <p>
     * This is called from the constructor so you can't rely on your class's
     * members in there since they won't be initialized yet.
     * 
     * @return the window created
     */
    protected abstract void initWindowAndGraphicsContext() throws SelenaException;

    /**
     * Create the facade manager.
     * 
     * @throws FacadeException
     */
    protected FacadeManager createFacadeManager() throws FacadeException {
        final FacadeManager fm = new FacadeManager(this);
        fm.registerReceiver(SystemFacade.class);
        return fm;
    }

    /**
     * Queue a runnable to be run on the main UI thread
     */
    public void queue(final Runnable runnable) {
        _queue.add(runnable);
    }

    /**
     * Should be called to update the window
     */
    protected boolean update() {
        // update the Java message queue
        {
            Runnable runnable = _queue.poll();
            while (runnable != null) {
                try {
                    runnable.run();
                }
                catch (final Throwable t) {
                    Log.e("Exception in queue", t);
                }
                runnable = _queue.poll();
            }
        }
        // update the window... dispatch queue input events, etc...
        _wnd.updateWindow(true);
        // update the system message queue
        sys.getAndDispatchAllCurrentThreadMessages();
        if (_wnd.getIsActive()) {
            _graphicsContext.changeWindow(_wnd);
            _console.popAndRunAllCommands();
            final float frameTime = clock.getFrameTime();
            _graphics.updateMaterials(frameTime);
            _uiContext.update(frameTime);
            clock.updateFrame();
        }
        return !_wnd.getRequestedClose();
    }

    private void doDraw() {
        if ((_uiContext == null) || (_graphicsContext == null) || !_graphicsContext.getCanUse())
            return;
        _uiContext.draw();
        _graphicsContext.display(0, null);
    }

    /**
     * Should be called to draw the window's content
     */
    protected void draw() {
        if (_wnd.getIsActive()) {
            doDraw();
        }
    }

    final public ISystem getSystem() {
        return sys;
    }

    final public VM getScriptVM() {
        return _vm;
    }

    final public IConsole getConsole() {
        return _console;
    }

    final public IResources getResources() {
        return _resources;
    }

    final public AppConfig getConfig() {
        return _config;
    }

    final public IClock getClock() {
        return clock;
    }

    final public IOSWindow getOSWindow() {
        return _wnd;
    }

    final public IGraphics getGraphics() {
        return _graphics;
    }

    final public IGraphicsContext getGraphicsContext() {
        return _graphicsContext;
    }

    final public IUI getUI() {
        return _ui;
    }

    final public IUIContext getUIContext() {
        return _uiContext;
    }

    final public AppEventHandler getEventHandler() {
        return _eventHandler;
    }

    final public FacadeManager getFacadeManager() {
        return _facadeManager;
    }

    @Override
    public boolean onMessageSink(final IMessage msg) {
        try {
            if (_eventHandler.onWindowMessage(this, msg))
                return true;
        }
        catch (final Exception ex) {
            Log.e("Window Message Handler Exception : " + Strings.fromMessage(_wnd, msg), ex);
            return false;
        }

        final int msgId = msg.getID();
        if (msgId == EOSWindowMessage.Close) {
            Log.i("Default Close");
            _wnd.setRequestedClose(true);
            return true;
        }
        else if (msgId == EOSWindowMessage.SwitchIn) {
            _wnd.setRefreshTimer(_config.windowRefreshTimer);
        }
        else if (msgId == EOSWindowMessage.SwitchOut) {
            _wnd.setRefreshTimer(-1);
        }
        else if (msgId == EOSWindowMessage.EraseBackground) {
            return true;
        }
        else if (msgId == EOSWindowMessage.Paint) {
            if (!_wnd.getIsActive()) {
                doDraw();
            }
            return true;
        }

        _uiContext.sendWindowMessage(msgId, msg.getA(), msg.getB());
        return false;
    }

    @Override
    public boolean messageHandler(final IWidget w, final int msg, final Object a, final Object b)
    {
        try {
            if (w == null || w.equals(_rootWidget)) {
                final boolean ret = _eventHandler.onRootMessage(this, w, msg, a, b);
                if (!ret) {
                    if (msg == EUIMessage.ContextAfterDraw) {
                        if (_config.drawFps) {
                            final IVGCanvas canvas = IVGCanvas.query(a);
                            final Vector4f vp = _graphicsContext.getMetrics().getRectangle();
                            final IFont font = _uiContext.getRootWidget().getFont();
                            final float fh = font.getHeight();
                            final int wasBgColor = font.getBackgroundColor();
                            font.setBackgroundColor(ULColor.build(0, 0, 0, 255));
                            final Vector4f posRect = Vec4f(5, vp.getHeight() - fh - 5, 0, 0);
                            final String fpsText = "FPS:" +
                                                   clock.getAverageFrameRate() +
                                                   " (" +
                                                   ((int)(clock.getFrameTime() * 1000.0)) +
                                                   ")";
                            canvas.blitText(font, posRect, EFontFormatFlags.Left, fpsText);
                            font.setBackgroundColor(wasBgColor);
                            canvas.flush();
                        }
                    }
                }
                return ret;
            }
            else {
                return _eventHandler.onDesktopMessage(this, w, msg, a, b);
            }
        }
        catch (final Exception ex) {
            Log.e("Widget Message Handler Exception : " + Strings.fromMessage(null, w, msg, a, b), ex);
            if (msg == EUIMessage.SinkAttached)
                throw new RuntimeException(ex);
            return false;
        }
    }

    private static final IHString hstrKronosEntity = Lib.hstr("KronosEntity");
    private static final IHString hstrUIWidget = Lib.hstr("UIWidget");
    private static final IHString hstrRmlNode = Lib.hstr("RmlNode");

    private class ScriptingHost implements IScriptingHostImpl {

        @Override
        public boolean evalString(final IHString ahspContext, final String aaszCode) {
            try {
                final Object r = _facadeManager.callCommand(aaszCode);
                final String ret = (r == null) ? "" : r.toString();
                _console.setVariable("result", ret);
                return true;
            }
            catch (final Throwable t) {
                _console.setVariable("result", null);
                _console.setVariable("error", Strings.getStackTrace(t));
                return false;
            }
        }

        @Override
        public boolean canEvalImpl(final IHString ahspContext, final IHString ahspCodeResource)
        {
            try {
                final String className = ahspCodeResource.getChars();
                Validate.matchesPattern(className, "([\\w]+\\.)*[\\w]+");
                return true;
            }
            catch (final Throwable t) {
                return false;
            }
        }

        @Override
        public IUnknown evalImpl(final IHString ahspContext, final IHString ahspCodeResource, final UUID aIID)
        {
            final String className = ahspCodeResource.getChars();
            try {
                Validate.matchesPattern(className, "([\\w]+\\.)*[\\w]+");
                if (hstrUIWidget.equals(ahspContext)) {
                    final Object o = NewInstances.newInstance(className);
                    return IWidgetSink.impl((IWidgetSinkImpl)o);
                }
                else if (hstrRmlNode.equals(ahspContext)) {
                    final Object o = NewInstances.newInstance(className);
                    return IRmlNodeSink.impl((IRmlNodeSinkImpl)o);
                }
                else if (hstrKronosEntity.equals(ahspContext)) {
                    final Object o = NewInstances.newInstance(className);
                    return IKronosEntitySink.impl((IKronosEntitySinkImpl)o);
                }
                else {
                    throw new IllegalArgumentException("Unknown context : " + ahspContext.getChars());
                }
            }
            catch (final Throwable t) {
                Log.e("Can't create an instance of :" + className, t);
            }
            return null;
        }

        @Override
        public void service(final boolean abForceGC) {
        }
    }
}
