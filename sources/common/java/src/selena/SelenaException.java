package selena;

public class SelenaException extends Exception {
    private static final long serialVersionUID = 1L;

    public SelenaException() {
        super();
    }

    public SelenaException(final String msg) {
        super(msg);
    }

    public SelenaException(final String msg, final Throwable cause) {
        super(msg, cause);
    }

    public SelenaException(final Throwable cause) {
        super(cause);
    }
}
