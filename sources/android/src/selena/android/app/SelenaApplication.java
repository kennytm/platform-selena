package selena.android.app;

import java.io.File;

import ni.aglSystem.IResources;
import ni.aglSystem.Lib;
import selena.common.util.Log;
import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;

/**
 * Selena Application for Android
 * <p>
 * This class takes care of initializing all the low-level AOM services
 * automatically, to use it in your Android application just add
 * {@code <application android:name="selena.android.app.SelenaApplication" ... /> }
 * in your AndroidManifest.xml.
 *
 * @author Pierre Renaux
 */
public class SelenaApplication extends Application implements Log.ISink {
    @Override
    public void onCreate() {
        super.onCreate();
        Log.registerSink(this);
        initContextProperties(this);
    }

    /**
     * Initialize the aglSystem for the specified context.
     * <p>
     * Ideally you shouldn't be using this method directly but instead use
     * {@link SelenaApplication} as you Android application.
     *
     * @param ctx
     */
    static public void initContextProperties(final Context ctx) {

        final ApplicationInfo ai = ctx.getApplicationInfo();
        final File homeDir = ctx.getDir("home", Context.MODE_PRIVATE);
        final String appDir = homeDir.getParent();
        final String nativeLibDir = appDir + "/lib";
        System.setProperty(Lib.PROPERTY_ANDROID_APPDIR, appDir);
        System.setProperty(Lib.PROPERTY_ANDROID_HOMEDIR, homeDir.getAbsolutePath());
        System.setProperty(Lib.PROPERTY_ANDROID_APPDATADIR, ctx.getDir("appdata", Context.MODE_PRIVATE).getAbsolutePath());
        System.setProperty(Lib.PROPERTY_ANDROID_LIBDIR, nativeLibDir);

        // Add the app's APK to the resource manager
        final IResources res = Lib.getResources();
        res.addSource(ai.publicSourceDir);
        res.addSource("assets/");
    }

    @Override
    public void log(final int type, final String tag, final String file, final String func, final int line, final String msg)
    {
        int priority = android.util.Log.VERBOSE;
        switch (type) {
        case Log.Debug:
            priority = android.util.Log.DEBUG;
            break;
        case Log.Info:
            priority = android.util.Log.INFO;
            break;
        case Log.Warning:
            priority = android.util.Log.WARN;
            break;
        case Log.Error:
            priority = android.util.Log.ERROR;
            break;
        }
        android.util.Log.println(priority, tag, msg);
        // This is always using the ERROR priority so that I can double click on it in Eclipse to jump to the code...
        if (priority < android.util.Log.WARN)
            priority = android.util.Log.WARN;
        android.util.Log.println(priority, tag, "    at " + tag + "." + func + "(" + file + ":" + line + ")");
    }
}
