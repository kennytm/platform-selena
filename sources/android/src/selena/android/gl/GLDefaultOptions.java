package selena.android.gl;

public class GLDefaultOptions {
    public static boolean USE_AA = false;
    public static boolean TRANSLUCENT_WINDOW = false;
    public static boolean USE_DEPTH_BUFFER_24 = false;
    public static boolean USE_DEFAULT_GL_CONTEXT = false;
    public static boolean USE_GL_POS = false;
    public static boolean USE_GL_LOWMEM = false;
    public static boolean USE_GL_ES2 = false;
    public static int     RESOLUTION_SCALE = 1;
    public static int     DISPATCH_DRAW_WAIT_FOR_NFRAMES_COUNT = 1;

    public static boolean LOG_THREADS = true;
    public static boolean LOG_SURFACE = true;
    public static boolean LOG_RENDERER = false;

    // Work-around for bug 2263168
    public static boolean DRAW_TWICE_AFTER_SIZE_CHANGED = true;
}
