package selena.common.facade.command;

import selena.common.json.JSONException;
import selena.common.json.JSONObject;

/**
 * Represents a JSON RPC result.
 */
public class JsonCommandResult {

    private JsonCommandResult() {
        // Utility class.
    }

    public static JSONObject empty(final int id) throws JSONException {
        final JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("result", JSONObject.NULL);
        json.put("error", JSONObject.NULL);
        return json;
    }

    public static JSONObject result(final int id, final Object data) throws JSONException
    {
        final JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("result", JsonBuilder.build(data));
        json.put("error", JSONObject.NULL);
        return json;
    }

    public static JSONObject error(final int id, final Throwable t) throws JSONException {
        final JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("result", JSONObject.NULL);
        json.put("error", t.toString());
        return json;
    }
}
