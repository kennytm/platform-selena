package selena.common.math;

import ni.types.Vector2f;
import ni.types.Vector2l;
import ni.types.Vector4f;
import ni.types.Vector4l;

public class Rect extends Vec {
    protected Rect() {
    }

    //===========================================================================
    //
    // Rectf
    //
    //===========================================================================
    public static final Vector4f RECTF_NULL = Vec4f();

    public static Vector4f Rectf() {
        return new Vector4f();
    }

    public static Vector4f Rectf(final int x, final int y, final int width, final int height)
    {
        return (new Vector4f()).set(x, y, x + width, y + height);
    }

    public static Vector4f Rectf(final float x, final float y, final float width, final float height)
    {
        return (new Vector4f()).set(x, y, x + width, y + height);
    }

    public static Vector4f Rectf(final Object o) {
        return (new Vector4f()).set(o);
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4f move(Vector4f Out, final Vector4f V, final float x, final float y)
    {
        if (Out == null) {
            Out = new Vector4f();
        }
        Out.x = V.x + x;
        Out.z = V.z + x;
        Out.y = V.y + y;
        Out.w = V.w + y;
        return Out;
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4f move(final Vector4f Out, final Vector4f V, final Vector2f p) {
        return move(Out, V, p.x, p.y);
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4f move(final Vector4f V, final float x, final float y) {
        return move(V, V, x, y);
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4f move(final Vector4f V, final Vector2f p) {
        return move(V, V, p.x, p.y);
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4f moveTo(Vector4f Out, final Vector4f V, final float x, final float y)
    {
        if (Out == null) {
            Out = new Vector4f();
        }
        final float w = V.getWidth();
        final float h = V.getHeight();
        Out.x = x;
        Out.z = x + w;
        Out.y = y;
        Out.w = y + h;
        return Out;
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4f moveTo(final Vector4f Out, final Vector4f V, final Vector2f p)
    {
        return moveTo(Out, V, p.x, p.y);
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4f moveTo(final Vector4f V, final float x, final float y) {
        return moveTo(V, V, x, y);
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4f moveTo(final Vector4f V, final Vector2f p) {
        return moveTo(V, V, p.x, p.y);
    }

    public static boolean intersectRect(final Vector4f A, final Vector4f B) {
        if (B.getRight() < A.getLeft())
            return false;
        if (B.getBottom() < A.getTop())
            return false;
        if (B.getLeft() > A.getRight())
            return false;
        if (B.getTop() > A.getBottom())
            return false;
        return true;
    }

    public static boolean intersect(final Vector4f A, final float px, final float py) {
        return ((px >= A.getLeft()) && (px < A.getRight()) &&
                (py >= A.getTop()) && (py < A.getBottom()));
    }

    public static boolean intersect(final Vector4f A, final Vector2f aPoint) {
        return intersect(A, aPoint.x, aPoint.y);
    }

    public static Vector4f clipRectangle(Vector4f Out, final Vector4f A, final Vector4f aRect)
    {
        if (Out == null) {
            Out = new Vector4f();
        }
        Out.setLeft(
                aRect.getLeft() < A.getLeft() ? A.getLeft() :
                        (aRect.getLeft() > A.getRight()) ? A.getRight() : aRect.getLeft());
        Out.setRight(
                aRect.getRight() < A.getLeft() ? A.getLeft() :
                        (aRect.getRight() > A.getRight()) ? A.getRight() : aRect.getRight());
        Out.setTop(
                aRect.getTop() < A.getTop() ? A.getTop() :
                        (aRect.getTop() > A.getBottom()) ? A.getBottom() : aRect.getTop());
        Out.setBottom(
                aRect.getBottom() < A.getTop() ? A.getTop() :
                        (aRect.getBottom() > A.getBottom()) ? A.getBottom() : aRect.getBottom());
        return Out;
    }

    public static Vector4f getFrameCenter(Vector4f Out, final Vector4f A, final float aLeft, final float aRight, final float aTop, final float aBottom)
    {
        if (Out == null) {
            Out = new Vector4f();
        }
        Out.setLeft(A.getLeft() + aLeft);
        Out.setRight(A.getRight() - aRight);
        Out.setTop(A.getTop() + aTop);
        Out.setBottom(A.getBottom() - aBottom);
        return Out;
    }

    public static Vector4f getFrameCenter(final Vector4f Out, final Vector4f A, final Vector4f frame)
    {
        return getFrameCenter(Out, A, frame.getLeft(), frame.getRight(), frame.getTop(), frame.getBottom());
    }

    //===========================================================================
    //
    // Rectl
    //
    //===========================================================================
    public static final Vector4l RECTL_ZERO = Vec4l(0, 0, 0, 0);

    public static Vector4l Rectl() {
        return new Vector4l();
    }

    public static Vector4l Rectl(final int x, final int y, final int width, final int height)
    {
        return (new Vector4l()).set(x, y, x + width, y + height);
    }

    public static Vector4l Rectl(final float x, final float y, final float width, final float height)
    {
        return (new Vector4l()).set(x, y, x + width, y + height);
    }

    public static Vector4l Rectl(final Object o) {
        return (new Vector4l()).set(o);
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4l move(Vector4l Out, final Vector4l V, final int x, final int y)
    {
        if (Out == null) {
            Out = new Vector4l();
        }
        Out.x = V.x + x;
        Out.z = V.z + x;
        Out.y = V.y + y;
        Out.w = V.w + y;
        return Out;
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4l move(final Vector4l Out, final Vector4l V, final Vector2l p) {
        return move(Out, V, p.x, p.y);
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4l move(final Vector4l V, final int x, final int y) {
        return move(V, V, x, y);
    }

    /**
     * Move the rectangle of the specified offset.
     */
    public static Vector4l move(final Vector4l V, final Vector2l p) {
        return move(V, V, p.x, p.y);
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4l moveTo(Vector4l Out, final Vector4l V, final int x, final int y)
    {
        if (Out == null) {
            Out = new Vector4l();
        }
        final int w = V.getWidth();
        final int h = V.getHeight();
        Out.x = x;
        Out.z = x + w;
        Out.y = y;
        Out.w = y + h;
        return Out;
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4l moveTo(final Vector4l Out, final Vector4l V, final Vector2l p)
    {
        return moveTo(Out, V, p.x, p.y);
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4l moveTo(final Vector4l V, final int x, final int y) {
        return moveTo(V, V, x, y);
    }

    /**
     * Move the rectangle to the specified position.
     */
    public static Vector4l moveTo(final Vector4l V, final Vector2l p) {
        return moveTo(V, V, p.x, p.y);
    }

    public static boolean intersectRect(final Vector4l A, final Vector4l B) {
        if (B.getRight() < A.getLeft())
            return false;
        if (B.getBottom() < A.getTop())
            return false;
        if (B.getLeft() > A.getRight())
            return false;
        if (B.getTop() > A.getBottom())
            return false;
        return true;
    }

    public static boolean intersect(final Vector4l A, final int px, final int py) {
        return ((px >= A.getLeft()) && (px < A.getRight()) &&
                (py >= A.getTop()) && (py < A.getBottom()));
    }

    public static boolean intersect(final Vector4l A, final Vector2l aPoint) {
        return intersect(A, aPoint.x, aPoint.y);
    }

    public static Vector4l clipRectangle(Vector4l Out, final Vector4l A, final Vector4l aRect)
    {
        if (Out == null) {
            Out = new Vector4l();
        }
        Out.setLeft(
                aRect.getLeft() < A.getLeft() ? A.getLeft() :
                        (aRect.getLeft() > A.getRight()) ? A.getRight() : aRect.getLeft());
        Out.setRight(
                aRect.getRight() < A.getLeft() ? A.getLeft() :
                        (aRect.getRight() > A.getRight()) ? A.getRight() : aRect.getRight());
        Out.setTop(
                aRect.getTop() < A.getTop() ? A.getTop() :
                        (aRect.getTop() > A.getBottom()) ? A.getBottom() : aRect.getTop());
        Out.setBottom(
                aRect.getBottom() < A.getTop() ? A.getTop() :
                        (aRect.getBottom() > A.getBottom()) ? A.getBottom() : aRect.getBottom());
        return Out;
    }

    public static Vector4l getFrameCenter(Vector4l Out, final Vector4l A, final int aLeft, final int aRight, final int aTop, final int aBottom)
    {
        if (Out == null) {
            Out = new Vector4l();
        }
        Out.setLeft(A.getLeft() + aLeft);
        Out.setRight(A.getRight() - aRight);
        Out.setTop(A.getTop() + aTop);
        Out.setBottom(A.getBottom() - aBottom);
        return Out;
    }

    public static Vector4l getFrameCenter(final Vector4l Out, final Vector4l A, final Vector4l frame)
    {
        return getFrameCenter(Out, A, frame.getLeft(), frame.getRight(), frame.getTop(), frame.getBottom());
    }
}
