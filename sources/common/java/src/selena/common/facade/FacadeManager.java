package selena.common.facade;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Collection;
import java.util.HashMap;

import selena.common.facade.command.CommandException;
import selena.common.facade.command.MethodDescriptor;
import selena.common.json.JSONArray;
import selena.common.json.JSONObject;
import selena.common.util.Log;

public class FacadeManager {
    private final HashMap<Class<? extends Facade>,Facade> mReceivers;
    private final HashMap<String,MethodDescriptor> mKnownFacades = new HashMap<String,MethodDescriptor>();
    private final Object mContext;

    /**
     * @param aContext a context object that can be retrieve simply through the
     *            mManager.getContext() member of each Facade.
     */
    public FacadeManager(final Object aContext) {
        mContext = aContext;
        mReceivers = new HashMap<Class<? extends Facade>,Facade>();
    }

    public Object getContext() {
        return mContext;
    }

    public Object callCommand(String cmd) throws Throwable {
        final FacadeManager receiverManager = this;
        char inString = 0;
        String method = null;
        final JSONArray params = new JSONArray();
        int tokenStart = 0;
        cmd = cmd.trim();
        final CharacterIterator ci = new StringCharacterIterator(cmd);
        char ch = ci.first();
        // Log.d("--- COMMAND: " + cmd);
        while (true) {
            if (inString != 0) {
                if (ch == inString)
                    inString = 0;
            }
            else {
                if ((ch == '\'') || (ch == '"') || (ch == '`')) {
                    inString = ch;
                }
                else if ((ch == ' ') || (ch == CharacterIterator.DONE)) {
                    final int idx = ci.getIndex();
                    // Log.d("NEWTOK: " + idx + "(" + ch + ")");
                    if (ch == CharacterIterator.DONE) {
                        if (inString != 0) {
                            throw new CommandException("String not closed !");
                        }
                    }

                    final String p = cmd.substring(tokenStart, idx).trim();
                    if (p.length() > 0) {
                        if (method == null) {
                            method = p.toLowerCase();
                            // Log.d("METHOD: [" + method + "]");
                        }
                        else {
                            // Log.d("PARAM: [" + p + "]");
                            switch (p.charAt(0)) {
                            // object
                            case '{':
                                params.add(new JSONObject(p));
                                break;
                            // array
                            case '[':
                                params.add(new JSONArray(p));
                                break;
                            // string
                            case '`':
                            case '\'':
                            case '"':
                                params.add(p.substring(1, p.length() - 1));
                                break;
                            // number
                            default:
                                try {
                                    if (p.contains(".")) {
                                        params.add(new Double(p));
                                    }
                                    else {
                                        params.add(new Long(p));
                                    }
                                }
                                catch (final java.lang.NumberFormatException e) {
                                    // Push a string if can't be converted to a
                                    // number...
                                    params.add(p);
                                }
                                break;
                            }
                        }
                    }
                    tokenStart = ci.getIndex();
                    // Log.d("tokenStart: " + tokenStart);
                }
            }

            if (ch == CharacterIterator.DONE) {
                // Log.d("BREAK AT: " + ci.getIndex());
                break;
            }

            ch = ci.next();
        }

        final MethodDescriptor meth = receiverManager.getMethodDescriptor(method);
        if (meth == null) {
            throw new CommandException("Unknown RPC method: " + method);
        }

        return meth.invoke(receiverManager, params);
    }

    @SuppressWarnings("unchecked")
    public <T> T registerReceiver(final Class<? extends Facade> c) throws FacadeException
    {
        mReceivers.put(c, null);

        final Facade r = getReceiver(c);
        if (r == null)
            throw new RuntimeException("'" + c.getName() + "' is not a valid Facade, make sure the constructor is declared as public.");

        final Collection<MethodDescriptor> methodList = MethodDescriptor.collectFrom(c);
        for (final MethodDescriptor m : methodList) {
            final String commandName = m.getCallName().toLowerCase();
            if (mKnownFacades.containsKey(commandName)) {
                throw new FacadeException("An command with the name " + m.getCallName() + " is already known.");
            }
            mKnownFacades.put(commandName, m);
            // Log.d("Added RPC method : " + m.getCallName() + " ("+m.toString()+")");
        }

        return (T)r;
    }

    public void registerReceivers(final Collection<Class<? extends Facade>> classList) throws FacadeException
    {
        for (final Class<? extends Facade> c : classList) {
            registerReceiver(c);
        }
    }

    public Collection<Class<? extends Facade>> getRpcReceiverClasses() {
        return mReceivers.keySet();
    }

    private Facade get(final Class<? extends Facade> clazz) {
        Facade object = mReceivers.get(clazz);
        if (object != null) {
            return object;
        }

        Constructor<? extends Facade> constructor;
        try {
            constructor = clazz.getConstructor(getClass());
            object = constructor.newInstance(this);
            mReceivers.put(clazz, object);
        }
        catch (final Exception e) {
            Log.e(e.toString());
        }

        return object;
    }

    public <T extends Facade> T getReceiver(final Class<T> clazz) {
        final Facade receiver = get(clazz);
        return clazz.cast(receiver);
    }

    public MethodDescriptor getMethodDescriptor(final String methodName) {
        return mKnownFacades.get(methodName);
    }

    public final Object doInvoke(final Class<? extends Facade> clazz,
            final Method method,
            final Object[] args)
            throws Exception
    {
        final Facade object = get(clazz);
        return method.invoke(object, args);
    }

    public void shutdown() {
        for (final Facade receiver : mReceivers.values()) {
            if (receiver != null) {
                receiver.shutdown();
            }
        }
    }
}
