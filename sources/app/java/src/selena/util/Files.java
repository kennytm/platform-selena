package selena.util;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.nio.ByteBuffer;

import ni.aglSystem.EFileOpenMode;
import ni.aglSystem.IFile;
import ni.aglSystem.Lib;

public class Files extends selena.common.util.Files {
    protected Files() {
    }

    /**
     * Check whether the specified resource name can be found by the resource
     * manager.
     * 
     * @param aPath is the name of the resource to look for.
     * @param aBasePath aBasePath is an additional directory where the resource
     *            will be searched for, only the directory part of the path is
     *            used.
     * @return true if the resource can be found, false if not.
     */
    static public boolean exists(final String aPath, final String aBasePath) {
        return Lib.getResources().exists(aPath, aBasePath);
    }

    /**
     * Check whether the specified resource name can be found by the resource
     * manager.
     * 
     * @param aPath is the name of the resource to look for.
     * @return true if the resource can be found, false if not.
     */
    static public boolean exists(final String aPath) {
        return exists(aPath, null);
    }

    /**
     * Open a read-only file using the resource manager.
     * 
     * @param aPath is the name of the resource to open.
     * @param aBasePath is an additional directory where the resource will be
     *            searched for, only the directory part of the path is used.
     * @return The opened file.
     * @throws FileNotFoundException
     */
    static public IFile openRead(final String aPath, final String aBasePath) throws FileNotFoundException
    {
        final IFile fp = Lib.getResources().open(aPath, aBasePath);
        if (fp == null)
            throw new FileNotFoundException();
        return fp;
    }

    /**
     * Open a read-only file using the resource manager.
     * 
     * @param aPath
     * @return The opened file.
     * @throws FileNotFoundException
     */
    static public IFile openRead(final String aPath) throws FileNotFoundException {
        return openRead(aPath, null);
    }

    /**
     * Open a write-only file.
     * 
     * @param aPath the path where to write the file.
     * @return The opened filed.
     * @throws IOException if the file can't be created or overwritten.
     */
    static public IFile openWrite(final String aPath) throws IOException {
        final IFile fp = Lib.getSystem().fileOpen(aPath, EFileOpenMode.Write);
        if (fp == null)
            throw new IOException();
        return fp;
    }

    /**
     * Read the file in a byte array, from the current position to the end of
     * the file.
     * 
     * @param fp is the file to read from.
     * @return A new byte array containing the data read.
     * @throws EOFException if there is nothing to read (0 bytes left)
     * @throws InterruptedIOException if the read failed because the
     *             {@link IFile#read} method fails (aka doesn't read the
     *             expected number of bytes).
     */
    static byte[] readToByteArray(final IFile fp) throws EOFException, InterruptedIOException
    {
        final long readSize = fp.getSize() - fp.tell();
        if (readSize <= 0)
            throw new EOFException();
        final byte[] data = new byte[(int)readSize];
        if (fp.read(ByteBuffer.wrap(data), (int)readSize) != readSize)
            throw new InterruptedIOException();
        return data;
    }

    /**
     * Read a whole resource file in a byte array.
     * 
     * @param aPath is the path of the resource to read from.
     * @return A new byte array containing the data read.
     * @throws FileNotFoundException {@link #openRead(String)}
     * @throws EOFException {@link #readToByteArray(IFile)}
     * @throws InterruptedIOException {@link #readToByteArray(IFile)}
     */
    static byte[] readToByteArray(final String aPath) throws FileNotFoundException, EOFException, InterruptedIOException
    {
        final IFile fp = openRead(aPath);
        try {
            return readToByteArray(fp);
        }
        finally {
            fp.invalidate();
            fp.dispose();
        }
    }
}
