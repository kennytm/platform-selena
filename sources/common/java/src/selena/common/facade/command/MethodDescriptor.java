package selena.common.facade.command;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import selena.common.facade.Facade;
import selena.common.facade.FacadeManager;
import selena.common.facade.annotations.Command;
import selena.common.facade.annotations.Default;
import selena.common.facade.annotations.Optional;
import selena.common.facade.annotations.Param;
import selena.common.json.JSONArray;
import selena.common.json.JSONException;
import selena.common.util.Log;
import selena.common.util.Strings;

/**
 * An adapter that wraps {@code Method}.
 */
public final class MethodDescriptor {
    private static final Map<Class<?>,Converter<?>> sConverters = populateConverters();

    private final String mCommand;
    private final Method mMethod;
    private final Class<? extends Facade> mClass;

    public MethodDescriptor(final Class<? extends Facade> clazz, final Method method) {
        mClass = clazz;
        mMethod = method;
        final Command rpc = mMethod.getAnnotation(Command.class);
        mCommand = rpc.ns() + ":" + mMethod.getName();
    }

    @Override
    public String toString() {
        return mCommand + " (" + mMethod.getDeclaringClass().getCanonicalName() + "." + mMethod.getName() + ")";
    }

    /** Collects all methods with {@code RPC} annotation from given class. */
    public static Collection<MethodDescriptor> collectFrom(final Class<? extends Facade> clazz)
    {
        final List<MethodDescriptor> descriptors = new ArrayList<MethodDescriptor>();
        for (final Method method : clazz.getMethods()) {
            if (method.isAnnotationPresent(Command.class)) {
                descriptors.add(new MethodDescriptor(clazz, method));
            }
        }
        return descriptors;
    }

    /**
     * Invokes the call that belongs to this object with the given parameters. Wraps the response
     * (possibly an exception) in a JSONObject.
     * 
     * @param parameters
     *          {@code JSONArray} containing the parameters
     * @return result
     * @throws Throwable
     */
    public Object invoke(final FacadeManager manager, final JSONArray parameters)
            throws Throwable
    {
        final Type[] parameterTypes = getGenericParameterTypes();
        final Object[] args = new Object[parameterTypes.length];
        final Annotation annotations[][] = getParameterAnnotations();

        if (parameters.length() > args.length) {
            throw new CommandException("Too many parameters specified.");
        }

        for (int i = 0; i < args.length; i++) {
            final Type parameterType = parameterTypes[i];
            if (i < parameters.length()) {
                args[i] = convertParameter(parameters, i, parameterType);
            }
            else if (MethodDescriptor.hasDefaultValue(annotations[i])) {
                args[i] = MethodDescriptor.getDefaultValue(parameterType, annotations[i]);
            }
            else {
                throw new CommandException("Argument " + (i + 1) + " is not present");
            }
        }

        Object result = null;
        try {
            result = manager.doInvoke(mClass, mMethod, args);
        }
        catch (final InvocationTargetException e) {
            if (e.getCause() instanceof SecurityException) {
                Log.d("RPC invoke failed: " + e.getCause().getMessage());
            }
            throw e;
        }
        catch (final Throwable t) {
            throw t.getCause();
        }
        return result;
    }

    /**
     * Converts a parameter from JSON into a Java Object.
     */
    static Object convertParameter(final JSONArray parameters, final int index, final Type type)
            throws JSONException, CommandException
    {
        try {
            // We must handle null and numbers explicitly because we cannot magically cast them. We
            // also need to convert implicitly from numbers to bools.
            if (parameters.isNull(index)) {
                return null;
            }
            else if ((type == Boolean.class) || (type == boolean.class)) {
                return new Boolean(
                        Strings.isTrue(parameters.get(index).toString()));
            }
            else if ((type == Long.class) || (type == long.class)) {
                return parameters.getLong(index);
            }
            else if ((type == Double.class) || (type == double.class)) {
                return parameters.getDouble(index);
            }
            else if ((type == Integer.class) || (type == int.class)) {
                return parameters.getInt(index);
            }
            else {
                // Magically cast the parameter to the right Java type.
                return ((Class<?>)type).cast(parameters.get(index));
            }
        }
        catch (final ClassCastException e) {
            throw new CommandException("Argument " + (index + 1) + " should be of type "
                               + ((Class<?>)type).getSimpleName() + ".");
        }
    }

    public Method getMethod() {
        return mMethod;
    }

    public Class<? extends Facade> getDeclaringClass() {
        return mClass;
    }

    public String getMethodName() {
        return mMethod.getName();
    }

    public String getCallName() {
        return mCommand;
    }

    public Type[] getGenericParameterTypes() {
        return mMethod.getGenericParameterTypes();
    }

    public Annotation[][] getParameterAnnotations() {
        return mMethod.getParameterAnnotations();
    }

    /**
     * Returns a human-readable help text for this RPC, based on annotations in the source code.
     * 
     * @return derived help string
     */
    public String getHelp() {
        final StringBuilder helpBuilder = new StringBuilder();
        final Command rpcAnnotation = mMethod.getAnnotation(Command.class);

        helpBuilder.append(mMethod.getName());
        helpBuilder.append("(");
        final Class<?>[] parameterTypes = mMethod.getParameterTypes();
        final Type[] genericParameterTypes = mMethod.getGenericParameterTypes();
        final Annotation[][] annotations = mMethod.getParameterAnnotations();
        for (int i = 0; i < parameterTypes.length; i++) {
            if (i == 0) {
                helpBuilder.append("\n  ");
            }
            else {
                helpBuilder.append(",\n  ");
            }

            helpBuilder.append(getHelpForParameter(genericParameterTypes[i], annotations[i]));
        }
        helpBuilder.append(")\n\n");
        helpBuilder.append(rpcAnnotation.description());
        if (!rpcAnnotation.returns().equals("")) {
            helpBuilder.append("\n");
            helpBuilder.append("\nReturns:\n  ");
            helpBuilder.append(rpcAnnotation.returns());
        }

        return helpBuilder.toString();
    }

    /**
     * Returns the help string for one particular parameter. This respects optional parameters.
     * 
     * @param parameterType
     *          (generic) type of the parameter
     * @param annotations
     *          annotations of the parameter, may be null
     * @return string describing the parameter based on source code annotations
     */
    private static String getHelpForParameter(final Type parameterType, final Annotation[] annotations)
    {
        final StringBuilder result = new StringBuilder();

        appendTypeName(result, parameterType);
        result.append(" ");
        result.append(getName(annotations));
        if (hasDefaultValue(annotations)) {
            result.append("[optional");
            if (hasExplicitDefaultValue(annotations)) {
                result.append(", default " + getDefaultValue(parameterType, annotations));
            }
            result.append("]");
        }

        final String description = getDescription(annotations);
        if (description.length() > 0) {
            result.append(": ");
            result.append(description);
        }

        return result.toString();
    }

    /**
     * Appends the name of the given type to the {@link StringBuilder}.
     * 
     * @param builder
     *          string builder to append to
     * @param type
     *          type whose name to append
     */
    private static void appendTypeName(final StringBuilder builder, final Type type) {
        if (type instanceof Class<?>) {
            builder.append(((Class<?>)type).getSimpleName());
        }
        else {
            final ParameterizedType parametrizedType = (ParameterizedType)type;
            builder.append(((Class<?>)parametrizedType.getRawType()).getSimpleName());
            builder.append("<");

            final Type[] arguments = parametrizedType.getActualTypeArguments();
            for (int i = 0; i < arguments.length; i++) {
                if (i > 0) {
                    builder.append(", ");
                }
                appendTypeName(builder, arguments[i]);
            }
            builder.append(">");
        }
    }

    /**
     * Returns parameter descriptors suitable for the RPC call text representation.
     * 
     * <p>
     * Uses parameter value, default value or name, whatever is available first.
     * 
     * @return an array of parameter descriptors
     */
    public ParameterDescriptor[] getParameterValues(final String[] values) {
        final Type[] parameterTypes = mMethod.getGenericParameterTypes();
        final Annotation[][] parametersAnnotations = mMethod.getParameterAnnotations();
        final ParameterDescriptor[] parameters = new ParameterDescriptor[parametersAnnotations.length];
        for (int index = 0; index < parameters.length; index++) {
            String value;
            if (index < values.length) {
                value = values[index];
            }
            else if (hasDefaultValue(parametersAnnotations[index])) {
                final Object defaultValue = getDefaultValue(parameterTypes[index], parametersAnnotations[index]);
                if (defaultValue == null) {
                    value = null;
                }
                else {
                    value = String.valueOf(defaultValue);
                }
            }
            else {
                value = getName(parametersAnnotations[index]);
            }
            parameters[index] = new ParameterDescriptor(value, parameterTypes[index]);
        }
        return parameters;
    }

    /**
     * Returns parameter hints.
     * 
     * @return an array of parameter hints
     */
    public String[] getParameterHints() {
        final Annotation[][] parametersAnnotations = mMethod.getParameterAnnotations();
        final String[] hints = new String[parametersAnnotations.length];
        for (int index = 0; index < hints.length; index++) {
            final String name = getName(parametersAnnotations[index]);
            final String description = getDescription(parametersAnnotations[index]);
            String hint = "No paramenter description.";
            if (!name.equals("") && !description.equals("")) {
                hint = name + ": " + description;
            }
            else if (!name.equals("")) {
                hint = name;
            }
            else if (!description.equals("")) {
                hint = description;
            }
            hints[index] = hint;
        }
        return hints;
    }

    /**
     * Extracts the formal parameter name from an annotation.
     * 
     * @param annotations
     *          the annotations of the parameter
     * @return the formal name of the parameter
     */
    private static String getName(final Annotation[] annotations) {
        for (final Annotation a : annotations) {
            if (a instanceof Param) {
                return ((Param)a).name();
            }
        }
        throw new IllegalStateException("No parameter name");
    }

    /**
     * Extracts the parameter description from its annotations.
     * 
     * @param annotations
     *          the annotations of the parameter
     * @return the description of the parameter
     */
    private static String getDescription(final Annotation[] annotations) {
        for (final Annotation a : annotations) {
            if (a instanceof Param) {
                return ((Param)a).description();
            }
        }
        throw new IllegalStateException("No parameter description");
    }

    /**
     * Returns the default value for a specific parameter.
     * 
     * @param parameterType
     *          parameterType
     * @param annotations
     *          annotations of the parameter
     */
    public static Object getDefaultValue(final Type parameterType, final Annotation[] annotations)
    {
        for (final Annotation a : annotations) {
            if (a instanceof Default) {
                final Default defaultAnnotation = (Default)a;
                final Converter<?> converter = converterFor(parameterType, defaultAnnotation.converter());
                return converter.convert(defaultAnnotation.value());
            }
            else if (a instanceof Optional) {
                return null;
            }
        }
        throw new IllegalStateException("No default value for " + parameterType);
    }

    @SuppressWarnings("rawtypes")
    private static Converter<?> converterFor(final Type parameterType,
                                             final Class<? extends Converter> converterClass)
    {
        if (converterClass == Converter.class) {
            final Converter<?> converter = sConverters.get(parameterType);
            if (converter == null) {
                throw new IllegalArgumentException("No predefined converter found for " + parameterType);
            }
            return converter;
        }
        try {
            final Constructor<?> constructor = converterClass.getConstructor(new Class<?>[0]);
            return (Converter<?>)constructor.newInstance(new Object[0]);
        }
        catch (final Exception e) {
            throw new IllegalArgumentException("Cannot create converter from "
                                               + converterClass.getCanonicalName());
        }
    }

    /**
     * Determines whether or not this parameter has default value.
     * 
     * @param annotations
     *          annotations of the parameter
     */
    public static boolean hasDefaultValue(final Annotation[] annotations) {
        for (final Annotation a : annotations) {
            if ((a instanceof Default) || (a instanceof Optional)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns whether the default value is specified for a specific parameter.
     * 
     * @param annotations
     *          annotations of the parameter
     */
    static boolean hasExplicitDefaultValue(final Annotation[] annotations) {
        for (final Annotation a : annotations) {
            if (a instanceof Default) {
                return true;
            }
        }
        return false;
    }

    /** Returns the converters for {@code String}, {@code Integer} and {@code Boolean}. */
    private static Map<Class<?>,Converter<?>> populateConverters() {
        final Map<Class<?>,Converter<?>> converters = new HashMap<Class<?>,Converter<?>>();
        converters.put(String.class, new Converter<String>() {
            @Override
            public String convert(final String value) {
                return value;
            }
        });
        converters.put(Integer.class, new Converter<Integer>() {
            @Override
            public Integer convert(final String input) {
                try {
                    return Integer.decode(input);
                }
                catch (final NumberFormatException e) {
                    throw new IllegalArgumentException("'" + input + "' is not an integer");
                }
            }
        });
        converters.put(Boolean.class, new Converter<Boolean>() {
            @Override
            public Boolean convert(String input) {
                if (input == null) {
                    return null;
                }
                input = input.toLowerCase();
                if (input.equals("true")) {
                    return Boolean.TRUE;
                }
                if (input.equals("false")) {
                    return Boolean.FALSE;
                }
                throw new IllegalArgumentException("'" + input + "' is not a boolean");
            }
        });
        return converters;
    }
}
