package selena.app;

import ni.aglSystem.ISystem;
import ni.aglSystem.Lib;
import selena.common.util.Log;
import selena.util.Strings;

public final class App {
    private App() {
    }

    static public void run(final AppEventHandler eventHandler) {
        try {
            final NativeAppWindow app = new NativeAppWindow(eventHandler);
            while (app.update()) {
                app.draw();
            }
            app.dispose();
        }
        catch (final Exception e) {
            final ISystem sys = Lib.getSystem();
            if (sys != null) {
                Log.e("Fatal Error", e);
                sys.fatalError(Strings.getStackTrace(e));
            }
        }
    }
}
