package selena.app;

import java.io.IOException;

import ni.aglSystem.ECreateInstanceFlags;
import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.IOSWindow;
import ni.types.Vector4l;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import selena.SelenaException;

final class LWJGLWindowFixed extends LWJGLBaseWindow {
    public LWJGLWindowFixed(final AppEventHandler eventHandler)
            throws SelenaException, IOException {
        super(eventHandler);
    }

    @Override
    protected void dispose() {
        super.dispose();
        Display.destroy();
        java.lang.System.exit(0);
    }

    @Override
    protected void create(final Vector4l r) throws LWJGLException,
            SelenaException
    {
        // Create the generic window object
        _wnd = IOSWindow.query(sys.createInstance("aglSystem.OSWindowGeneric",
                null, null, ECreateInstanceFlags.Default, 0, 0, 0));
        if (_wnd == null)
            throw new SelenaException("Can't create the application's window !");
        _wndMT = _wnd.getMessageTargets();
        _wnd.setRect(r);

        Display.setTitle(_config.windowTitle);
        Display.setLocation(-1, -1);
        Display.setDisplayMode(new DisplayMode(r.getWidth(), r.getHeight()));
    }

    @Override
    protected void updateWindow() {
        // Close event...
        if (Display.isCloseRequested()) {
            postEventMessage(EOSWindowMessage.Close);
        }
    }
}
