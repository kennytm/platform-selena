package selena.common.facade.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation to document a command parameter.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
@Documented
public @interface Param {
    /**
     * The name of the formal parameter. This should be in agreement with the java code.
     */
    public String name();

    /**
     * Description of the RPC. This should be a short descriptive statement without a full stop, such
     * as 'disables the WiFi mode'.
     */
    public String description() default "";
}
