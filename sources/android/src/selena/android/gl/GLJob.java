package selena.android.gl;

import javax.microedition.khronos.opengles.GL10;

public interface GLJob {
    public void Run(GL10 gl);
};
