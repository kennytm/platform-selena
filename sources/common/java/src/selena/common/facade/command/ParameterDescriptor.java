package selena.common.facade.command;

import java.lang.reflect.Type;

/**
 * Command parameter description.
 */
public final class ParameterDescriptor {
    private final String value;
    private final Type type;

    public ParameterDescriptor(final String value, final Type type) {
        this.value = value;
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public Type getType() {
        return type;
    }
}
