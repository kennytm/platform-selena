package selena.msg;

import ni.aglSystem.IFile;
import ni.aglSystem.IMessage;
import ni.aglSystem.Lib;
import ni.types.MessageID;

import selena.common.util.Log;
import selena.util.ReflectSinks;
import junit.framework.TestCase;

public class MsgTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    MessageTarget foo;
    MessageTarget bar;

    private static final int SEND_COUNT = 100000;

    public static final int MSG_Ping = MessageID.buildLocal('p');
    public static final int MSG_PingInFile = MessageID.buildLocal('f');

    public class Foo extends MessageTarget {
        public Foo() {
            super(MsgTest.class);
        }

        public void onPing() {
            bar.send(MSG_Ping);
        }
    }

    public class PingDirect extends MessageTarget {
        public int pingCount = 0;

        public PingDirect() {
            super(MsgTest.class);
        }

        @Override
        public boolean onMessageSink(final IMessage apMsg) {
            if (apMsg.getID() == MSG_Ping) {
                ++pingCount;
            }
            else if (apMsg.getID() == MSG_PingInFile) {
                ++pingCount;
                final IFile pingFile = IFile.query(apMsg.getA());
                pingFile.writeLE32(pingCount);
            }
            return false;
        }
    }

    public class PingReflect extends MessageTarget {
        public int pingCount = 0;

        public PingReflect() {
            super(MsgTest.class);
        }

        public void onPing() {
            //Log.i("Bar::onPing");
            ++pingCount;
        }

        public void onPingInFile(final IFile pingFile) {
            ++pingCount;
            pingFile.writeLE32(pingCount);
        }
    }

    public void testMessageTarget() {
        Log.i("USE_SET_ACCESSIBLE: " + ReflectSinks.USE_SET_ACCESSIBLE);
        if (ReflectSinks.USE_SET_ACCESSIBLE) {
            foo = new MessageTarget(MsgTest.class) {
                @SuppressWarnings("unused")
                public void onPing() {
                    bar.send(MSG_Ping);
                }
            };
        }
        else {
            foo = new Foo();
        }
        bar = new PingReflect();
        foo.send(MSG_Ping);
        assertEquals(1, ((PingReflect)bar).pingCount);
    }

    public void testPingDirectSendMessage() {
        final MessageTarget ping = new PingDirect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.send(MSG_Ping);
        }
        assertEquals(SEND_COUNT, ((PingDirect)ping).pingCount);
    }

    public void testPingInFileDirectSendMessage() {
        final IFile pingFile = Lib.getSystem().createFileDynamicMemory(1000, "");
        final MessageTarget ping = new PingDirect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.send(MSG_PingInFile, pingFile);
        }
        assertEquals(SEND_COUNT, ((PingDirect)ping).pingCount);
        assertEquals(SEND_COUNT, pingFile.getSize() / 4);
    }

    public void testPingReflectSendMessage() {
        final MessageTarget ping = new PingReflect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.send(MSG_Ping);
        }
        assertEquals(SEND_COUNT, ((PingReflect)ping).pingCount);
    }

    public void testPingInFileReflectSendMessage() {
        final IFile pingFile = Lib.getSystem().createFileDynamicMemory(1000, "");
        final MessageTarget ping = new PingReflect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.send(MSG_PingInFile, pingFile);
        }
        assertEquals(SEND_COUNT, ((PingReflect)ping).pingCount);
        assertEquals(SEND_COUNT, pingFile.getSize() / 4);
    }

    public void testPingDirectPostMessage_OneDispatch() {
        final MessageTarget ping = new PingDirect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.post(MSG_Ping);
        }
        Msg.getAndDispatchAllCurrentThreadMessages();
        assertEquals(SEND_COUNT, ((PingDirect)ping).pingCount);
    }

    public void testPingReflectPostMessage_OneDispatch() {
        final MessageTarget ping = new PingReflect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.post(MSG_Ping);
        }
        Msg.getAndDispatchAllCurrentThreadMessages();
        assertEquals(SEND_COUNT, ((PingReflect)ping).pingCount);
    }

    public void testPingDirectPostMessage_DirectDispatch() {
        final MessageTarget ping = new PingDirect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.post(MSG_Ping);
            Msg.getAndDispatchAllCurrentThreadMessages();
        }
        assertEquals(SEND_COUNT, ((PingDirect)ping).pingCount);
    }

    public void testPingReflectPostMessage_DirectDispatch() {
        final MessageTarget ping = new PingReflect();
        for (int i = 0; i < SEND_COUNT; ++i) {
            ping.post(MSG_Ping);
            Msg.getAndDispatchAllCurrentThreadMessages();
        }
        assertEquals(SEND_COUNT, ((PingReflect)ping).pingCount);
    }
}
