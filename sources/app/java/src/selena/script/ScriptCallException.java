package selena.script;

public class ScriptCallException extends ScriptException {
    private static final long serialVersionUID = 1L;

    public ScriptCallException() {
        super();
    }

    public ScriptCallException(final Throwable t) {
        super(t);
    }

    public ScriptCallException(final String message) {
        super(message);
    }

    public ScriptCallException(final String message, final Throwable t) {
        super(message, t);
    }
}