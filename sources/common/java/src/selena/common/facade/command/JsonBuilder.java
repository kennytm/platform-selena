package selena.common.facade.command;

import java.util.List;
import java.util.Map;
import java.util.Set;

import selena.common.json.JSONArray;
import selena.common.json.JSONException;
import selena.common.json.JSONObject;

public class JsonBuilder {

    private JsonBuilder() {
        // This is a utility class.
    }

    @SuppressWarnings("unchecked")
    public static Object build(final Object data) throws JSONException {
        if (data == null) {
            return JSONObject.NULL;
        }
        if (data instanceof Integer) {
            return data;
        }
        if (data instanceof Float) {
            return data;
        }
        if (data instanceof Double) {
            return data;
        }
        if (data instanceof Long) {
            return data;
        }
        if (data instanceof String) {
            return data;
        }
        if (data instanceof Boolean) {
            return data;
        }
        if (data instanceof JSONObject) {
            return data;
        }
        if (data instanceof JSONArray) {
            return data;
        }
        if (data instanceof Set<?>) {
            return new JSONArray((Set<?>)data);
        }
        if (data instanceof List<?>) {
            return new JSONArray((List<?>)data);
        }
        if (data instanceof Map<?,?>) {
            return new JSONObject((Map<String,?>)data);
        }
        throw new JSONException("Failed to build JSON result.");
    }
}
