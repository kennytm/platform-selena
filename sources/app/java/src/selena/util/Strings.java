package selena.util;

import ni.aglSystem.IMessage;

public class Strings extends selena.common.util.Strings {
    protected Strings() {
    }

    static public String fromMessage(final Object receiver, final IMessage m) {
        final Object sender = m.getSender();
        if ((sender != null) || (receiver != null)) {
            return fromMessage(sender, receiver, m.getID(), m.getA(), m.getB());
        }
        else {
            return fromMessage(m.getID(), m.getA(), m.getB());
        }
    }

    static public String fromMessage(final IMessage m) {
        return fromMessage(null, m);
    }
}
