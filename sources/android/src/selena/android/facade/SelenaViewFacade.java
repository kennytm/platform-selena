package selena.android.facade;

import selena.android.gl.GLDefaultOptions;
import selena.android.util.Dialogs;
import selena.android.util.Views;
import selena.android.view.SelenaView;
import selena.common.facade.Facade;
import selena.common.facade.FacadeManager;
import selena.common.facade.annotations.Command;
import selena.common.facade.annotations.Default;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

//================================================================
//
// Facade
//
//================================================================
public class SelenaViewFacade extends Facade {
    public SelenaView mView = null;

    public SelenaViewFacade(final FacadeManager manager) {
        super(manager);
    }

    @Override
    public void shutdown() {
        mView = null;
    }

    @Command(ns = "android")
    public void setAutoPauseRendering(final boolean onOff) {
        if (onOff) {
            mView.mGLView.setFrameToRender(0);
        }
        else {
            mView.mGLView.setFrameToRender(-1);
        }
        mView.mGLView.render();
    }

    @Command(ns = "android")
    public void setFrameToRender(final int frameToRender) {
        mView.mGLView.setFrameToRender(frameToRender);
    }

    @Command(ns = "android")
    public void setMaxFrameToRender(final int maxFrameToRender) {
        mView.mGLView.setMaxFrameToRender(maxFrameToRender);
    }

    @Command(ns = "android")
    public void setNumFrameToAddOnRender(final int numFrameToAddOnRender) {
        mView.mGLView.setNumFrameToAddOnRender(numFrameToAddOnRender);
    }

    @Command(ns = "android")
    public void setMinFrameTime(final int minFrameTime) {
        mView.mGLView.setMinFrameTime(minFrameTime);
    }

    @Command(ns = "mobile")
    public int windowWidth() {
        return Views.getHolderWidth(mView);
    }

    @Command(ns = "mobile")
    public int windowHeight() {
        return Views.getHolderHeight(mView);
    }

    @Command(ns = "mobile")
    public String GL_COLOR_BUFFER_FORMAT() {
        return GLDefaultOptions.TRANSLUCENT_WINDOW ? "R8G8B8A8" : "R5G6B5";
    }

    @Command(ns = "mobile")
    public String GL_DEPTH_BUFFER_FORMAT() {
        return GLDefaultOptions.USE_DEPTH_BUFFER_24 ? "D24S8" : "D16";
    }

    @Command(ns = "mobile")
    public boolean GL_POS() {
        return GLDefaultOptions.USE_GL_POS;
    }

    @Command(ns = "mobile")
    public boolean GL_LOWMEM() {
        return GLDefaultOptions.USE_GL_LOWMEM;
    }

    @Command(ns = "mobile")
    public boolean GL_ES2() {
        return !GLDefaultOptions.USE_GL_POS && GLDefaultOptions.USE_GL_ES2;
    }

    @Command(ns = "mobile", description = "Show or hide the device's soft keyboard")
    public void keyboard(final boolean abVisible)
    {
        final InputMethodManager imm = (InputMethodManager)mView.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (abVisible)
            imm.showSoftInput(mView, InputMethodManager.SHOW_FORCED);
        else
            imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
    }

    /**
     * Shows a message with an OK callback.
     */
    @Command(ns = "mobile")
    public void alertOk(final String message, @Default(value = "") final String onOK)
    {
        Dialogs.alertOk(mView.getContext(), message,
                new Runnable() {
                    @Override
                    public void run() {
                        if (onOK.length() > 0)
                            mView.pushConsoleCommand(onOK);
                    }
                });
    }

    /**
     * Shows a message with an OK and Cancel callback.
     */
    @Command(ns = "mobile")
    public void alertOkCancel(final String message,
            @Default(value = "") final String onOK,
            @Default(value = "") final String onCancel)
    {
        Dialogs.alertOkCancel(mView.getContext(), message,
                new Runnable() {
                    @Override
                    public void run() {
                        if (onOK.length() > 0)
                            mView.pushConsoleCommand(onOK);
                    }
                },
                new Runnable() {
                    @Override
                    public void run() {
                        if (onCancel.length() > 0)
                            mView.pushConsoleCommand(onCancel);
                    }
                });
    }
}
