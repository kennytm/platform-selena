package selena.msg;

import ni.aglSystem.IMessageSystem;
import ni.aglSystem.Lib;

public class Msg {
    private static IMessageSystem msgSys = Lib.getSystem().getMessageSystem();

    public static void sendMessage(final int tid, final int msg) {
        msgSys.sendMessage(tid, msg, null, null);
    }

    public static void sendMessage(final int tid, final int msg, final Object a) {
        msgSys.sendMessage(tid, msg, a, null);
    }

    public static void sendMessage(final int tid, final int msg, final Object a, final Object b)
    {
        msgSys.sendMessage(tid, msg, a, b);
    }

    public static void postMessage(final int tid, final int msg) {
        msgSys.postMessage(tid, msg, null, null);
    }

    public static void postMessage(final int tid, final int msg, final Object a) {
        msgSys.postMessage(tid, msg, a, null);
    }

    public static void postMessage(final int tid, final int msg, final Object a, final Object b)
    {
        msgSys.postMessage(tid, msg, a, b);
    }

    public static void getAndDispatchAllCurrentThreadMessages() {
        msgSys.getAndDispatchAllCurrentThreadMessages();
    }
}
