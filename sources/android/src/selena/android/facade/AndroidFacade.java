package selena.android.facade;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import selena.android.util.Dialogs;
import selena.android.util.FileUtils;
import selena.android.util.MainThread;
import selena.common.facade.Facade;
import selena.common.facade.FacadeManager;
import selena.common.facade.annotations.Command;
import selena.common.facade.annotations.Default;
import selena.common.facade.annotations.Optional;
import selena.common.facade.annotations.Param;
import selena.common.util.Log;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.telephony.TelephonyManager;
import android.text.ClipboardManager;

public class AndroidFacade extends Facade {
    private final Context mContext;
    private final Vibrator mVibrator;

    static public int RESID_Logo48 = 0;

    @Override
    public void shutdown() {
    }

    public AndroidFacade(final FacadeManager manager) {
        super(manager);
        mContext = (Context)manager.getContext();
        mVibrator = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);
    }

    // TODO(damonkohler): Pull this out into proper argument deserialization and support
    // complex/nested types being passed in.
    private static void putExtrasFromJsonObject(final JSONObject extras, final Intent intent) throws JSONException
    {
        final JSONArray names = extras.names();
        for (int i = 0; i < names.length(); i++) {
            final String name = names.getString(i);
            final Object data = extras.get(name);
            if (data == null) {
                continue;
            }
            if (data instanceof Integer) {
                intent.putExtra(name, (Integer)data);
            }
            if (data instanceof Float) {
                intent.putExtra(name, (Float)data);
            }
            if (data instanceof Double) {
                intent.putExtra(name, (Double)data);
            }
            if (data instanceof Long) {
                intent.putExtra(name, (Long)data);
            }
            if (data instanceof String) {
                intent.putExtra(name, (String)data);
            }
            if (data instanceof Boolean) {
                intent.putExtra(name, (Boolean)data);
            }
        }
    }

    private Intent buildIntent(final String action, final String uri, final String type, final JSONObject extras,
            final String packagename, final String classname, final JSONArray categories) throws JSONException
    {
        final Intent intent = new Intent(action);
        intent.setDataAndType(uri != null ? Uri.parse(uri) : null, type);
        if (packagename != null && classname != null) {
            intent.setComponent(new ComponentName(packagename, classname));
        }
        if (extras != null) {
            putExtrasFromJsonObject(extras, intent);
        }
        if (categories != null) {
            for (int i = 0; i < categories.length(); i++) {
                intent.addCategory(categories.getString(i));
            }
        }
        return intent;
    }

    private void startActivity(final Intent intent) {
        try {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }
        catch (final Exception e) {
            Log.e("Failed to launch intent.", e);
        }
    }

    @Command(ns = "generic", description = "Put text in the clipboard.")
    public void setClipboard(@Param(name = "text") final String text) {
        final ClipboardManager clipboard =
                (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setText(text);
    }

    @Command(ns = "generic", description = "Read text from the clipboard.", returns = "The text in the clipboard.")
    public String getClipboard() {
        final ClipboardManager clipboard =
                (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        return clipboard.getText().toString();
    }

    @Command(ns = "generic", description = "Vibrates the phone or a specified duration in milliseconds.")
    public void vibrate(
            @Param(name = "duration", description = "duration in milliseconds") @Default("200") final Integer duration)
    {
        mVibrator.vibrate(duration);
    }

    @Command(ns = "android", description = "Starts an activity.")
    public void startActivity(
            @Param(name = "action") final String action,
            @Param(name = "uri") @Optional final String uri,
            @Param(name = "type", description = "MIME type/subtype of the URI") @Optional final String type,
            @Param(name = "extras", description = "a Map of extras to add to the Intent") @Optional final JSONObject extras,
            @Param(name = "packagename", description = "name of package. If used, requires classname to be useful") @Optional final String packagename,
            @Param(name = "classname", description = "name of class. If used, requires packagename to be useful") @Optional final String classname)
            throws Exception
    {
        final Intent intent = buildIntent(action, uri, type, extras, packagename, classname, null);
        startActivity(intent);
    }

    @Command(ns = "android", description = "Send a broadcast.")
    public void sendBroadcast(
            @Param(name = "action") final String action,
            @Param(name = "uri") @Optional final String uri,
            @Param(name = "type", description = "MIME type/subtype of the URI") @Optional final String type,
            @Param(name = "extras", description = "a Map of extras to add to the Intent") @Optional final JSONObject extras,
            @Param(name = "packagename", description = "name of package. If used, requires classname to be useful") @Optional final String packagename,
            @Param(name = "classname", description = "name of class. If used, requires packagename to be useful") @Optional final String classname)
            throws JSONException
    {
        final Intent intent = buildIntent(action, uri, type, extras, packagename, classname, null);
        try {
            mContext.sendBroadcast(intent);
        }
        catch (final Exception e) {
            Log.e("Failed to broadcast intent.", e);
        }
    }

    @Command(ns = "android", description = "Create an Intent.", returns = "An object representing an Intent")
    public Intent makeIntent(
            @Param(name = "action") final String action,
            @Param(name = "uri") @Optional final String uri,
            @Param(name = "type", description = "MIME type/subtype of the URI") @Optional final String type,
            @Param(name = "extras", description = "a Map of extras to add to the Intent") @Optional final JSONObject extras,
            @Param(name = "categories", description = "a List of categories to add to the Intent") @Optional final JSONArray categories,
            @Param(name = "packagename", description = "name of package. If used, requires classname to be useful") @Optional final String packagename,
            @Param(name = "classname", description = "name of class. If used, requires packagename to be useful") @Optional final String classname,
            @Param(name = "flags", description = "Intent flags") @Optional final Integer flags)
            throws JSONException
    {
        final Intent intent = buildIntent(action, uri, type, extras, packagename, classname, categories);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (flags != null) {
            intent.setFlags(flags);
        }
        return intent;
    }

    @Command(ns = "android", description = "Start Activity using Intent")
    public void startActivityIntent(
            @Param(name = "intent", description = "Intent in the format as returned from makeIntent") final Intent intent)
            throws Exception
    {
        startActivity(intent);
    }

    @Command(ns = "android", description = "Send Broadcast Intent")
    public void sendBroadcastIntent(
            @Param(name = "intent", description = "Intent in the format as returned from makeIntent") final Intent intent)
            throws Exception
    {
        mContext.sendBroadcast(intent);
    }

    @Command(ns = "android", description = "Displays a short-duration Toast notification.")
    public void makeToast(@Param(name = "message") final String message) {
        Dialogs.makeToast(mContext, message);
    }

    @Command(ns = "android", description = "Displays a longer-duration Toast notification.")
    public void makeLongToast(@Param(name = "message") final String message) {
        Dialogs.makeLongToast(mContext, message);
    }

    @Command(ns = "android", description = "Displays a notification that will be canceled when the user clicks on it.")
    public void notify(@Param(name = "title", description = "title") final String title,
            @Param(name = "message") final String message)
    {
        Dialogs.notify(mContext, title, message);
    }

    @Command(ns = "android", description = "Launches an activity that sends an e-mail message to a given recipient.")
    public void sendEmail(
            @Param(name = "to", description = "A comma separated list of recipients.") final String to,
            @Param(name = "subject") final String subject,
            @Param(name = "body") final String body,
            @Param(name = "attachmentUri") @Optional final String attachmentUri)
    {
        final Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(android.content.Intent.EXTRA_EMAIL, to.split(","));
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        if (attachmentUri != null) {
            intent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.parse(attachmentUri));
        }
        startActivity(intent);
    }

    @Command(ns = "android", description = "Returns package version code.")
    public int getPackageVersionCode(@Param(name = "packageName") final String packageName)
    {
        int result = -1;
        PackageInfo pInfo = null;
        try {
            pInfo =
                    mContext.getPackageManager().getPackageInfo(packageName, PackageManager.GET_META_DATA);
        }
        catch (final NameNotFoundException e) {
            pInfo = null;
        }
        if (pInfo != null) {
            result = pInfo.versionCode;
        }
        return result;
    }

    @Command(ns = "android", description = "A map of various useful environment details")
    public Map<String,Object> environment() {
        final Map<String,Object> result = new HashMap<String,Object>();
        final Map<String,Object> zone = new HashMap<String,Object>();
        final TimeZone tz = TimeZone.getDefault();
        zone.put("id", tz.getID());
        zone.put("display", tz.getDisplayName());
        zone.put("offset", tz.getOffset((new Date()).getTime()));
        result.put("TZ", zone);
        result.put("download", FileUtils.getExternalDownload().getAbsolutePath());
        result.put("appcache", mContext.getCacheDir().getAbsolutePath());
        result.put("SDK", android.os.Build.VERSION.SDK);
        return result;
    }

    @Command(ns = "android", description = "Returns package version name.")
    public String getPackageVersion(@Param(name = "packageName") final String packageName)
    {
        PackageInfo packageInfo = null;
        try {
            packageInfo =
                    mContext.getPackageManager().getPackageInfo(packageName, PackageManager.GET_META_DATA);
        }
        catch (final NameNotFoundException e) {
            return null;
        }
        if (packageInfo != null) {
            return packageInfo.versionName;
        }
        return null;
    }

    @Command(ns = "android", description = "Get list of constants (static final fields) for a class")
    public Bundle getConstants(
            @Param(name = "classname", description = "Class to get constants from") final String classname)
            throws Exception
    {
        final Bundle result = new Bundle();
        final int flags = Modifier.FINAL | Modifier.PUBLIC | Modifier.STATIC;
        final Class<?> clazz = Class.forName(classname);
        for (final Field field : clazz.getFields()) {
            if ((field.getModifiers() & flags) == flags) {
                final Class<?> type = field.getType();
                final String name = field.getName();
                if (type == int.class) {
                    result.putInt(name, field.getInt(null));
                }
                else if (type == long.class) {
                    result.putLong(name, field.getLong(null));
                }
                else if (type == double.class) {
                    result.putDouble(name, field.getDouble(null));
                }
                else if (type == char.class) {
                    result.putChar(name, field.getChar(null));
                }
                else if (type instanceof Object) {
                    result.putString(name, field.get(null).toString());
                }
            }
        }
        return result;
    }

    @Command(ns = "android", description = "Get the path to the specified system directory (Home|AppData|Temp|Cache)")
    public String getSystemDir(final String aDirName) {
        final String dirName = aDirName.toLowerCase();
        if ("home".equals(dirName)) {
            return FileUtils.getHomeDir(mContext).getAbsolutePath();
        }
        else if ("appdata".equals(dirName)) {
            return FileUtils.getAppDataDir(mContext).getAbsolutePath();
        }
        else if ("temp".equals(dirName)) {
            return FileUtils.getTempDir(mContext).getAbsolutePath();
        }
        else if ("cache".equals(dirName)) {
            return FileUtils.getCacheDir(mContext).getAbsolutePath();
        }
        return "";
    }

    @Command(ns = "android", description = "Set the context's window title")
    public void setTitle(final String aTitle)
    {
        MainThread.run(mContext, new Runnable() {
            @Override
            public void run() {
                ((Activity)mContext).setTitle(aTitle);
            }
        });
    }

    @Command(ns = "mobile", description = "Get unique id representing the device")
    public String getDeviceId() {
        String ret = "";
        try {
            ret = getDeviceIdTelephony();
            ret += "-" + getDeviceIdWifi();
        }
        catch (final Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    String getDeviceIdTelephony() {
        final TelephonyManager tm = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getDeviceId();
    }

    String getDeviceIdWifi() {
        final WifiManager wm = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        return wm.getConnectionInfo().getMacAddress();
    }

    @Command(ns = "android")
    public void startMainIntent(final String intentName)
    {
        MainThread.run(mContext, new Runnable() {

            @Override
            public void run() {
                final String cname[] = intentName.split("/");
                if (cname == null || cname.length != 2) {
                    Log.d("'startMainIntent ComponentName': Invalid component name, should be something like 'com.ts.gvm/.GvmLauncher'");
                    return;
                }
                if (cname[1].startsWith(".")) {
                    cname[1] = cname[0] + cname[1];
                }
                Log.d("Starting: " + cname[0] + ", " + cname[1]);
                final Intent intent = new Intent();
                intent.setComponent(new ComponentName(cname[0], cname[1]));
                mContext.startActivity(intent);
                Log.d("Started: " + intentName);
            }
        });
    }

    @Command(ns = "system", description = "A map of the board BUILD infos")
    static public Map<String,Object> build() {
        final Map<String,Object> r = new HashMap<String,Object>();
        r.put("BOARD", android.os.Build.BOARD);
        // r.put("BOOTLOADER",android.os.Build.BOOTLOADER);
        r.put("BRAND", android.os.Build.BRAND);
        r.put("CPU_ABI", android.os.Build.CPU_ABI);
        // r.put("CPU_ABI2",android.os.Build.CPU_ABI2);
        r.put("DEVICE", android.os.Build.DEVICE);
        r.put("DISPLAY", android.os.Build.DISPLAY);
        r.put("FINGERPRINT", android.os.Build.FINGERPRINT);
        // r.put("HARDWARE",android.os.Build.HARDWARE);
        r.put("HOST", android.os.Build.HOST);
        r.put("ID", android.os.Build.ID);
        r.put("MANUFACTURER", android.os.Build.MANUFACTURER);
        r.put("MODEL", android.os.Build.MODEL);
        r.put("PRODUCT", android.os.Build.PRODUCT);
        // r.put("RADIO",android.os.Build.RADIO);
        // r.put("SERIAL",android.os.Build.SERIAL);
        r.put("TAGS", android.os.Build.TAGS);
        r.put("TIME", android.os.Build.TIME);
        r.put("TYPE", android.os.Build.TYPE);
        r.put("USER", android.os.Build.USER);
        final Map<String,Object> ver = new HashMap<String,Object>();
        ver.put("CODENAME", android.os.Build.VERSION.CODENAME);
        ver.put("INCREMENTAL", android.os.Build.VERSION.INCREMENTAL);
        ver.put("RELEASE", android.os.Build.VERSION.RELEASE);
        ver.put("SDK", android.os.Build.VERSION.SDK);
        ver.put("SDK_INT", android.os.Build.VERSION.SDK_INT);
        r.put("VERSION", ver);
        return r;
    }

    @Command(ns = "system", description = "Get the CPU informations.")
    static public Map<String,String> cpuInfo() {
        final Map<String,String> r = new HashMap<String,String>();
        final String ci = FileUtils.readToString("/proc/cpuinfo");
        final String[] lines = ci.split("\n");
        for (final String l : lines) {
            final String[] vals = l.split(":");
            if (vals.length > 1) {
                r.put(vals[0].trim(), vals[1].trim());
            }
        }
        return r;
    }

    @Command(ns = "system", description = "Check the CPU architecture.")
    static public int cpuArch() {
        detectCpuInfos();
        return CPU_ARCH;
    }

    @Command(ns = "system", description = "Check the CPU features.")
    static public int cpuFeatures() {
        detectCpuInfos();
        return CPU_FEATURES;
    }

    @Command(ns = "system", description = "Check the CPU serial.")
    static public String cpuSerial() {
        detectCpuInfos();
        return CPU_SERIAL;
    }

    private static void detectCpuInfos() {
        if (CPU_ARCH >= 0 && CPU_FEATURES >= 0 && CPU_SERIAL != null)
            return;
        final Map<String,String> r = cpuInfo();
        CPU_SERIAL = r.get("Serial");
        final String arch = r.get("Processor").toLowerCase();
        CPU_ARCH = 0;
        if (arch.contains("armv7"))
            CPU_ARCH = CPU_ARCH_ARMv7;
        else if (arch.contains("armv6"))
            CPU_ARCH = CPU_ARCH_ARMv6;
        final String feats = r.get("Features").toLowerCase();
        CPU_FEATURES = 0;
        if (feats.contains("vfpv3"))
            CPU_FEATURES |= CPU_FEATURE_VFPv3;
        if (feats.contains("vfp"))
            CPU_FEATURES |= CPU_FEATURE_VFP;
        if (feats.contains("neon"))
            CPU_FEATURES |= CPU_FEATURE_NEON;
    }

    private static String CPU_SERIAL = null;

    private static int CPU_FEATURES = -1;
    public final static int CPU_FEATURE_VFP = 1 << 0;
    public final static int CPU_FEATURE_VFPv3 = 1 << 1;
    public final static int CPU_FEATURE_NEON = 1 << 2;

    private static int CPU_ARCH = -1;
    public final static int CPU_ARCH_ARMv7 = 7;
    public final static int CPU_ARCH_ARMv6 = 6;
    public final static int CPU_ARCH_ARMv5 = 5;
}
