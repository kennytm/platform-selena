package selena.common.facade.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link Command} annotation is used to annotate implementations of facade commands.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Command {
    /**
     * Returns the name of the command namespace that will be registered.
     * <pre>
     * @Command(ns="os") void setClipboard(), will register the command os:setClipboard
     * <p>
     * All command names are case insensitive
     */
    String ns();

    /**
     * Returns brief description of the function. Should be limited to one or two sentences.
     */
    String description() default "";

    /**
     * Gives a brief description of the functions return value (and the underlying data structure).
     */
    String returns() default "";
}
