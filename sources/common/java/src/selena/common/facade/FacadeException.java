package selena.common.facade;

import selena.SelenaException;

@SuppressWarnings("serial")
public class FacadeException extends SelenaException {

    public FacadeException(final Exception e) {
        super(e);
    }

    public FacadeException(final String message) {
        super(message);
    }

    public FacadeException(final String message, final Exception e) {
        super(message, e);
    }
}
