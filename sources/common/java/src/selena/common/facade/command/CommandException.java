package selena.common.facade.command;

@SuppressWarnings("serial")
public class CommandException extends Exception {

    public CommandException(final String message) {
        super(message);
    }

}
