package selena.kronos;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import ni.Kronos.EKronosEntityMessage;
import ni.Kronos.IKronosEntity;
import ni.Kronos.IKronosEntitySinkImpl;
import selena.common.util.Collections;
import selena.common.util.Enums;
import selena.common.util.Log;
import selena.common.util.Strings;
import selena.util.ReflectSinks;

/**
 * Default Entity Sink that implements {@link IKronosEntitySinkImpl} using
 * Reflection.
 * <p>
 * Message Method: [<b>void</b>|<b>boolean</b>]
 * <b>on</b>[<i>ThisClassMessageName </i>|<i>OtherClassMessageName</i>|<i>
 * {@link EKronosEntityMessage}</i>]( <br>
 * <b>param0</b>: Entity or A or B<br>
 * <b>param1</b>: A or B or Entity</li> <br>
 * <b>param2</b>: B or A or Entity</li> <br>
 * )<br>
 * If the return type is void, false is returned by the sink.
 * 
 * @author Pierre Renaux
 */
public class EntitySink implements IKronosEntitySinkImpl {
    Map<Integer,Method> jumpTable;
    Object[] invokeParams1 = { null };
    Object[] invokeParams2 = { null, null };
    Object[] invokeParams3 = { null, null, null };

    public EntitySink() {
        this(null, (Class<?>[])null);
    }

    public EntitySink(Class<?> thisClass, final Class<?>[] otherClass) {
        if (thisClass == null) {
            thisClass = this.getClass();
        }
        ReflectSinks.validate(thisClass);

        final Method[] meths = thisClass.getDeclaredMethods();

        for (Method m : meths) {
            final String methName = m.getName();
            if (methName.startsWith("on")) {
                m = ReflectSinks.getAccessibleMethod(m);
                final String keyName = Strings.substringAfter(m.getName(), "on");
                int msgId = Enums.getValue(thisClass, "MSG_" + keyName);
                if (msgId == -1) {
                    if (otherClass != null) {
                        for (final Class<?> cl : otherClass) {
                            msgId = Enums.getValue(cl, "MSG_" + keyName);
                            if (msgId != -1)
                                break;
                        }
                    }
                    if (msgId == -1) {
                        msgId = Enums.getValue(EKronosEntityMessage.class, keyName);
                    }
                }
                if (msgId != -1) {
                    if (jumpTable == null) {
                        jumpTable = Collections.HashMap();
                    }
                    jumpTable.put(msgId, m);
                }
                else {
                    Log.w("Receiver method '" + m.getName() + "' cannot find a message to bind to.");
                }
            }
        }
    }

    @Override
    public boolean onKronosEntitySink(final IKronosEntity e, final int msg, final Object a, final Object b)
    {
        boolean r = false;
        /*
         * Call Message Method
         */
        if (jumpTable != null) {
            final Method m = jumpTable.get(msg);
            if (m != null) {
                try {
                    Object ir;
                    final Class<?>[] params = m.getParameterTypes();
                    switch (params.length) {
                    case 0:
                        ir = m.invoke(this);
                        break;
                    case 1:
                        invokeParams1[0] = getParam0(params[0], e, a, b);
                        ir = m.invoke(this, invokeParams1);
                        break;
                    case 2:
                        invokeParams2[0] = getParam0(params[0], e, a, b);
                        invokeParams2[1] = getParam1(params[1], e, a, b);
                        ir = m.invoke(this, invokeParams2);
                        break;
                    default:
                        invokeParams3[0] = getParam0(params[0], e, a, b);
                        invokeParams3[1] = getParam1(params[1], e, a, b);
                        invokeParams3[2] = getParam2(params[2], e, a, b);
                        ir = m.invoke(this, invokeParams3);
                        break;
                    }
                    if (ir instanceof Boolean) {
                        r = (Boolean)ir;
                    }
                    else if (ir == null) {
                        r = false;
                    }
                }
                catch (final Exception ex) {
                    throw new RuntimeException("Invoke error : " + m.getName(), ex);
                }
            }
        }

        return r;
    }

    private Object getParam0(final Class<?> paramType, final IKronosEntity e, final Object a, final Object b) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Object r;
        r = ReflectSinks.convertParamObject(paramType, e);
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, a);
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, b);
        if (r != null)
            return r;
        return null;
    }

    private Object getParam1(final Class<?> paramType, final IKronosEntity e, final Object a, final Object b) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Object r;
        r = ReflectSinks.convertParamObject(paramType, a);
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, b);
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, e);
        if (r != null)
            return r;
        return null;
    }

    private Object getParam2(final Class<?> paramType, final IKronosEntity e, final Object a, final Object b) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Object r;
        r = ReflectSinks.convertParamObject(paramType, b);
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, a);
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, e);
        if (r != null)
            return r;
        return null;
    }
}
