package selena.app;

import java.io.IOException;
import java.nio.IntBuffer;

import ni.aglGraphics.EGraphicsContextType;
import ni.aglGraphics.ETextureFlags;
import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.EPointerButton;
import ni.aglSystem.IMessageTargets;
import ni.aglSystem.Lib;
import ni.types.Vector2l;
import ni.types.Vector4l;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Cursor;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.PixelFormat;

import selena.SelenaException;
import selena.common.math.Rect;
import selena.common.util.Log;

abstract class LWJGLBaseWindow extends BaseAppWindow {
    private final Vector2l vMouseMove = new Vector2l();
    private final Vector2l vRelMouseMove = new Vector2l();
    protected Cursor currentCursor;
    protected Cursor invisibleCursor;
    protected IMessageTargets _wndMT;
    protected long[] lastMouseDown = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
    private final long DOUBLE_CLICK_INTERVAL = 300;

    protected LWJGLBaseWindow(final AppEventHandler eventHandler)
            throws SelenaException, IOException {
        super(eventHandler);
        construct();
    }

    @Override
    protected void dispose() {
        super.dispose();
    }

    abstract protected void create(Vector4l r) throws LWJGLException,
            SelenaException;

    abstract protected void updateWindow();

    protected void postInputMessage(final int msg, final Object a) {
        _wndMT.postMessage(msg, a, null);
    }

    protected void postEventMessage(final int msg) {
        _wndMT.postMessage(msg, null, null);
    }

    @Override
    protected AppConfig createAppConfig() {
        final AppConfig cfg = new AppConfig();
        if (!cfg.isEmbedded) {
            if (!cfg.renDriver.contains("GL")) {
                cfg.renDriver = "renGL1";
            }
        }
        return cfg;
    }

    private void updateKeyboard() {
        // Keyboard events, LWJGL uses the same KeyCode as eKey (lucky us :P)
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                postInputMessage(EOSWindowMessage.KeyDown,
                        Keyboard.getEventKey());
                final int ch = Keyboard.getEventCharacter();
                if (ch > 0) {
                    postInputMessage(EOSWindowMessage.KeyChar, ch);
                }
            }
            else {
                postInputMessage(EOSWindowMessage.KeyUp, Keyboard.getEventKey());
            }
        }
    }

    private int getViewHeight() {
        if (Display.getParent() != null)
            return Display.getParent().getHeight();
        else
            return Display.getDisplayMode().getHeight();
    }

    private void updateMouse() {
        // Mouse events
        while (Mouse.next()) {
            final int bt = Mouse.getEventButton();
            switch (bt) {
            case -1: {
                // Relative Mouse Move
                {
                    vRelMouseMove.x = Mouse.getEventDX();
                    vRelMouseMove.y = -Mouse.getEventDY();
                    if ((vRelMouseMove.x != 0) || (vRelMouseMove.y != 0)) {
                        postInputMessage(EOSWindowMessage.RelativeMouseMove,
                                vRelMouseMove);
                    }
                }
                // Mouse Move
                {
                    final int newX = Mouse.getEventX();
                    final int newY = getViewHeight() - Mouse.getEventY();
                    if ((newX != vMouseMove.x) || (newY != vMouseMove.y)) {
                        vMouseMove.x = newX;
                        vMouseMove.y = newY;
                        postInputMessage(EOSWindowMessage.MouseMove, vMouseMove);
                        try {
                            if (Mouse.isInsideWindow()) {
                                if (currentCursor != invisibleCursor) {
                                    Mouse.setNativeCursor(invisibleCursor);
                                    currentCursor = invisibleCursor;
                                }
                            }
                            else {
                                if (currentCursor != null) {
                                    Mouse.setNativeCursor(null);
                                    currentCursor = null;
                                }
                            }
                        }
                        catch (final LWJGLException ex) {
                            Log.e(ex);
                        }
                    }
                }
                // Wheel
                {
                    final int wheel = Mouse.getEventDWheel();
                    if (wheel != 0) {
                        final float v = (wheel) / 120.0f;
                        postInputMessage(EOSWindowMessage.MouseWheel, v);
                    }
                }
                break;
            }
            default:
                if (Mouse.getEventButtonState()) {
                    postInputMessage(EOSWindowMessage.MouseButtonDown, bt);
                    if (bt < lastMouseDown.length) {
                        final long thisDown = Mouse.getEventNanoseconds() / 1000000; // to milliseconds
                        final long lastDown = lastMouseDown[bt];
                        lastMouseDown[bt] = thisDown;
                        if (lastDown > 0) {
                            final long msSinceLast = (thisDown - lastDown);
                            if (msSinceLast <= DOUBLE_CLICK_INTERVAL) {
                                postInputMessage(EOSWindowMessage.MouseButtonDoubleClick, EPointerButton.DoubleClickBt0 + bt);
                            }
                        }
                    }
                }
                else {
                    postInputMessage(EOSWindowMessage.MouseButtonUp, bt);
                }
                break;
            }
        }
    }

    @Override
    protected boolean update() {
        updateWindow();
        updateKeyboard();
        updateMouse();
        return super.update();
    }

    @Override
    protected void draw() {
        super.draw();
        // Update the window. If the window is visible clears the dirty flag and calls swapBuffers() and finally polls the input devices.
        Display.update();
    }

    @Override
    protected void initWindowAndGraphicsContext() throws SelenaException {
        //// Create the window ////
        Vector4l r = _config.windowRect;
        if (r == null) {
            r = sys.getMonitorRect(0);
            r.setWidth((int)(r.getWidth() * 0.6));
            r.setHeight((int)(r.getHeight() * 0.6));
            if ((r.getWidth() < 10) || (r.getHeight() < 10)) {
                r.set(0, 0, 1024, 768);
            }
        }
        if (_config.windowCenter) {
            final Vector4l mr = sys.getMonitorRect(0);
            Rect.moveTo(r, r,
                    (mr.getWidth() - r.getWidth()) / 2,
                    (mr.getHeight() - r.getHeight()) / 2);
        }

        // Create the generic window object
        try {
            create(r);
            final PixelFormat pixelFormat = new PixelFormat(32, 8, 24, 8, 0); // bpp, alfa, dep, sten, fsaa
            Display.create(pixelFormat);
            final IntBuffer img = IntBuffer.allocate(16 * 16);
            invisibleCursor = new Cursor(16, 16, 7, 7, 1, img, null);
        }
        catch (final LWJGLException e) {
            throw new SelenaException("LWJGL initialization error !", e);
        }
        Keyboard.enableRepeatEvents(true);

        //// Create the graphics context ////
        if (!_graphics.initializeDriver(Lib.hstr(_config.renDriver)))
            throw new SelenaException("Can't initialize graphics driver !");

        _graphicsContext = _graphics.createContext(EGraphicsContextType.Driver,
                _wnd, null, null, 0, 0, _config.renBBFmt, _config.renDSFmt,
                false, _config.renSwapInterval,
                // ETextureFlags.Virtual asks the driver to not create the context by itself and use the one we already have...
                _config.renBBFlags | ETextureFlags.Virtual);
        if (_graphicsContext == null)
            throw new SelenaException("Can't create graphics context !");
    }
}