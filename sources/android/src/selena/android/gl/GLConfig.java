package selena.android.gl;

import javax.microedition.khronos.opengles.GL10;

import selena.common.util.Log;

public class GLConfig {
    public int glesVersion;
    /** The number of bits requested for the red component */
    public int redSize;
    /** The number of bits requested for the green component */
    public int greenSize;
    /** The number of bits requested for the blue component */
    public int blueSize;
    /** The number of bits requested for the alpha component */
    public int alphaSize;
    /** The number of bits requested for the depth component */
    public int depthSize;
    /** The number of bits requested for the stencil component */
    public int stencilSize;
    /** The prefered number of AA samples **/
    public int aaSamples;
    /** GL strings */
    public String strVendor = "";
    public String strRenderer = "";
    public String strVersion = "";

    GLConfig() {
        glesVersion = GLDefaultOptions.USE_GL_ES2 ? 2 : 1;
        redSize = GLDefaultOptions.TRANSLUCENT_WINDOW ? 8 : 5;
        greenSize = GLDefaultOptions.TRANSLUCENT_WINDOW ? 8 : 6;
        blueSize = GLDefaultOptions.TRANSLUCENT_WINDOW ? 8 : 5;
        alphaSize = GLDefaultOptions.TRANSLUCENT_WINDOW ? 8 : 0;
        depthSize = GLDefaultOptions.USE_DEPTH_BUFFER_24 ? 24 : 16;
        stencilSize = 0;
        aaSamples = GLDefaultOptions.USE_AA ? 4 : 1;
    }
}
