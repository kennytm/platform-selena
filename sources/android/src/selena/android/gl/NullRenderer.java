package selena.android.gl;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.view.SurfaceHolder;

/**
 * A null renderer implementation.
 */
public class NullRenderer implements Renderer {
    @Override
    public SurfaceHolder getHolder() {
        return null;
    }

    @Override
    public boolean onInitConfig(final GLConfig aConfig) {
        return true;
    }

    @Override
    public void onThreadExit() {
    }

    @Override
    public void onSurfaceCreated(final GL10 gl, final EGLConfig config) {
    }

    @Override
    public void onSurfaceChanged(final GL10 gl, final int width, final int height) {
    }

    @Override
    public void onSurfaceLost() {
    }

    @Override
    public void onDrawFrame(final GL10 gl) {
    }
}
