package selena.app;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

import ni.aglSystem.ECreateInstanceFlags;
import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.IOSWindow;
import ni.types.Vector4l;

import org.lwjgl.LWJGLException;
import org.lwjgl.LWJGLUtil;
import org.lwjgl.opengl.Display;

import selena.SelenaException;
import selena.common.math.Rect;
import selena.common.math.Vec;

final class LWJGLWindowResizable extends LWJGLBaseWindow {
    private Vector4l _wndRect;
    private Frame _frame;
    private Canvas _canvas;
    private AtomicReference<Dimension> _newCanvasSize;

    public LWJGLWindowResizable(final AppEventHandler eventHandler)
            throws SelenaException, IOException {
        super(eventHandler);
    }

    @Override
    protected void dispose() {
        super.dispose();
        Display.destroy();
        _frame.dispose();
        java.lang.System.exit(0);
    }

    @Override
    protected void create(final Vector4l r) throws LWJGLException,
            SelenaException
    {
        //// Create the window ////
        _wndRect = r.clone();

        // Create the generic window object
        _wnd = IOSWindow.query(sys.createInstance("aglSystem.OSWindowGeneric",
                null, null, ECreateInstanceFlags.Default, 0, 0, 0));
        if (_wnd == null)
            throw new SelenaException("Can't create the application's window !");
        _wndMT = _wnd.getMessageTargets();

        // Create frame
        _frame = new Frame();
        _frame.setTitle(_config.windowTitle);
        _frame.setSize(_wndRect.getWidth(), _wndRect.getHeight());
        _frame.setLocation(_wndRect.x, _wndRect.y);
        Rect.moveTo(_wndRect, Vec.VEC2L_ZERO);

        // Create Canvas in frame
        _newCanvasSize = new AtomicReference<Dimension>();
        _canvas = new Canvas();
        _canvas.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(final ComponentEvent e) {
                _newCanvasSize.set(_canvas.getSize());
            }
        });

        _frame.add(_canvas, BorderLayout.CENTER);
        if (LWJGLUtil.getPlatform() == LWJGLUtil.PLATFORM_WINDOWS) {
            _frame.addWindowFocusListener(new WindowAdapter() {
                @Override
                public void windowGainedFocus(final WindowEvent e) {
                    _canvas.requestFocusInWindow();
                }
            });
        }
        _frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                postEventMessage(EOSWindowMessage.Close);
            }
        });
        _frame.setVisible(true);

        _wndRect.setSize(_canvas.getWidth(), _canvas.getHeight());
        _wnd.setRect(_wndRect);
        Display.setParent(_canvas);
    }

    @Override
    protected void updateWindow() {
        final Dimension newSize = _newCanvasSize.getAndSet(null);
        if (newSize != null) {
            _wndRect.setWidth(newSize.width);
            _wndRect.setHeight(newSize.height);
            _wnd.setRect(_wndRect);
            postEventMessage(EOSWindowMessage.Size);
        }
    }
}
