package selena.msg;

import ni.aglSystem.IMessageSink;
import ni.aglSystem.IMessageTarget;
import ni.aglSystem.Lib;

public class MessageTarget extends MessageSink {
    final protected IMessageTarget mt;
    final protected int tid;

    public MessageTarget() {
        this(null, null);
    }

    public MessageTarget(final Class<?>... otherClass) {
        this(null, otherClass);
    }

    public MessageTarget(final Class<?> thisClass, final Class<?>[] otherClass) {
        super(thisClass, otherClass);
        mt = Lib.getSystem().createMessageTargetLocal(null, IMessageSink.impl(this));
        tid = mt.getTargetID();
    }

    public int getTargetID() {
        return tid;
    }

    public void send(final int msgId) {
        Msg.sendMessage(tid, msgId, null, null);
    }

    public void send(final int msgId, final Object a) {
        Msg.sendMessage(tid, msgId, a, null);
    }

    public void send(final int msgId, final Object a, final Object b) {
        Msg.sendMessage(tid, msgId, a, b);
    }

    public void post(final int msgId) {
        Msg.postMessage(tid, msgId, null, null);
    }

    public void post(final int msgId, final Object a) {
        Msg.postMessage(tid, msgId, a, null);
    }

    public void post(final int msgId, final Object a, final Object b) {
        Msg.postMessage(tid, msgId, a, b);
    }
}
