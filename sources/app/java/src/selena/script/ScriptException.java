package selena.script;

import selena.SelenaException;

public class ScriptException extends SelenaException {
    private static final long serialVersionUID = 1L;

    public ScriptException() {
        super();
    }

    public ScriptException(final Throwable t) {
        super(t);
    }

    public ScriptException(final String message) {
        super(message);
    }

    public ScriptException(final String message, final Throwable t) {
        super(message, t);
    }
}