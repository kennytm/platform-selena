package selena.common.util;

import java.util.HashSet;

public class Log {
    protected Log() {
    }

    private static final String THIS_CLASSNAME = Log.class.getName();

    public static boolean ENABLED = true;
    public static boolean PRINT_STACKS = true;

    public final static int Verbose = (1 << 0);
    public final static int Debug = (1 << 1);
    public final static int Info = (1 << 2);
    public final static int Normal = (1 << 2);
    public final static int Console = (1 << 3);
    public final static int Memory = (1 << 4);
    public final static int Server = (1 << 5);
    public final static int Client = (1 << 6);
    public final static int ProfilerEnter = (1 << 8);
    public final static int ProfilerLeave = (1 << 9);
    public final static int Profiler = (1 << 10);
    public final static int ProgressEnter = (1 << 11);
    public final static int ProgressLeave = (1 << 12);
    public final static int Progress = (1 << 13);
    public final static int FunctionEnter = (1 << 14);
    public final static int FunctionLeave = (1 << 15);
    public final static int Function = (1 << 16);
    public final static int User0 = (1 << 17);
    public final static int User1 = (1 << 18);
    public final static int User2 = (1 << 19);
    public final static int User3 = (1 << 20);
    public final static int User4 = (1 << 21);
    public final static int User5 = (1 << 22);
    public final static int User6 = (1 << 23);
    public final static int User7 = (1 << 24);
    public final static int Warning = (1 << 29);
    public final static int Error = (1 << 30);
    public final static int Undef = ~0;

    public interface ISink {
        public void log(int type, String tag, String file, String func, int line, String msg);
    };

    private static final long mBaseTime = System.currentTimeMillis();
    private static long mPrevTime = -1;
    private static StringBuilder mBuilder = null;
    private static HashSet<ISink> msetSinks = null;

    private static long getTime() {
        return System.currentTimeMillis() - mBaseTime;
    }

    /**
     * Register a sink that will be called every time a message is logged.
     */
    public static void registerSink(final ISink sink) {
        if (msetSinks == null) {
            msetSinks = new HashSet<ISink>();
        }
        msetSinks.add(sink);
    }

    /**
     * Write the format of a log time tag in the specified StringBuilder.
     */
    public static void formatTime(final StringBuilder sb, final String tag, final long timeMs, final long prevTimeMs)
    {
        if (timeMs >= 0) {
            sb.append("[");
            sb.append(timeMs);
            if (prevTimeMs >= 0) {
                sb.append(":");
                sb.append(timeMs - prevTimeMs);
            }
            sb.append("]");
        }
        if (tag != null) {
            sb.append(" <");
            sb.append(tag);
            sb.append("> ");
        }
    }

    /**
     * Write the format of a log type and function in the specified
     * StringBuilder.
     */
    public static void formatTypeAndFunc(final StringBuilder sb, final int type, final String func)
    {
        switch (type) {
        case Verbose:
            sb.append("Verbose");
            break;
        case Debug:
            sb.append("Debug");
            break;
        case Info:
            sb.append("Info");
            break;
        case Warning:
            sb.append("Warning");
            break;
        case Error:
            sb.append("Error");
            break;
        case Console:
            sb.append("Console");
            break;
        case Memory:
            sb.append("Memory");
            break;
        case Server:
            sb.append("Server");
            break;
        case Client:
            sb.append("Client");
            break;
        case ProfilerEnter:
            sb.append("ProfilerEnter");
            break;
        case ProfilerLeave:
            sb.append("ProfilerLeave");
            break;
        case Profiler:
            sb.append("Profiler");
            break;
        case ProgressEnter:
            sb.append("ProgressEnter");
            break;
        case ProgressLeave:
            sb.append("ProgressLeave");
            break;
        case Progress:
            sb.append("Progress");
            break;
        case FunctionEnter:
            sb.append("FunctionEnter");
            break;
        case FunctionLeave:
            sb.append("FunctionLeave");
            break;
        case Function:
            sb.append("Function");
            break;
        case User0:
            sb.append("User0");
            break;
        case User1:
            sb.append("User1");
            break;
        case User2:
            sb.append("User2");
            break;
        case User3:
            sb.append("User3");
            break;
        case User4:
            sb.append("User4");
            break;
        case User5:
            sb.append("User5");
            break;
        case User6:
            sb.append("User6");
            break;
        case User7:
            sb.append("User7");
            break;
        case Undef:
            sb.append("Undef");
            break;
        default:
            sb.append("(");
            sb.append(type);
            sb.append(")");
            break;
        }
        if (func != null) {
            if (type != Undef) {
                sb.append(" ");
            }
            sb.append("in ");
            sb.append(func);
        }
    }

    /**
     * Write a formatted log message in the specified StringBuilder.
     */
    public static void formatMessage(
            final StringBuilder sb,
            final int type,
            final String tag,
            final String file,
            final String func,
            final int line,
            final String msg,
            final long time,
            final long prevTime)
    {
        if (type == Undef) {
            sb.append("-----------------------------------------------------------\n");
        }
        else {
            formatTime(sb, tag, time, prevTime);
            sb.append("[");
            formatTypeAndFunc(sb, type, func);
            sb.append("]: (");
            sb.append(file);
            sb.append(":");
            sb.append(line);
            sb.append(")\n");
            sb.append(msg);
            if (!msg.endsWith("\n")) {
                sb.append("\n");
            }
        }
    }

    public static void formatThrowable(final StringBuilder sb, final String msg, final Throwable e)
    {
        if (msg != null) {
            sb.append(msg);
            if (!msg.endsWith("\n")) {
                sb.append("\n");
            }
        }
        sb.append("Exception: ");
        sb.append(e.getMessage());
        sb.append("\n");
        if (PRINT_STACKS) {
            sb.append(Strings.getStackTrace(e));
        }
    }

    synchronized public static String getThrowableMessage(final String msg, final Throwable e)
    {
        if (mBuilder == null) {
            mBuilder = new StringBuilder();
        }
        else {
            mBuilder.setLength(0);
        }
        formatThrowable(mBuilder, msg, e);
        return mBuilder.toString();
    }

    synchronized public static void systemOutLog(final int type, final String tag, final String file, final String func, final int line, final String msg)
    {
        if (mBuilder == null) {
            mBuilder = new StringBuilder();
        }
        else {
            mBuilder.setLength(0);
        }
        final long time = getTime();
        formatMessage(mBuilder, type, tag, file, func, line, msg, time, mPrevTime);
        mPrevTime = time;
        System.out.print(mBuilder);
    }

    private static int findFirstStackTraceElementAfterClass(final StackTraceElement[] s)
    {
        if (s == null)
            return -1;
        boolean foundClass = false;
        for (int i = 0; i < s.length; ++i) {
            if (!s[i].isNativeMethod() && s[i].getClassName().equals(THIS_CLASSNAME)) {
                foundClass = true;
            }
            else if (foundClass) {
                return i;
            }
        }
        return -1;
    }

    public static void doLog(final int type, final String msg) {
        final StackTraceElement[] stackTraceElements = Thread.currentThread()
                .getStackTrace();
        final int stackIndex = findFirstStackTraceElementAfterClass(stackTraceElements);
        if (stackIndex < 0) {
            final String noStack = "<nostack>";
            if ((msetSinks != null) && !msetSinks.isEmpty()) {
                for (final ISink s : msetSinks) {
                    s.log(type, noStack, noStack, noStack, -1, msg);
                }
            }
            else {
                systemOutLog(type, noStack, noStack, noStack, -1, msg);
            }
        }
        else {
            if ((msetSinks != null) && !msetSinks.isEmpty()) {
                for (final ISink s : msetSinks) {
                    s.log(type,
                            stackTraceElements[stackIndex].getClassName(),
                            stackTraceElements[stackIndex].getFileName(),
                            stackTraceElements[stackIndex].getMethodName(),
                            stackTraceElements[stackIndex].getLineNumber(),
                            msg);
                }
            }
            else {
                systemOutLog(type,
                        stackTraceElements[stackIndex].getClassName(),
                        stackTraceElements[stackIndex].getFileName(),
                        stackTraceElements[stackIndex].getMethodName(),
                        stackTraceElements[stackIndex].getLineNumber(),
                        msg);
            }
        }
    }

    public static void i(final String msg) {
        if (!ENABLED)
            return;
        doLog(Info, msg);
    }

    public static void i(final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Info, getThrowableMessage(null, e));
    }

    public static void i(final String msg, final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Info, getThrowableMessage(msg, e));
    }

    public static void w(final String msg) {
        if (!ENABLED)
            return;
        doLog(Warning, msg);
    }

    public static void w(final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Warning, getThrowableMessage(null, e));
    }

    public static void w(final String msg, final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Warning, getThrowableMessage(msg, e));
    }

    public static void e(final String msg) {
        if (!ENABLED)
            return;
        doLog(Error, msg);
    }

    public static void e(final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Error, getThrowableMessage(null, e));
    }

    public static void e(final String msg, final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Error, getThrowableMessage(msg, e));
    }

    public static void d(final String msg) {
        if (!ENABLED)
            return;
        doLog(Debug, msg);
    }

    public static void d(final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Debug, getThrowableMessage(null, e));
    }

    public static void d(final String msg, final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Debug, getThrowableMessage(msg, e));
    }

    public static void v(final String msg) {
        if (!ENABLED)
            return;
        doLog(Verbose, msg);
    }

    public static void v(final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Verbose, getThrowableMessage(null, e));
    }

    public static void v(final String msg, final Throwable e) {
        if (!ENABLED)
            return;
        doLog(Verbose, getThrowableMessage(msg, e));
    }
};
