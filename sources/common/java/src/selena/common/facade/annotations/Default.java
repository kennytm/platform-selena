package selena.common.facade.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import selena.common.facade.command.Converter;

/**
 * Use this annotation to mark a command parameter that have a default value.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
@Documented
public @interface Default {
    /** The default value of the RPC parameter. */
    public String value();

    @SuppressWarnings("rawtypes")
    public Class<? extends Converter> converter() default Converter.class;
}
