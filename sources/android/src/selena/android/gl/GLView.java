package selena.android.gl;

import java.util.LinkedList;
import java.util.Queue;

import javax.microedition.khronos.opengles.GL10;

import selena.android.gl.EglHelper.EGLConfigChooser;
import selena.common.util.Log;
import android.content.pm.ConfigurationInfo;

/**
 * An implementation of SurfaceView that uses the dedicated surface for
 * displaying OpenGL rendering.
 * <p>
 * A GLSurfaceView provides the following features:
 * <p>
 * <ul>
 * <li>Manages a surface, which is a special piece of memory that can be
 * composited into the Android view system.
 * <li>Manages an EGL display, which enables OpenGL to render into a surface.
 * <li>Accepts a user-provided Renderer object that does the actual rendering.
 * <li>Renders on a dedicated thread to decouple rendering performance from the
 * UI thread.
 * <li>Supports both on-demand and continuous rendering.
 * <li>Optionally wraps, traces, and/or error-checks the renderer's OpenGL
 * calls.
 * </ul>
 * <h3>Using GLSurfaceView</h3>
 * <p>
 * Typically you use GLSurfaceView by subclassing it and overriding one or more
 * of the View system input event methods. If your application does not need to
 * override event methods then GLSurfaceView can be used as-is. For the most
 * part GLSurfaceView behavior is customized by calling "set" methods rather
 * than by subclassing. For example, unlike a regular View, drawing is delegated
 * to a separate Renderer object which is registered with the GLSurfaceView
 * using the {@link #setRenderer(Renderer)} call.
 * <p>
 * <h3>Initializing GLSurfaceView</h3> All you have to do to initialize a
 * GLSurfaceView is call {@link #setRenderer(Renderer)}. However, if desired,
 * you can modify the default behavior of GLSurfaceView by calling one or more
 * of these methods before calling setRenderer:
 * <ul>
 * <li>{@link #setDebugFlags(int)}
 * <li>{@link #setEGLConfigChooser(boolean)}
 * <li>{@link #setEGLConfigChooser(EGLConfigChooser)}
 * <li>{@link #setEGLConfigChooser(int, int, int, int, int, int)}
 * <li>{@link #setGLWrapper(GLWrapper)}
 * </ul>
 * <p>
 * <h4>Choosing an EGL Configuration</h4> A given Android device may support
 * multiple possible types of drawing surfaces. The available surfaces may
 * differ in how may channels of data are present, as well as how many bits are
 * allocated to each channel. Therefore, the first thing GLSurfaceView has to do
 * when starting to render is choose what type of surface to use.
 * <p>
 * By default GLSurfaceView chooses an available surface that's closest to a
 * 16-bit R5G6B5 surface with a 16-bit depth buffer and no stencil. If you would
 * prefer a different surface (for example, if you do not need a depth buffer)
 * you can override the default behavior by calling one of the
 * setEGLConfigChooser methods.
 * <p>
 * <h4>Debug Behavior</h4> You can optionally modify the behavior of
 * GLSurfaceView by calling one or more of the debugging methods
 * {@link #setDebugFlags(int)}, and {@link #setGLWrapper}. These methods may be
 * called before and/or after setRenderer, but typically they are called before
 * setRenderer so that they take effect immediately.
 * <p>
 * <h4>Setting a Renderer</h4> Finally, you must call {@link #setRenderer} to
 * register a {@link Renderer}. The renderer is responsible for doing the actual
 * OpenGL rendering.
 * <p>
 * <h3>Rendering Mode</h3> Once the renderer is set, you can control whether the
 * renderer draws continuously or on-demand by calling {@link #setRenderMode}.
 * The default is continuous rendering.
 * <p>
 * <h3>Activity Life-cycle</h3> A GLSurfaceView must be notified when the
 * activity is paused and resumed. GLSurfaceView clients are required to call
 * {@link #onPause()} when the activity pauses and {@link #onResume()} when the
 * activity resumes. These calls allow GLSurfaceView to pause and resume the
 * rendering thread, and also allow GLSurfaceView to release and recreate the
 * OpenGL display.
 * <p>
 * <h3>Handling events</h3>
 * <p>
 * To handle an event you will typically subclass GLSurfaceView and override the
 * appropriate method, just as you would with any other View. However, when
 * handling the event, you may need to communicate with the Renderer object
 * that's running in the rendering thread. You can do this using any standard
 * Java cross-thread communication mechanism. In addition, one relatively easy
 * way to communicate with your renderer is to call {@link #pushJob(GvmGLJob)}.
 * For example:
 * 
 * <pre class="prettyprint">
 * class MyGLSurfaceView extends GLSurfaceView {
 * 
 *     private MyRenderer mMyRenderer;
 * 
 *     public void start() {
 *         mMyRenderer = ...;
 *         setRenderer(mMyRenderer);
 *     }
 * 
 *     public boolean onKeyDown(int keyCode, KeyEvent event) {
 *         if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
 *             pushJob(new GvmGLJob() {
 *                 public void Run(GL10 gl) {
 *                     mMyRenderer.handleDpadCenter();
 *                 }
 *             });
 *             return true;
 *         }
 *         return super.onKeyDown(keyCode, event);
 *     }
 * }
 * </pre>
 */
public class GLView {
    /**
     * Set the renderer associated with this view. Also starts the thread that
     * will call the renderer, which in turn causes the rendering to start.
     * <p>
     * The following GLView methods can only be called <em>before</em>
     * setRenderer is called:
     * <ul>
     * <li>{@link #setEGLConfigChooser(boolean)}
     * <li>{@link #setEGLConfigChooser(EGLConfigChooser)}
     * <li>{@link #setEGLConfigChooser(int, int, int, int, int, int)}
     * </ul>
     * <p>
     * The following GLSurfaceView methods can only be called <em>after</em>
     * setRenderer is called:
     * <ul>
     * <li>{@link #getRenderMode()}
     * <li>{@link #onPause()}
     * <li>{@link #onResume()}
     * <li>{@link #pushJob(GvmGLJob)}
     * <li>{@link #requestRender()}
     * <li>{@link #setRenderMode(int)}
     * </ul>
     * 
     * @param renderer the renderer to use to perform OpenGL drawing.
     */
    public void startup(final Renderer renderer) {
        checkRenderThreadState();
        mGLThread = new GLThread(mEglHelper, renderer);
        mGLThread.start();
    }

    public boolean isStarted() {
        return mGLThread != null;
    }

    public void shutdown() {
        requestExitAndWait();
    }

    public int waitForNFrames(final int nFrames) {
        if (mGLThread == null)
            return -1;
        return mGLThread.waitForNFrames(nFrames);
    }

    public int getNumFrames() {
        if (mGLThread == null)
            return -1;
        synchronized (sGLThreadManager) {
            return mGLThread.mNumFrames;
        }
    }

    /**
     * Get the EglHelper associated with the view.
     */
    public EglHelper getEglHelper() {
        return mEglHelper;
    }

    /**
     * Set the minimum of time a frame should take, if onDrawFrame takes less
     * time than this the thread will sleep until mMinFrameTime is reached. Set
     * 0 for no minimum.
     * <p>
     * Can be called only after startup()
     */
    public void setMinFrameTime(final int nMinFrameTimeInMs) {
        if (mGLThread == null)
            return;
        synchronized (sGLThreadManager) {
            mGLThread.mMinFrameTime = nMinFrameTimeInMs;
        }
    }

    /**
     * Get the minimum of time a frame should take.
     * <p>
     * Can be called only after startup()
     */
    public int getMinFrameTime() {
        if (mGLThread == null)
            return 0;
        synchronized (sGLThreadManager) {
            return mGLThread.mMinFrameTime;
        }
    }

    /**
     * Request that the renderer render some frames.
     */
    public void render() {
        if (mGLThread != null) {
            mGLThread.render();
        }
    }

    /**
     * <ul>
     * <li>&lt;0 : continuous rendering (request rendering a frame at the end of
     * each frame)</li>
     * <li>&gt;0 : renders for the specified number of frames, render() queues
     * setNumFrameToAddOnRender() frames</li>
     * </ul>
     */
    public void setFrameToRender(final int frameToRender) {
        if (mGLThread == null)
            return;
        synchronized (sGLThreadManager) {
            mGLThread.mFrameToRender = frameToRender;
            if (mGLThread.mFrameToRender > mGLThread.mMaxFrameToRender)
                mGLThread.mFrameToRender = mGLThread.mMaxFrameToRender;
        }
    }

    /**
     * Set the maximum number of frames that can be queued.
     */
    public void setMaxFrameToRender(final int maxFrameToRender) {
        if (mGLThread == null)
            return;
        synchronized (sGLThreadManager) {
            mGLThread.mMaxFrameToRender = maxFrameToRender;
        }
    }

    /**
     * Number of frames to queue when rendering is requested.
     */
    public void setNumFrameToAddOnRender(final int numFrameToAddOnRender) {
        if (mGLThread == null)
            return;
        synchronized (sGLThreadManager) {
            mGLThread.mNumFrameToAddOnRender = numFrameToAddOnRender;
        }
    }

    /**
     * Must be called when the surface is created.
     */
    public void onSurfaceCreated() {
        if (mGLThread != null) {
            mGLThread.surfaceCreated();
        }
    }

    /**
     * Must be called when the surface is destroyed.
     */
    public void onSurfaceDestroyed() {
        if (mGLThread != null) {
            // Surface will be destroyed when we return
            mGLThread.surfaceDestroyed();
        }
    }

    /**
     * Must be called when the surface changed.
     */
    public void onSurfaceChanged(final int w, final int h) {
        if (mGLThread != null) {
            mGLThread.onWindowResize(w, h);
        }
    }

    /**
     * Inform the view that the activity is paused. The owner of this view must
     * call this method when the activity is paused. Calling this method will
     * pause the rendering thread. Must not be called before a renderer has been
     * set.
     */
    public void onPause() {
        if (mGLThread != null) {
            mGLThread.onPause();
        }
    }

    /**
     * Inform the view that the activity is resumed. The owner of this view must
     * call this method when the activity is resumed. Calling this method will
     * recreate the OpenGL display and resume the rendering thread. Must not be
     * called before a renderer has been set.
     */
    public void onResume() {
        if (mGLThread != null) {
            mGLThread.onResume();
        }
    }

    public void setSafeMode(final boolean safeMode) {
        if (mGLThread != null) {
            mGLThread.setSafeMode(safeMode);
        }
    }

    public void onDestroy() {
        Log.d("onDestroy");
        shutdown();
    }

    public void setNullRenderer() {
        if (mGLThread != null) {
            synchronized (sGLThreadManager) {
                mGLThread.mRenderer = new NullRenderer();
            }
        }
    }

    public Renderer getRenderer() {
        if (mGLThread != null) {
            synchronized (sGLThreadManager) {
                return mGLThread.mRenderer;
            }
        }
        return null;
    }

    public void requestExitAndWait() {
        if (mGLThread != null) {
            mGLThread.requestExitAndWait();
        }
    }

    /**
     * Inform the view that the window focus has changed.
     */
    public void onWindowFocusChanged(final boolean hasFocus) {
        Log.d("onWindowFocusChanged: " + hasFocus);
        if (mGLThread != null) {
            mGLThread.onWindowFocusChanged(hasFocus);
        }
    }

    private void checkRenderThreadState() {
        if (mGLThread != null) {
            throw new IllegalStateException(
                    "setRenderer has already been called for this instance.");
        }
    }

    public void swapBuffers() {
        /* No explicit swap buffer, because it is too unstable on some devices such as the M9 */
    }

    // ----------------------------------------------------------------------
    public boolean pushJob(final GLJob aJob) {
        if (mGLThread != null) {
            mGLThread.pushJob(aJob);
        }
        return false;
    }

    private GLThread mGLThread;
    private final EglHelper mEglHelper = new EglHelper();

    /**
     * A generic GL Thread. Takes care of initializing EGL and GL. Delegates to
     * a Renderer instance to do the actual drawing. Can be configured to render
     * continuously or on request. All potentially blocking synchronization is
     * done through the sGLThreadManager object. This avoids multiple-lock
     * ordering issues.
     */
    private class GLThread extends Thread {
        public GLThread(final EglHelper eglHelper, final Renderer renderer) {
            super();
            mEglHelper = eglHelper;
            mWidth = 0;
            mHeight = 0;
            mRequestRender = true;
            mRenderer = renderer;
        }

        @Override
        public void run() {
            setName("GvmGLThread " + getId());
            if (GLDefaultOptions.LOG_THREADS) {
                Log.i("starting tid=" + getId());
            }

            try {
                guardedRun();
            }
            catch (final InterruptedException e) {
                // fall thru and exit normally
            }
            finally {
                sGLThreadManager.threadExiting(this);
            }
        }

        /*
         * This private method should only be called inside a
         * synchronized(sGLThreadManager) block.
         */
        private void stopEglLocked() {
            if (mHaveEglSurface) {
                mHaveEglSurface = false;
                mEglHelper.destroySurface();
                sGLThreadManager.releaseEglSurfaceLocked(this);
            }
        }

        private void guardedRun() throws InterruptedException {
            mHaveEglContext = false;
            mHaveEglSurface = false;

            try {
                GL10 gl = null;
                boolean createEglSurface = false;
                boolean sizeChanged = false;
                boolean doRenderNotification = false;
                int w = 0;
                int h = 0;
                int framesSinceResetHack = 0;

                if (!mRenderer.onInitConfig(mEglHelper.getConfig())) {
                    Log.v("Renderer can't initialize config !");
                    return;
                }

                while (true) {
                    synchronized (sGLThreadManager) {
                        while (true) {
                            if (mShouldExit) {
                                break;
                            }

                            // Do we need to release the EGL surface?
                            if (mHaveEglSurface && mPaused) {
                                if (GLDefaultOptions.LOG_SURFACE) {
                                    Log.i("releasing EGL surface because paused tid=" + getId());
                                }
                                stopEglLocked();
                            }

                            // Have we lost the surface view surface?
                            if ((!mHasSurface) && (!mWaitingForSurface)) {
                                if (GLDefaultOptions.LOG_SURFACE) {
                                    Log.i("noticed surfaceView surface lost tid=" + getId());
                                }
                                if (mHaveEglSurface) {
                                    stopEglLocked();
                                }
                                mWaitingForSurface = true;
                                sGLThreadManager.notifyAll();
                            }

                            // Have we acquired the surface view surface?
                            if (mHasSurface && mWaitingForSurface) {
                                if (GLDefaultOptions.LOG_SURFACE) {
                                    Log.i("noticed surfaceView surface acquired tid=" + getId());
                                }
                                mWaitingForSurface = false;
                                sGLThreadManager.notifyAll();
                            }

                            if (doRenderNotification) {
                                mWantRenderNotification = false;
                                doRenderNotification = false;
                                mRenderCompleteFrame = mNumFrames;
                                sGLThreadManager.notifyAll();
                            }

                            // Ready to draw?
                            if ((!mPaused) && mHasSurface
                                && (mWidth > 0) && (mHeight > 0)
                                && (mRequestRender)) {

                                if (mHaveEglContext && !mHaveEglSurface) {
                                    // Let's make sure the context hasn't been lost.
                                    if (!mEglHelper.verifyContext()) {
                                        mEglHelper.finish();
                                        mRenderer.onSurfaceLost();
                                        mHaveEglContext = false;
                                    }
                                }
                                // If we don't have an egl surface, try to acquire one.
                                if ((!mHaveEglContext) && sGLThreadManager.tryAcquireEglSurfaceLocked(this)) {
                                    mHaveEglContext = true;
                                    mEglHelper.start();

                                    sGLThreadManager.notifyAll();
                                }

                                if (mHaveEglContext && !mHaveEglSurface) {
                                    mHaveEglSurface = true;
                                    createEglSurface = true;
                                    sizeChanged = true;
                                }

                                if (mHaveEglSurface) {
                                    if (mSizeChanged) {
                                        sizeChanged = true;
                                        w = mWidth;
                                        h = mHeight;
                                        mWantRenderNotification = true;
                                        if (GLDefaultOptions.DRAW_TWICE_AFTER_SIZE_CHANGED) {
                                            // We keep mRequestRender true so that we draw twice after the size changes.
                                            // (Once because of mSizeChanged, the second time because of mRequestRender.)
                                            // This forces the updated graphics onto the screen.
                                        }
                                        else {
                                            mRequestRender = false;
                                        }
                                        mSizeChanged = false;
                                    }
                                    else {
                                        mRequestRender = false;
                                    }
                                    sGLThreadManager.notifyAll();
                                    break;
                                }
                            }

                            // By design, this is the only place in a GLThread thread where we wait().
                            if (GLDefaultOptions.LOG_THREADS) {
                                Log.i("waiting tid=" + getId() + " (shouldExit: " + mShouldExit + ")");
                            }
                            sGLThreadManager.wait();
                        }
                        if (mShouldExit) {
                            break;
                        }
                    } // end of synchronized(sGLThreadManager)

                    if (mHasFocus) {
                        if (createEglSurface) {
                            gl = (GL10)mEglHelper.createSurface(mRenderer.getHolder());
                            sGLThreadManager.checkGLDriver(gl);
                            if (GLDefaultOptions.LOG_RENDERER) {
                                Log.w("onSurfaceCreated");
                            }
                            mRenderer.onSurfaceCreated(gl, mEglHelper.mEglConfig);
                            createEglSurface = false;
                            framesSinceResetHack = 0;
                        }

                        if (sizeChanged) {
                            if (GLDefaultOptions.LOG_RENDERER) {
                                Log.w("onSurfaceChanged(" + w + ", " + h + ")");
                            }
                            mRenderer.onSurfaceChanged(gl, w, h);
                            sizeChanged = false;
                        }

                        synchronized (sGLThreadManager) {
                            ++mNumFrames;
                            if (GLDefaultOptions.LOG_RENDERER) {
                                Log.w("onDrawFrame: " + mNumFrames);
                            }
                        }

                        // Some phones (Motorola Cliq, Backflip; also the
                        // Huawei Pulse, and maybe the Samsung Behold II), use a
                        // broken graphics driver from Qualcomm.  It fails in a
                        // very specific case: when the EGL context is lost due to
                        // resource constraints, and then recreated, if GL commands
                        // are sent within two frames of the surface being created
                        // then eglSwapBuffers() will hang.  Normally, applications using
                        // the stock GLSurfaceView never run into this problem because it
                        // discards the EGL context explicitly on every pause.  But
                        // I've modified this class to not do that--I only want to reload
                        // textures when the context is actually lost--so this bug
                        // revealed itself as black screens on devices like the Cliq.
                        // Thus, in "safe mode," I force two swaps to occur before
                        // issuing any GL commands.  Don't ask me how long it took
                        // to figure this out.
                        if ((framesSinceResetHack > 1) || !mSafeMode) {
                            if (mMinFrameTime > 0) {
                                final long endTime = System.currentTimeMillis();
                                // Check if dt > 0, it can be negative on some android builds, if the user changes the date "in the past"
                                // endTime will be smaller ("in the past") than start time and endTime-mStartTime will result in 
                                // a negative dt which causes sleep() to be much longer than it should...
                                final long dt = endTime - mStartTime;
                                if (dt > 0 && dt < mMinFrameTime) {
                                    Thread.sleep(mMinFrameTime - dt);
                                }
                                mStartTime = System.currentTimeMillis();
                            }
                            while (pollAndRunJob(gl)) {
                            }
                            mRenderer.onDrawFrame(gl);
                        }
                        else {
                            Log.w("Safe Mode Wait...");
                        }

                        framesSinceResetHack++;

                        synchronized (sGLThreadManager) {
                            if (mCanSwapBuffers) {
                                if (!mEglHelper.swap()) {
                                    if (GLDefaultOptions.LOG_SURFACE) {
                                        Log.i("(!mEglHelper.swap()) egl surface lost tid=" + getId());
                                    }
                                    stopEglLocked();
                                }
                            }
                            if (mFrameToRender != 0) {
                                if (mFrameToRender > 0) {
                                    // decrement if mFrameToRender is positive...
                                    --mFrameToRender;
                                }
                                doRequestRender();
                            }
                        }
                    } // if (mHasFocus)

                    synchronized (sGLThreadManager) {
                        if (mWantRenderNotification) {
                            doRenderNotification = true;
                        }
                    }
                }

                mRenderer.onThreadExit();
            }
            finally {
                /*
                 * clean-up everything...
                 */
                synchronized (sGLThreadManager) {
                    stopEglLocked();
                    mEglHelper.finish();
                }
            }
        }

        public void surfaceCreated() {
            synchronized (sGLThreadManager) {
                if (GLDefaultOptions.LOG_THREADS) {
                    Log.i("surfaceCreated tid=" + getId());
                }
                mHasSurface = true;
                sGLThreadManager.notifyAll();
            }
        }

        public void surfaceDestroyed() {
            synchronized (sGLThreadManager) {
                if (GLDefaultOptions.LOG_THREADS) {
                    Log.i("surfaceDestroyed tid=" + getId());
                }
                mHasSurface = false;
                sGLThreadManager.notifyAll();
                while ((!mWaitingForSurface) && (!mExited)) {
                    try {
                        sGLThreadManager.wait();
                    }
                    catch (final InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public void onPause() {
            synchronized (sGLThreadManager) {
                mPaused = true;
                sGLThreadManager.notifyAll();
            }
        }

        public void onResume() {
            synchronized (sGLThreadManager) {
                mPaused = false;
                mRequestRender = true;
                sGLThreadManager.notifyAll();
            }
        }

        public void onWindowResize(final int w, final int h) {
            synchronized (sGLThreadManager) {
                mWidth = w;
                mHeight = h;
                mSizeChanged = true;
            }
            waitForRenderComplete();
        }

        public int waitForRenderComplete() {
            synchronized (sGLThreadManager) {
                if (mPaused || mExited) {
                    Log.e("waitForRenderComplete - called while paused or exited !");
                    return -1;
                }

                mRequestRender = true;
                mWantRenderNotification = true;
                mRenderCompleteFrame = -1;

                Log.d("waitForRenderComplete - Enter: " + mShouldExit + ", " + mExited);
                sGLThreadManager.notifyAll();
                // Time that a cycle will wait...
                final int cycleWaitTime = 500;
                // maximum waiting time
                int maxWaitingTime = 3000;
                // Wait for thread to react to resize and render a frame
                while (!mExited && !mPaused) {
                    if (mRenderCompleteFrame >= 0) {
                        Log.d("waitForRenderComplete - LeaveAt: " + mRenderCompleteFrame);
                        return mRenderCompleteFrame;
                    }
                    if (GLDefaultOptions.LOG_SURFACE) {
                        Log.i("Waiting for render complete : " + mShouldExit);
                    }
                    try {
                        sGLThreadManager.wait(cycleWaitTime);
                    }
                    catch (final InterruptedException ex) {
                        Log.e("Interrupted !");
                        Thread.currentThread().interrupt();
                    }
                    maxWaitingTime -= cycleWaitTime;
                    if (maxWaitingTime <= 0) {
                        Log.d("waitForRenderComplete - Timeout: " + mShouldExit + ", " + mExited);
                        return -1;
                    }
                }

                Log.d("waitForRenderComplete - Leave: " + mShouldExit + ", " + mExited);
                return -1;
            }
        }

        public int waitForNFrames(final int aNumFramesToWaitFor) {
            int completeFrame = -1;
            synchronized (sGLThreadManager) {
                mCanSwapBuffers = false;
                for (int i = 0; i < aNumFramesToWaitFor; ++i) {
                    completeFrame = waitForRenderComplete();
                    if (completeFrame == -1)
                        break;
                }
                mCanSwapBuffers = true;
            }
            return completeFrame;
        }

        //
        // On some Qualcomm devices (such as the HTC Magic running Android 1.6),
        // there's a bug in the graphics driver that will cause glViewport() to
        // do the wrong thing in a very specific situation.  When the screen is
        // rotated, if a surface is created in one layout (say, portrait view)
        // and then rotated to another, subsequent calls to glViewport are clipped.
        // So, if the window is, say, 320x480 when the surface is created, and
        // then the rotation occurs and glViewport() is called with the new
        // size of 480x320, devices with the buggy driver will clip the viewport
        // to the old width (which means 320x320...ugh!).  This is fixed in
        // Android 2.1 Qualcomm devices (like Nexus One) and doesn't affect
        // non-Qualcomm devices (like the Motorola DROID).
        //
        // Unfortunately, under Android 1.6 this exact case occurs when the
        // screen is put to sleep and then wakes up again.  The lock screen
        // comes up in portrait mode, but at the same time the window surface
        // is also created in the backgrounded game.  When the lock screen is closed
        // and the game comes forward, the window is fixed to the correct size
        // which causes the bug to occur.
        //
        // The solution used here is to simply never render when the window surface
        // does not have the focus.  When the lock screen (or menu) is up, rendering
        // will stop.  This resolves the driver bug (as the egl surface won't be created
        // until after the screen size has been fixed), and is generally good practice
        // since you don't want to be doing a lot of CPU intensive work when the lock
        // screen is up (to preserve battery life).
        public void onWindowFocusChanged(final boolean hasFocus) {
            synchronized (sGLThreadManager) {
                Log.d("onWindowFocusChanged - Enter: " + mShouldExit + ", " + mExited);
                mHasFocus = hasFocus;
                sGLThreadManager.notifyAll();
                if (mHasFocus) {
                    render();
                }
                if (GLDefaultOptions.LOG_SURFACE) {
                    Log.i("Focus " + (mHasFocus ? "gained" : "lost"));
                }
                Log.d("onWindowFocusChanged - Leave: " + mShouldExit + ", " + mExited);
            }
        }

        public void requestExitAndWait() {
            // don't call this from GLThread thread or it is a guaranteed
            // deadlock!
            synchronized (sGLThreadManager) {
                Log.d("requestExitAndWait - Enter: " + mShouldExit + ", " + mExited);
                mShouldExit = true;
                sGLThreadManager.notifyAll();
                while (!mExited) {
                    try {
                        sGLThreadManager.wait();
                    }
                    catch (final InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                }
                Log.d("requestExitAndWait - Leave: " + mShouldExit + ", " + mExited);
            }
        }

        public void setSafeMode(final boolean on) {
            mSafeMode = on;
        }

        private final EglHelper mEglHelper;

        //========================================================================
        //
        //  Variables protected by sGLThreadManager
        //
        //========================================================================
        private boolean mSizeChanged = true;
        // Once the thread is started, all accesses to the following member
        // variables are protected by the sGLThreadManager monitor
        private boolean mShouldExit;
        private boolean mExited;
        private boolean mPaused;
        private boolean mHasSurface;
        private boolean mWaitingForSurface;
        private boolean mHaveEglContext;
        private boolean mHaveEglSurface;
        private int mWidth;
        private int mHeight;
        private boolean mRequestRender;
        // -1 if waiting for render request, else the frame number the request was completed on
        private int mRenderCompleteFrame;
        private boolean mHasFocus;
        private boolean mSafeMode = false;
        private boolean mCanSwapBuffers = true;
        private boolean mWantRenderNotification = true;
        private int mNumFrames = 0;

        //========================================================================
        //
        //  Renderer
        //
        //========================================================================
        private Renderer mRenderer;

        //========================================================================
        //
        //  Jobs
        //
        //========================================================================
        private final Queue<GLJob> mJobsQueue = new LinkedList<GLJob>();

        public void pushJob(final GLJob job) {
            synchronized (sGLThreadManager) {
                mJobsQueue.offer(job);
            }
        }

        private boolean pollAndRunJob(final GL10 gl) {
            GLJob job;
            synchronized (sGLThreadManager) {
                job = mJobsQueue.poll();
                if (job == null)
                    return false;
            }
            job.Run(gl);
            return true;
        }

        //========================================================================
        //
        //  MinFrametime
        //
        //========================================================================
        private int mMinFrameTime = 1000 / 60;
        private long mStartTime = System.currentTimeMillis();

        //========================================================================
        //
        //  AutoPause rendering
        //
        //========================================================================
        private int mFrameToRender = -1;
        private int mMaxFrameToRender = 240;
        private int mNumFrameToAddOnRender = 60;

        private void doRequestRender() {
            mRequestRender = true;
            sGLThreadManager.notifyAll();
        }

        public void render() {
            synchronized (sGLThreadManager) {
                if (mFrameToRender >= 0) {
                    mFrameToRender += mNumFrameToAddOnRender;
                    if (mFrameToRender > mMaxFrameToRender)
                        mFrameToRender = mMaxFrameToRender;
                }
                doRequestRender();
            }
        }
    }

    //========================================================================
    //
    //  GLThreadManager
    //
    //========================================================================
    private static class GLThreadManager {

        public synchronized void threadExiting(final GLThread thread) {
            if (GLDefaultOptions.LOG_THREADS) {
                Log.i("exiting tid=" + thread.getId());
            }
            thread.mExited = true;
            if (mEglOwner == thread) {
                mEglOwner = null;
            }
            notifyAll();
        }

        /*
         * Tries once to acquire the right to use an EGL
         * surface. Does not block. Requires that we are already
         * in the sGLThreadManager monitor when this is called.
         *
         * @return true if the right to use an EGL surface was acquired.
         */
        public boolean tryAcquireEglSurfaceLocked(final GLThread thread) {
            if ((mEglOwner == thread) || (mEglOwner == null)) {
                mEglOwner = thread;
                notifyAll();
                return true;
            }
            checkGLESVersion();
            if (mMultipleGLESContextsAllowed) {
                return true;
            }
            return false;
        }

        /*
         * Releases the EGL surface. Requires that we are already in the
         * sGLThreadManager monitor when this is called.
         */
        public void releaseEglSurfaceLocked(final GLThread thread) {
            if (mEglOwner == thread) {
                mEglOwner = null;
            }
            notifyAll();
        }

        public synchronized void checkGLDriver(final GL10 gl) {
            if (!mGLESDriverCheckComplete) {
                checkGLESVersion();
                if (mGLESVersion < kGLES_20) {
                    mMultipleGLESContextsAllowed = false;
                    notifyAll();
                }
                mGLESDriverCheckComplete = true;
            }
        }

        private void checkGLESVersion() {
            if (!mGLESVersionCheckComplete) {
                mGLESVersion = ConfigurationInfo.GL_ES_VERSION_UNDEFINED;
                if (mGLESVersion >= kGLES_20) {
                    mMultipleGLESContextsAllowed = true;
                }
                mGLESVersionCheckComplete = true;
            }

        }

        private boolean mGLESVersionCheckComplete;
        private int mGLESVersion;
        private boolean mGLESDriverCheckComplete;
        private boolean mMultipleGLESContextsAllowed;
        private static final int kGLES_20 = 0x20000;
        private GLThread mEglOwner;
    }

    public static final GLThreadManager sGLThreadManager = new GLThreadManager();
}
