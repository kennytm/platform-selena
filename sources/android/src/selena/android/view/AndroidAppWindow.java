package selena.android.view;

import static selena.common.math.Rect.Rectl;
import static selena.common.math.Vec.Vec4f;

import java.io.FileNotFoundException;

import javax.microedition.khronos.opengles.GL10;

import ni.aglGraphics.EGraphicsContextType;
import ni.aglGraphics.ETextureFlags;
import ni.aglSystem.ECreateInstanceFlags;
import ni.aglSystem.EKey;
import ni.aglSystem.EMotionEvent;
import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.IMessageTargets;
import ni.aglSystem.IOSWindow;
import ni.aglSystem.IOSWindowGeneric;
import ni.aglSystem.Lib;
import ni.types.Vector4f;
import ni.types.Vector4l;
import selena.SelenaException;
import selena.android.facade.AndroidFacade;
import selena.android.util.Keys;
import selena.app.AppConfig;
import selena.app.AppEventHandler;
import selena.common.facade.FacadeException;
import selena.common.facade.FacadeManager;
import selena.common.facade.facade.SystemFacade;
import selena.common.util.Log;
import android.app.Activity;
import android.content.Context;
import android.view.MotionEvent;

public class AndroidAppWindow extends selena.app.BaseAppWindow {
    private final Vector4f _motionParam = Vec4f();
    protected final Vector4l _wndRect;
    protected final Context _context;
    protected IMessageTargets _wndMT;
    protected IOSWindowGeneric _wndGeneric;

    public AndroidAppWindow(final Context activity, final AppEventHandler eventHandler, final int initialWidth, final int initialHeight) throws FileNotFoundException, SelenaException {
        super(eventHandler);
        _wndRect = Rectl(0, 0, initialWidth, initialHeight);
        _context = activity;
        construct();
    }

    public Context getContext() {
        return _context;
    }

    @Override
    protected AppConfig createAppConfig() throws SelenaException {
        final AppConfig cfg = new AppConfig();
        if (_context instanceof Activity) {
            cfg.windowTitle = ((Activity)_context).getTitle().toString();
        }
        return cfg;
    }

    @Override
    protected void initWindowAndGraphicsContext() throws SelenaException
    {
        // Create the generic window object
        _wnd = IOSWindow.query(sys.createInstance("aglSystem.OSWindowGeneric",
                null, null, ECreateInstanceFlags.Default, 0, 0, 0));
        if (_wnd == null)
            throw new SelenaException("Can't create the application's window !");
        _wnd.setTitle(_config.windowTitle);
        _wndMT = _wnd.getMessageTargets();
        _wndGeneric = IOSWindowGeneric.query(_wnd);
        _wnd.setRect(_wndRect);

        //// Create the graphics context ////
        if (!_graphics.initializeDriver(Lib.hstr(_config.renDriver)))
            throw new SelenaException("Can't initialize graphics driver !");

        _graphicsContext = _graphics.createContext(EGraphicsContextType.Driver,
                _wnd, null, null, 0, 0, _config.renBBFmt, _config.renDSFmt,
                false, _config.renSwapInterval,
                // ETextureFlags.Virtual asks the driver to not create the context by itself and use the one we already have...
                _config.renBBFlags | ETextureFlags.Virtual);
        if (_graphicsContext == null)
            throw new SelenaException("Can't create graphics context !");
    }

    @Override
    protected FacadeManager createFacadeManager() throws FacadeException {
        final FacadeManager fm = new FacadeManager(_context);
        fm.registerReceiver(SystemFacade.class);
        fm.registerReceiver(AndroidFacade.class);
        return fm;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void dispose() {
        super.dispose();
    }

    public void draw(final GL10 gl) {
        if (!update()) {
            if (_context instanceof Activity) {
                ((Activity)_context).finish();
            }
            return;
        }
        draw();
    }

    public void resize(final int width, final int height) {
        _wndRect.setWidth(width);
        _wndRect.setHeight(height);
        _wnd.setRect(_wndRect);
        _graphicsContext.changeWindow(null);
        _graphicsContext.changeWindow(_wnd);
        postEventMessage(EOSWindowMessage.Size);
    }

    public boolean inputKey(final int keyCode, final boolean isDown) {
        Log.d("[KBD] inputKey : " + keyCode + ", isDown: " + isDown);
        if (keyCode < Keys.mVKMap.length) {
            final int keyToSend = Keys.mVKMap[keyCode];
            if (keyToSend != EKey.Unknown) {
                _wndGeneric.genericKeyEvent(keyToSend, isDown);
                return true;
            }
        }
        return false;
    }

    public void inputString(final String str) {
        _wndGeneric.genericInputString(str);
    }

    // this shit isn't defined properly and is different between Android 2.1 & 2.2... Android API Happiness++
    public static final int ACTION_POINTER_INDEX_MASK = 0xff00;
    // this shit isn't defined properly and is different between Android 2.1 & 2.2... Android API Happiness++
    public static final int ACTION_POINTER_INDEX_SHIFT = 8;

    public void inputTouch(final MotionEvent aMotionEvent) {
        final int numEvents = aMotionEvent.getPointerCount();
        final int action = aMotionEvent.getAction();
        switch (action & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN: {
            for (int i = 0; i < numEvents; ++i) {
                final int pid = aMotionEvent.getPointerId(i);
                //Log.i("# FINGER [" + pid + "] Down (of " + numEvents + ")");
                _motionParam.x = aMotionEvent.getX(i);
                _motionParam.y = aMotionEvent.getY(i);
                _motionParam.z = aMotionEvent.getPressure(i);
                _wndGeneric.genericMotionEvent(pid, EMotionEvent.Down, _motionParam);
            }
            break;
        }
        case MotionEvent.ACTION_CANCEL:
        case MotionEvent.ACTION_UP: {
            for (int i = 0; i < numEvents; ++i) {
                final int pid = aMotionEvent.getPointerId(i);
                //Log.i("# FINGER [" + pid + "] Up (of " + numEvents + ")");
                _motionParam.x = aMotionEvent.getX(i);
                _motionParam.y = aMotionEvent.getY(i);
                _motionParam.z = aMotionEvent.getPressure(i);
                _wndGeneric.genericMotionEvent(pid, EMotionEvent.Up, _motionParam);
            }
            break;
        }
        case MotionEvent.ACTION_MOVE: {
            for (int i = 0; i < numEvents; ++i) {
                final int pid = aMotionEvent.getPointerId(i);
                _motionParam.x = aMotionEvent.getX(i);
                _motionParam.y = aMotionEvent.getY(i);
                _motionParam.z = aMotionEvent.getPressure(i);
                _wndGeneric.genericMotionEvent(pid, EMotionEvent.Drag, _motionParam);
            }
            break;
        }
        case MotionEvent.ACTION_POINTER_DOWN: {
            final int pointerIndex = (action & ACTION_POINTER_INDEX_MASK) >> ACTION_POINTER_INDEX_SHIFT;
            final int pid = aMotionEvent.getPointerId(pointerIndex);
            //Log.i("# FINGER [" + pid + "] Down");
            _motionParam.x = aMotionEvent.getX(pointerIndex);
            _motionParam.y = aMotionEvent.getY(pointerIndex);
            _motionParam.z = aMotionEvent.getPressure(pointerIndex);
            _wndGeneric.genericMotionEvent(pid, EMotionEvent.Down, _motionParam);
            break;
        }
        case MotionEvent.ACTION_POINTER_UP: {
            final int pointerIndex = (action & ACTION_POINTER_INDEX_MASK) >> ACTION_POINTER_INDEX_SHIFT;
            final int pid = aMotionEvent.getPointerId(pointerIndex);
            //Log.i("# FINGER [" + pid + "] Up");
            _motionParam.x = aMotionEvent.getX(pointerIndex);
            _motionParam.y = aMotionEvent.getY(pointerIndex);
            _motionParam.z = aMotionEvent.getPressure(pointerIndex);
            _wndGeneric.genericMotionEvent(pid, EMotionEvent.Up, _motionParam);
            break;
        }
        }
    }

    public void inputAccelerometer(final float[] values) {
        // swap x/y because we assume 'landscape' by default
        // scale factor is to give us a value which is similar to the PC test devices
        _motionParam.x = values[1] * 0.2f;
        _motionParam.y = -values[0] * 0.2f;
        _motionParam.z = values[2] * 0.2f;
        _wndGeneric.genericMotionEvent(0, EMotionEvent.Accelerometer, _motionParam);
    }

    public void postEventMessage(final int msg) {
        _wndMT.postMessage(msg, null, null);
    }

    public void postEventMessage(final int msg, final Object a) {
        _wndMT.postMessage(msg, a, null);
    }
}
