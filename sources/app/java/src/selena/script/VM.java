package selena.script;

import ni.aglSystem.IFile;
import ni.aglSystem.IScriptObject;
import ni.aglSystem.IScriptVM;
import ni.aglSystem.IScriptingHost;
import ni.aglSystem.Lib;
import selena.SelenaException;

public class VM {
    protected IScriptVM _vm;

    public VM() throws SelenaException {
        _vm = IScriptVM.query(Lib.createInstance("aglSystem.ScriptVM"));
        if (_vm == null)
            throw new SelenaException("Can't create the ScriptVM !");
    }

    public void dispose() {
        if (_vm != null) {
            _vm.invalidate();
            _vm.dispose();
            _vm = null;
        }
    }

    public IScriptingHost getScriptingHost() {
        return _vm;
    }

    public IScriptObject compile(final String code, final String name) throws ScriptCompilationException
    {
        final IScriptObject sobj = _vm.compileString(code, name);
        if (sobj == null)
            throw new ScriptCompilationException();
        return sobj;
    }

    public IScriptObject compile(final String code) throws ScriptCompilationException {
        return compile(code, null);
    }

    public IScriptObject compile(final IFile fp, final String name) throws ScriptCompilationException
    {
        final IScriptObject sobj = _vm.compile(fp, name);
        if (sobj == null)
            throw new ScriptCompilationException();
        return sobj;
    }

    public IScriptObject compile(final IFile fp) throws ScriptCompilationException {
        return compile(fp, null);
    }

    public void run(final IScriptObject thisTable, final IScriptObject closure) throws ScriptCallException
    {
        _vm.pushObject(closure);
        try {
            if (thisTable == null) {
                _vm.pushRootTable();
            }
            else {
                _vm.pushObject(thisTable);
            }
            if (!_vm.call(1, false))
                throw new ScriptCallException();
        }
        finally {
            // pop the closure
            _vm.pop(1);
        }
    }

    public void run(final IScriptObject thisTable, final String code) throws ScriptException
    {
        final IScriptObject closure = compile(code);
        try {
            run(thisTable, closure);
        }
        finally {
            closure.dispose();
        }
    }

    public void run(final IScriptObject thisTable, final IFile fp) throws ScriptException
    {
        final IScriptObject closure = compile(fp);
        try {
            run(thisTable, closure);
        }
        finally {
            closure.dispose();
        }
    }
}
