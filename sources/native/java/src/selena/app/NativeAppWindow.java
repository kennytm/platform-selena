package selena.app;

import java.io.IOException;

import ni.aglGraphics.EGraphicsContextType;
import ni.aglSystem.EOSWindowStyleFlags;
import ni.aglSystem.Lib;
import ni.types.Vector4l;
import selena.SelenaException;

class NativeAppWindow extends BaseAppWindow {
    NativeAppWindow(final AppEventHandler eventHandler) throws SelenaException, IOException {
        super(eventHandler);
        construct();
    }

    @Override
    protected AppConfig createAppConfig() {
        return new AppConfig();
    }

    @Override
    protected void initWindowAndGraphicsContext() throws SelenaException {
        //// Create the window ////
        Vector4l r = _config.windowRect;
        if (r == null) {
            r = sys.getMonitorRect(0);
            r.setWidth((int)(r.getWidth() * 0.75));
            r.setHeight((int)(r.getHeight() * 0.75));
            if ((r.getWidth() < 10) || (r.getHeight() < 10)) {
                r.set(0, 0, 1024, 768);
            }
        }

        _wnd = sys.createWindow(
                null,
                _config.windowTitle,
                r,
                false,
                EOSWindowStyleFlags.Regular);
        if (_wnd == null)
            throw new SelenaException("Can't create the application's window !");

        if (_config.windowCenter) {
            _wnd.centerWindow();
        }
        if (_config.windowFullScreen >= 0) {
            _wnd.setFullScreen(_config.windowFullScreen);
        }

        //// Create the graphics context ////
        if (!_graphics.initializeDriver(Lib.hstr(_config.renDriver)))
            throw new SelenaException("Can't initialize graphics driver !");

        _graphicsContext = _graphics.createContext(
                EGraphicsContextType.Driver,
                _wnd,
                null,
                null,
                0,
                0,
                _config.renBBFmt,
                _config.renDSFmt,
                false,
                _config.renSwapInterval,
                _config.renBBFlags);
        if (_graphicsContext == null)
            throw new SelenaException("Can't create graphics context !");
    }
}
