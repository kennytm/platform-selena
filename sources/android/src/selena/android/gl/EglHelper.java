package selena.android.gl;

import java.io.Writer;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGL11;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;
import android.view.SurfaceHolder;
import android.opengl.GLDebugHelper;

import selena.common.util.Log;

/**
 * EGL helper class.
 */
public class EglHelper {
    /**
     * Check glError() after every GL call and throw an exception if glError
     * indicates that an error has occurred. This can be used to help track down
     * which OpenGL ES call is causing an error.
     *
     * @see #getDebugFlags
     * @see #setDebugFlags
     */
    public final static int DEBUG_CHECK_GL_ERROR = 1;

    /**
     * Log GL calls to the system log at "verbose" level with tag
     * "GLSurfaceView".
     *
     * @see #getDebugFlags
     * @see #setDebugFlags
     */
    public final static int DEBUG_LOG_GL_CALLS = 2;

    private static final int EGL_OPENGL_ES_BIT = 0x0001;
    private static final int EGL_OPENGL_ES2_BIT = 0x0004;
    // private static final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;

    private GLConfig mConfig;
    public GLConfig getConfig() {
    	if (mConfig == null) {
    		mConfig = new GLConfig();
    	}
    	return mConfig;
    }


    public EglHelper() {
    }

    /**
     * Initialize EGL for a given configuration spec.
     *
     * @param configSpec
     */
    public void start() {
    	GLConfig config = getConfig();

        final int glesVersion = config.glesVersion;

        if (mEGLConfigChooser == null) {
            mEGLConfigChooser = new SimpleEGLConfigChooser(true);
        }
        if (mEGLContextFactory == null) {
            mEGLContextFactory = new DefaultContextFactory();
        }
        if (mEGLWindowSurfaceFactory == null) {
            mEGLWindowSurfaceFactory = new DefaultWindowSurfaceFactory();
        }

        /*
         * Get an EGL instance
         */
        mEgl = (EGL10)EGLContext.getEGL();

        /*
         * Get to the default display.
         */
        mEglDisplay = mEgl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        if (mEglDisplay == EGL10.EGL_NO_DISPLAY) {
            throw new RuntimeException("eglGetDisplay failed");
        }

        /*
         * We can now initialize EGL for that display
         */
        final int[] version = new int[2];
        if (!mEgl.eglInitialize(mEglDisplay, version)) {
            throw new RuntimeException("eglInitialize failed");
        }

        /*
         * Get EGL config
         */
        Log.v(">>> Looking for EGL Config ES" + glesVersion + " R" + config.redSize + "G" + config.greenSize + "B" + config.blueSize + "A" + config.alphaSize
              + " D" + config.depthSize + "S" + config.stencilSize
              + " AA" + config.aaSamples);

        boolean foundConfig = false;
        if (!GLDefaultOptions.USE_DEFAULT_GL_CONTEXT) {
            foundConfig = findAndInitConfig(config,glesVersion);
            if (!foundConfig) {
                if (config.aaSamples > 0) {
                    Log.v("Can't find requested config, try without AA");
                    config.aaSamples = 0;
                }
                foundConfig = findAndInitConfig(config,glesVersion);
            }
        }
        if (!foundConfig) {
            Log.v("Can't find config, get first available config");
            final int[] configSpec = new int[] {
                    EGL10.EGL_RENDERABLE_TYPE, (glesVersion == 2) ? EGL_OPENGL_ES2_BIT : EGL_OPENGL_ES_BIT,
                    EGL10.EGL_DEPTH_SIZE, 16,
                    EGL10.EGL_NONE
            };
            final EGLConfig[] configs = new EGLConfig[1];
            final int[] num_config = new int[1];
            mEgl.eglChooseConfig(mEglDisplay, configSpec, configs, 1, num_config);
            mEglConfig = configs[0];
        }

        /*
         * Create an OpenGL ES context. This must be done only once, an
         * OpenGL context is a somewhat heavy object.
         */
        mEglContext = mEGLContextFactory.createContext(mEgl, mEglDisplay, mEglConfig);
        if ((mEglContext == null) || (mEglContext == EGL10.EGL_NO_CONTEXT)) {
            throwEglException("createContext");
        }

        mEglSurface = null;
    }

    private boolean findAndInitConfig(GLConfig glConfig, final int glesVersion) {
        /*
         * Get EGL config
         */
        {
            mEglConfig = null;

            // if (glConfig.configAttrs == null)
            // glConfig.configAttrs = new int[] {EGL10.EGL_NONE};
            // int[] oldConf = glConfig.configAttrs;

            int i = 0;
            final int[] configAttrs = new int[17/* + oldConf.length-1*/];
            // for (i = 0; i < oldConf.length-1; i++)
            // glConfig.configAttrs[i] = oldConf[i];
            configAttrs[i++] = EGL10.EGL_RENDERABLE_TYPE;
            configAttrs[i++] = (glesVersion == 2) ? EGL_OPENGL_ES2_BIT : EGL_OPENGL_ES_BIT;
            configAttrs[i++] = EGL10.EGL_SAMPLES;
            configAttrs[i++] = glConfig.aaSamples;
            configAttrs[i++] = EGL10.EGL_RED_SIZE;
            configAttrs[i++] = glConfig.redSize;
            configAttrs[i++] = EGL10.EGL_GREEN_SIZE;
            configAttrs[i++] = glConfig.greenSize;
            configAttrs[i++] = EGL10.EGL_BLUE_SIZE;
            configAttrs[i++] = glConfig.blueSize;
            configAttrs[i++] = EGL10.EGL_ALPHA_SIZE;
            configAttrs[i++] = glConfig.alphaSize;
            configAttrs[i++] = EGL10.EGL_STENCIL_SIZE;
            configAttrs[i++] = glConfig.stencilSize;
            configAttrs[i++] = EGL10.EGL_DEPTH_SIZE;
            configAttrs[i++] = glConfig.depthSize;
            configAttrs[i++] = EGL10.EGL_NONE;

            final EGLConfig[] config = new EGLConfig[20];
            final int num_configs[] = new int[1];
            mEgl.eglChooseConfig(mEglDisplay, configAttrs, config, config.length, num_configs);
            Log.v("eglChooseConfig err: " + mEgl.eglGetError());

            // to make sure even worst score is better than this, like 8888 when request 565...
            int score = 1 << 24;

            final int val[] = new int[1];
            for (i = 0; i < num_configs[0]; ++i) {
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_RENDERABLE_TYPE, val);
                int es = val[0];
                switch (es) {
                case EGL_OPENGL_ES_BIT:
                    es = 1;
                    break;
                case EGL_OPENGL_ES2_BIT:
                    es = 2;
                    break;
                default:
                    es = 0;
                    break;
                }
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_SAMPLES, val);
                final int aa = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_RED_SIZE, val);
                final int r = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_GREEN_SIZE, val);
                final int g = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_BLUE_SIZE, val);
                final int b = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_ALPHA_SIZE, val);
                final int a = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_DEPTH_SIZE, val);
                final int d = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_STENCIL_SIZE, val);
                final int s = val[0];
                mEgl.eglGetConfigAttrib(mEglDisplay, config[i], EGL10.EGL_CONFIG_CAVEAT, val);
                final int caveat = val[0];

                if ((caveat == EGL10.EGL_SLOW_CONFIG) ||
                    (caveat == EGL10.EGL_NON_CONFORMANT_CONFIG))
                {
                    //
                    // if there's any "caveat" we skip the
                    // config... otherwise we don't know what
                    // context we'll end up with... Software or Buggy
                    // Rendering is no fun...
                    //
                    // If no config without caveat is found findConfig will return
                    // false and we'll eventually just use the default config...
                    //
                    continue;
                }

                int currScore = 0;
                currScore += Math.abs(aa - glConfig.aaSamples) << 24;
                currScore += (Math.abs(r - glConfig.redSize) + Math.abs(g - glConfig.greenSize) +
                              Math.abs(b - glConfig.blueSize) + Math.abs(a - glConfig.alphaSize))
                        << 16;
                currScore += Math.abs(d - glConfig.depthSize) << 8;
                currScore += Math.abs(s - glConfig.stencilSize);

                Log.v(">>> EGL Config [" + i + "] ES" + es + " R" + r + "G" + g + "B" + b + "A" + a
                      + " D" + d + "S" + s
                      + " AA" + aa + " Caveat[" + caveat + "] - Score: " + currScore);

                if ((mEglConfig == null) || (currScore < score)) {
                    String o = "";
                    o += "--> Config chosen: " + i + "\n";
                    /*for (int j = 0; j < ((configAttrs.length - 1) >> 1); j++) {
                        mEgl.eglGetConfigAttrib(mEglDisplay, config[i], configAttrs[j * 2], val);
                        if (val[0] >= configAttrs[(j * 2) + 1]) {
                            o += "setting " + j + ", matches: " + val[0] + "\n";
                        }
                    }*/
                    Log.v(o);

                    score = currScore;
                    mEglConfig = config[i];
                }
            }
        }

        return mEglConfig != null;
    }

    /*
     * React to the creation of a new surface by creating and returning an
     * OpenGL interface that renders to that surface.
     */
    public GL createSurface(final SurfaceHolder holder) {
        /*
         *  The window size has changed, so we need to create a new
         *  surface.
         */
        if ((mEglSurface != null) && (mEglSurface != EGL10.EGL_NO_SURFACE)) {

            /*
             * Unbind and destroy the old EGL surface, if
             * there is one.
             */
            mEgl.eglMakeCurrent(mEglDisplay, EGL10.EGL_NO_SURFACE,
                    EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            mEGLWindowSurfaceFactory.destroySurface(mEgl, mEglDisplay, mEglSurface);
        }

        /*
         * Create an EGL surface we can render into.
         */
        mEglSurface = mEGLWindowSurfaceFactory.createWindowSurface(
                mEgl, mEglDisplay, mEglConfig, holder);
        if ((mEglSurface == null) || (mEglSurface == EGL10.EGL_NO_SURFACE)) {
            throwEglException("createWindowSurface");
        }

        /*
         * Before we can issue GL commands, we need to make sure
         * the context is current and bound to a surface.
         */
        if (!mEgl.eglMakeCurrent(mEglDisplay, mEglSurface, mEglSurface, mEglContext)) {
            throwEglException("eglMakeCurrent");
        }

        GL gl = mEglContext.getGL();
        if (mGLWrapper != null) {
            gl = mGLWrapper.wrap(gl);
        }

        if ((mDebugFlags & (DEBUG_CHECK_GL_ERROR | DEBUG_LOG_GL_CALLS)) != 0) {
            int configFlags = 0;
            Writer log = null;
            if ((mDebugFlags & DEBUG_CHECK_GL_ERROR) != 0) {
                configFlags |= GLDebugHelper.CONFIG_CHECK_GL_ERROR;
            }
            if ((mDebugFlags & DEBUG_LOG_GL_CALLS) != 0) {
                log = new LogWriter();
            }
            gl = GLDebugHelper.wrap(gl, configFlags, log);
        }
        return gl;
    }

    /**
     * Display the current render surface.
     *
     * @return false if the context has been lost.
     */
    public boolean swap() {
        mEgl.eglSwapBuffers(mEglDisplay, mEglSurface);

        /*
         * Always check for EGL_CONTEXT_LOST, which means the context
         * and all associated data were lost (For instance because
         * the device went to sleep). We need to sleep until we
         * get a new surface.
         */
        return mEgl.eglGetError() != EGL11.EGL_CONTEXT_LOST;
    }

    public void destroySurface() {
        if ((mEglSurface != null) && (mEglSurface != EGL10.EGL_NO_SURFACE)) {
            mEgl.eglMakeCurrent(mEglDisplay, EGL10.EGL_NO_SURFACE,
                    EGL10.EGL_NO_SURFACE,
                    EGL10.EGL_NO_CONTEXT);
            mEGLWindowSurfaceFactory.destroySurface(mEgl, mEglDisplay, mEglSurface);
            mEglSurface = null;
        }
    }

    public void finish() {
        if (mEglContext != null) {
            mEGLContextFactory.destroyContext(mEgl, mEglDisplay, mEglContext);
            mEglContext = null;
        }
        if (mEglDisplay != null) {
            mEgl.eglTerminate(mEglDisplay);
            mEglDisplay = null;
        }
    }

    private void throwEglException(final String function) {
        throw new RuntimeException(function + " failed: " + mEgl.eglGetError());
    }

    /** Checks to see if the current context is valid. **/
    public boolean verifyContext() {
        final EGLContext currentContext = mEgl.eglGetCurrentContext();
        return (currentContext != EGL10.EGL_NO_CONTEXT) && (mEgl.eglGetError() != EGL11.EGL_CONTEXT_LOST);
    }

    EGL10 mEgl;
    EGLDisplay mEglDisplay;
    EGLSurface mEglSurface;
    EGLConfig mEglConfig;
    EGLContext mEglContext;

    /**
     * Set the glWrapper. If the glWrapper is not null, its
     * {@link GLWrapper#wrap(GL)} method is called whenever a surface is
     * created. A GLWrapper can be used to wrap the GL object that's passed to
     * the renderer. Wrapping a GL object enables examining and modifying the
     * behavior of the GL calls made by the renderer.
     * <p>
     * Wrapping is typically used for debugging purposes.
     * <p>
     * The default value is null.
     *
     * @param glWrapper
     *            the new GLWrapper
     */
    public void setGLWrapper(final GLWrapper glWrapper) {
        mGLWrapper = glWrapper;
    }

    /**
     * Set the debug flags to a new value. The value is constructed by
     * OR-together zero or more of the DEBUG_CHECK_* constants. The debug flags
     * take effect whenever a surface is created. The default value is zero.
     *
     * @param debugFlags
     *            the new debug flags
     * @see #DEBUG_CHECK_GL_ERROR
     * @see #DEBUG_LOG_GL_CALLS
     */
    public void setDebugFlags(final int debugFlags) {
        mDebugFlags = debugFlags;
    }

    /**
     * Get the current value of the debug flags.
     *
     * @return the current value of the debug flags.
     */
    public int getDebugFlags() {
        return mDebugFlags;
    }

    /**
     * Install a custom EGLContextFactory.
     * <p>
     * If this method is called, it must be called before
     * {@link #setRenderer(Renderer)} is called.
     * <p>
     * If this method is not called, then by default a context will be created
     * with no shared context and with a null attribute list.
     */
    public void setEGLContextFactory(final EGLContextFactory factory) {
        mEGLContextFactory = factory;
    }

    /**
     * Install a custom EGLWindowSurfaceFactory.
     * <p>
     * If this method is called, it must be called before
     * {@link #setRenderer(Renderer)} is called.
     * <p>
     * If this method is not called, then by default a window surface will be
     * created with a null attribute list.
     */
    public void setEGLWindowSurfaceFactory(final EGLWindowSurfaceFactory factory) {
        mEGLWindowSurfaceFactory = factory;
    }

    /**
     * Install a custom EGLConfigChooser.
     * <p>
     * If this method is called, it must be called before
     * {@link #setRenderer(Renderer)} is called.
     * <p>
     * If no setEGLConfigChooser method is called, then by default the view will
     * choose a config as close to 16-bit RGB as possible, with a depth buffer
     * as close to 16 bits as possible.
     *
     * @param configChooser
     */
    public void setEGLConfigChooser(final EGLConfigChooser configChooser) {
        mEGLConfigChooser = configChooser;
    }

    /**
     * Install a config chooser which will choose a config as close to 16-bit
     * RGB as possible, with or without an optional depth buffer as close to
     * 16-bits as possible.
     * <p>
     * If this method is called, it must be called before
     * {@link #setRenderer(Renderer)} is called.
     * <p>
     * If no setEGLConfigChooser method is called, then by default the view will
     * choose a config as close to 16-bit RGB as possible, with a depth buffer
     * as close to 16 bits as possible.
     *
     * @param needDepth
     */
    public void setEGLConfigChooser(final boolean needDepth) {
        setEGLConfigChooser(new SimpleEGLConfigChooser(needDepth));
    }

    /**
     * Install a config chooser which will choose a config with at least the
     * specified component sizes, and as close to the specified component sizes
     * as possible.
     * <p>
     * If this method is called, it must be called before
     * {@link #setRenderer(Renderer)} is called.
     * <p>
     * If no setEGLConfigChooser method is called, then by default the view will
     * choose a config as close to 16-bit RGB as possible, with a depth buffer
     * as close to 16 bits as possible.
     */
    public void setEGLConfigChooser(final int redSize, final int greenSize, final int blueSize,
            final int alphaSize, final int depthSize, final int stencilSize)
    {
        setEGLConfigChooser(new ComponentSizeChooser(redSize, greenSize,
                blueSize, alphaSize, depthSize, stencilSize));
    }

    /**
     * Inform the default EGLContextFactory and default EGLConfigChooser which
     * EGLContext client version to pick.
     * <p>
     * Use this method to create an OpenGL ES 2.0-compatible context. Example:
     *
     * <pre class="prettyprint">
     * public MyView(Context context) {
     *     super(context);
     *     setEGLContextClientVersion(2); // Pick an OpenGL ES 2.0 context.
     *     setRenderer(new MyRenderer());
     * }
     * </pre>
     * <p>
     * Note: Activities which require OpenGL ES 2.0 should indicate this by
     * setting @lt;uses-feature android:glEsVersion="0x00020000" /> in the
     * activity's AndroidManifest.xml file.
     * <p>
     * If this method is called, it must be called before
     * {@link #setRenderer(Renderer)} is called.
     * <p>
     * This method only affects the behavior of the default EGLContexFactory and
     * the default EGLConfigChooser. If
     * {@link #setEGLContextFactory(EGLContextFactory)} has been called, then
     * the supplied EGLContextFactory is responsible for creating an OpenGL ES
     * 2.0-compatible context. If {@link #setEGLConfigChooser(EGLConfigChooser)}
     * has been called, then the supplied EGLConfigChooser is responsible for
     * choosing an OpenGL ES 2.0-compatible config.
     *
     * @param version
     *            The EGLContext client version to choose. Use 2 for OpenGL ES
     *            2.0
     */
    public void setEGLContextClientVersion(final int version) {
        mEGLContextClientVersion = version;
    }

    // ----------------------------------------------------------------------
    /**
     * An interface for customizing the eglCreateContext and eglDestroyContext
     * calls.
     * <p>
     * This interface must be implemented by clients wishing to call
     * {@link GLSurfaceView#setEGLContextFactory(EGLContextFactory)}
     */
    public interface EGLContextFactory {
        EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig);

        void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context);
    }

    private class DefaultContextFactory implements EGLContextFactory {
        private final int EGL_CONTEXT_CLIENT_VERSION = 0x3098;

        @Override
        public EGLContext createContext(final EGL10 egl, final EGLDisplay display, final EGLConfig config)
        {
            final int[] attrib_list = { EGL_CONTEXT_CLIENT_VERSION, mEGLContextClientVersion,
                    EGL10.EGL_NONE };

            return egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT,
                    mEGLContextClientVersion != 0 ? attrib_list : null);
        }

        @Override
        public void destroyContext(final EGL10 egl, final EGLDisplay display,
                final EGLContext context)
        {
            egl.eglDestroyContext(display, context);
        }
    }

    /**
     * An interface for customizing the eglCreateWindowSurface and
     * eglDestroySurface calls.
     * <p>
     * This interface must be implemented by clients wishing to call
     * {@link GLSurfaceView#setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory)}
     */
    public interface EGLWindowSurfaceFactory {
        EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config,
                Object nativeWindow);

        void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface);
    }

    private static class DefaultWindowSurfaceFactory implements EGLWindowSurfaceFactory {

        @Override
        public EGLSurface createWindowSurface(final EGL10 egl, final EGLDisplay display,
                final EGLConfig config, final Object nativeWindow)
        {
            return egl.eglCreateWindowSurface(display, config, nativeWindow, null);
        }

        @Override
        public void destroySurface(final EGL10 egl, final EGLDisplay display,
                final EGLSurface surface)
        {
            egl.eglDestroySurface(display, surface);
        }
    }

    /**
     * An interface for choosing an EGLConfig configuration from a list of
     * potential configurations.
     * <p>
     * This interface must be implemented by clients wishing to call
     * {@link GLSurfaceView#setEGLConfigChooser(EGLConfigChooser)}
     */
    public interface EGLConfigChooser {
        /**
         * Choose a configuration from the list. Implementors typically
         * implement this method by calling {@link EGL10#eglChooseConfig} and
         * iterating through the results. Please consult the EGL specification
         * available from The Khronos Group to learn how to call
         * eglChooseConfig.
         *
         * @param egl
         *            the EGL10 for the current display.
         * @param display
         *            the current display.
         * @return the chosen configuration.
         */
        EGLConfig chooseConfig(EGL10 egl, EGLDisplay display);
    }

    private abstract class BaseConfigChooser
            implements EGLConfigChooser {
        public BaseConfigChooser(final int[] configSpec) {
            mConfigSpec = filterConfigSpec(configSpec);
        }

        @Override
        public EGLConfig chooseConfig(final EGL10 egl, final EGLDisplay display) {
            final int[] num_config = new int[1];
            if (!egl.eglChooseConfig(display, mConfigSpec, null, 0,
                    num_config)) {
                throw new IllegalArgumentException("eglChooseConfig failed");
            }

            final int numConfigs = num_config[0];

            if (numConfigs <= 0) {
                throw new IllegalArgumentException(
                        "No configs match configSpec");
            }

            final EGLConfig[] configs = new EGLConfig[numConfigs];
            if (!egl.eglChooseConfig(display, mConfigSpec, configs, numConfigs,
                    num_config)) {
                throw new IllegalArgumentException("eglChooseConfig#2 failed");
            }
            final EGLConfig config = chooseConfig(egl, display, configs);
            if (config == null) {
                throw new IllegalArgumentException("No config chosen");
            }
            return config;
        }

        abstract EGLConfig chooseConfig(EGL10 egl, EGLDisplay display,
                EGLConfig[] configs);

        protected int[] mConfigSpec;

        private int[] filterConfigSpec(final int[] configSpec) {
            if (mEGLContextClientVersion != 2) {
                return configSpec;
            }
            /* We know none of the subclasses define EGL_RENDERABLE_TYPE.
             * And we know the configSpec is well formed.
             */
            final int len = configSpec.length;
            final int[] newConfigSpec = new int[len + 2];
            System.arraycopy(configSpec, 0, newConfigSpec, 0, len - 1);
            newConfigSpec[len - 1] = EGL10.EGL_RENDERABLE_TYPE;
            newConfigSpec[len] = 4; /* EGL_OPENGL_ES2_BIT */
            newConfigSpec[len + 1] = EGL10.EGL_NONE;
            return newConfigSpec;
        }
    }

    private class ComponentSizeChooser extends BaseConfigChooser {
        public ComponentSizeChooser(final int redSize, final int greenSize, final int blueSize,
                final int alphaSize, final int depthSize, final int stencilSize) {
            super(new int[] {
                    EGL10.EGL_RED_SIZE, redSize,
                    EGL10.EGL_GREEN_SIZE, greenSize,
                    EGL10.EGL_BLUE_SIZE, blueSize,
                    EGL10.EGL_ALPHA_SIZE, alphaSize,
                    EGL10.EGL_DEPTH_SIZE, depthSize,
                    EGL10.EGL_STENCIL_SIZE, stencilSize,
                    EGL10.EGL_NONE });
            mValue = new int[1];
            mRedSize = redSize;
            mGreenSize = greenSize;
            mBlueSize = blueSize;
            mAlphaSize = alphaSize;
            mDepthSize = depthSize;
            mStencilSize = stencilSize;
        }

        @Override
        public EGLConfig chooseConfig(final EGL10 egl, final EGLDisplay display,
                final EGLConfig[] configs)
        {
            EGLConfig closestConfig = null;
            int closestDistance = 1000;
            for (final EGLConfig config : configs) {
                final int d = findConfigAttrib(egl, display, config,
                        EGL10.EGL_DEPTH_SIZE, 0);
                final int s = findConfigAttrib(egl, display, config,
                        EGL10.EGL_STENCIL_SIZE, 0);
                if ((d >= mDepthSize) && (s >= mStencilSize)) {
                    final int r = findConfigAttrib(egl, display, config,
                            EGL10.EGL_RED_SIZE, 0);
                    final int g = findConfigAttrib(egl, display, config,
                            EGL10.EGL_GREEN_SIZE, 0);
                    final int b = findConfigAttrib(egl, display, config,
                            EGL10.EGL_BLUE_SIZE, 0);
                    final int a = findConfigAttrib(egl, display, config,
                            EGL10.EGL_ALPHA_SIZE, 0);
                    final int distance = Math.abs(r - mRedSize)
                                         + Math.abs(g - mGreenSize)
                                         + Math.abs(b - mBlueSize)
                                         + Math.abs(a - mAlphaSize);
                    if (distance < closestDistance) {
                        closestDistance = distance;
                        closestConfig = config;
                    }
                }
            }
            return closestConfig;
        }

        private int findConfigAttrib(final EGL10 egl, final EGLDisplay display,
                final EGLConfig config, final int attribute, final int defaultValue)
        {

            if (egl.eglGetConfigAttrib(display, config, attribute, mValue)) {
                return mValue[0];
            }
            return defaultValue;
        }

        private final int[] mValue;
        // Subclasses can adjust these values:
        protected int mRedSize;
        protected int mGreenSize;
        protected int mBlueSize;
        protected int mAlphaSize;
        protected int mDepthSize;
        protected int mStencilSize;
    }

    /**
     * This class will choose a supported surface as close to RGB565 as
     * possible, with or without a depth buffer.
     */
    public class SimpleEGLConfigChooser extends ComponentSizeChooser {
        public SimpleEGLConfigChooser(final boolean withDepthBuffer) {
            super(4, 4, 4, 0, withDepthBuffer ? 16 : 0, 0);
            // Adjust target values. This way we'll accept a 4444 or
            // 555 buffer if there's no 565 buffer available.
            mRedSize = 5;
            mGreenSize = 6;
            mBlueSize = 5;
        }
    }

    static class LogWriter extends Writer {

        @Override
        public void close() {
            flushBuilder();
        }

        @Override
        public void flush() {
            flushBuilder();
        }

        @Override
        public void write(final char[] buf, final int offset, final int count) {
            for (int i = 0; i < count; i++) {
                final char c = buf[offset + i];
                if (c == '\n') {
                    flushBuilder();
                }
                else {
                    mBuilder.append(c);
                }
            }
        }

        private void flushBuilder() {
            if (mBuilder.length() > 0) {
                Log.v(mBuilder.toString());
                mBuilder.delete(0, mBuilder.length());
            }
        }

        private final StringBuilder mBuilder = new StringBuilder();
    }

    private EGLConfigChooser mEGLConfigChooser;
    private EGLContextFactory mEGLContextFactory;
    private EGLWindowSurfaceFactory mEGLWindowSurfaceFactory;
    private GLWrapper mGLWrapper;
    private int mDebugFlags;
    private int mEGLContextClientVersion;
}
