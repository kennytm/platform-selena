package selena.msg;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import ni.aglSystem.IMessage;
import ni.aglSystem.IMessageSinkImpl;
import selena.common.util.Collections;
import selena.common.util.Enums;
import selena.common.util.Log;
import selena.common.util.Strings;
import selena.util.ReflectSinks;

/**
 * Default Message Sink that implements {@link IMessageSinkImpl} using Reflection.
 *
 * <p>
 * Message Method: [<b>void</b>|<b>boolean</b>] <b>on</b>[<i>ThisClassMessageName</i>|<i>OtherClassMessageName</i>](
 *  <br><b>param1</b>: Extra1 or Extra2 or Sender</li>
 *  <br><b>param2</b>: Extra2 or Extra1 or Sender</li>
 *  <br><b>param3</b>: Sender</li>
 * <br>)<br>
 * If the return type is void, false is returned by the sink.
 *
 * @author Pierre Renaux
 *
 */
public class MessageSink implements IMessageSinkImpl {
    private Map<Integer,Method> jumpTable;
    private final Object[] invokeParams1 = { null };
    private final Object[] invokeParams2 = { null, null };
    private final Object[] invokeParams3 = { null, null, null };

    public MessageSink() {
        this(null, null);
    }

    public MessageSink(final Class<?>... otherClass) {
        this(null, otherClass);
    }

    public MessageSink(Class<?> thisClass, final Class<?>[] otherClass) {
        if (thisClass == null) {
            thisClass = this.getClass();
        }
        ReflectSinks.validate(thisClass);

        final Method[] meths = thisClass.getDeclaredMethods();
        for (Method m : meths) {
            if (m.getName().equals("onMessageSink")) {
                // That's the name of the method in IMessageSinkImpl avoid a pointless warning
                continue;
            }
            if (m.getName().startsWith("on")) {
                m = ReflectSinks.getAccessibleMethod(m);
                final String keyName = Strings.substringAfter(m.getName(), "on");
                int msgId = Enums.getValue(thisClass, "MSG_" + keyName);
                if ((msgId == -1) && (otherClass != null)) {
                    for (final Class<?> cl : otherClass) {
                        msgId = Enums.getValue(cl, "MSG_" + keyName);
                        if (msgId != -1)
                            break;
                    }
                }
                if (msgId != -1) {
                    if (jumpTable == null) {
                        jumpTable = Collections.HashMap();
                    }
                    jumpTable.put(msgId, m);
                }
                else {
                    Log.w("Receiver method '" + m.getName() + "' cannot find a message to bind to.");
                }
            }
        }
    }

    @Override
    public boolean onMessageSink(final IMessage apMsg) {
        /*
         * Call Message Method
         */
        if (jumpTable != null) {
            final int msg = apMsg.getID();
            final Method m = jumpTable.get(msg);
            if (m != null) {
                try {
                    Object ir;
                    final Class<?>[] params = m.getParameterTypes();
                    switch (params.length) {
                    case 0:
                        ir = m.invoke(this);
                        break;
                    case 1:
                        invokeParams1[0] = getParam0(params[0], apMsg);
                        ir = m.invoke(this, invokeParams1);
                        break;
                    case 2:
                        invokeParams2[0] = getParam0(params[0], apMsg);
                        invokeParams2[1] = getParam1(params[1], apMsg);
                        ir = m.invoke(this, invokeParams2);
                        break;
                    default:
                        invokeParams3[0] = getParam0(params[0], apMsg);
                        invokeParams3[1] = getParam1(params[1], apMsg);
                        invokeParams3[2] = getParam2(params[2], apMsg);
                        ir = m.invoke(this, invokeParams3);
                        break;
                    }
                    if (ir instanceof Boolean) {
                        return (Boolean)ir;
                    }
                }
                catch (final Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }

        return false;
    }

    // Extra1 or Extra2 or Sender
    private Object getParam0(final Class<?> paramType, final IMessage msg) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Object r;
        r = ReflectSinks.convertParamObject(paramType, msg.getA());
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, msg.getB());
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, msg.getSender());
        if (r != null)
            return r;
        return null;
    }

    // Extra2 or Extra1 or Sender
    private Object getParam1(final Class<?> paramType, final IMessage msg) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Object r;
        r = ReflectSinks.convertParamObject(paramType, msg.getB());
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, msg.getA());
        if (r != null)
            return r;
        r = ReflectSinks.convertParamObject(paramType, msg.getSender());
        if (r != null)
            return r;
        return null;
    }

    // Sender
    private Object getParam2(final Class<?> paramType, final IMessage msg) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException
    {
        Object r;
        r = ReflectSinks.convertParamObject(paramType, msg.getSender());
        if (r != null)
            return r;
        return null;
    }
}
