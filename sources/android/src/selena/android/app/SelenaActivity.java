package selena.android.app;

import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.IMessage;
import ni.aglUI.IWidget;
import selena.android.util.Dialogs;
import selena.android.view.SelenaView;
import selena.app.AppConfig;
import selena.app.AppEventHandler;
import selena.app.BaseAppWindow;
import selena.common.util.Log;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

public class SelenaActivity extends Activity implements AppEventHandler {

    private final int _selenaLayoutResourceId;
    private final int _selenaViewResourceId;
    protected SelenaView _selenaView;

    /**
     * Default constructor that will set a {@link SelenaView} as content view
     * (with {@code this.setContentView})
     */
    public SelenaActivity() {
        _selenaLayoutResourceId = -1;
        _selenaViewResourceId = -1;
    }

    /**
     * Constructor to use to specify a layout from an Android layout XML.
     * 
     * @param aSelenaLayoutResourceId is the layout resource id, ex:
     *            {@code R.layout.main}
     * @param aSelenaViewResourceId is the id of the view in the layout, ex:
     *            {@code R.id.surfaceView1}
     */
    public SelenaActivity(final int aSelenaLayoutResourceId, final int aSelenaViewResourceId) {
        _selenaLayoutResourceId = aSelenaLayoutResourceId;
        _selenaViewResourceId = aSelenaViewResourceId;
    }

    /**
     * Called when the {@link EOSWindowMessage#Close} message is received by the
     * window in {@link #onWindowMessage}.
     * <p>
     * By default shows an OK/Cancel dialog to confirm that the user wants to
     * exit the activity.
     * <p>
     * To change this behavior you can override the method and call appWindow's
     * {@link BaseAppWindow#close} to close the window, ex:
     * {@code appWindow.close()}.
     * 
     * @param appWindow {@link BaseAppWindow}
     */
    protected void tryClose(final BaseAppWindow appWindow) {
        Dialogs.alertOkCancel(this, "Do you want to exit '" + appWindow.getOSWindow().getTitle() + "' ?",
                new Runnable() {
                    @Override
                    public void run() {
                        appWindow.close();
                    }
                },
                null);
    }

    /**
     * Set the activity to FullScreen, must be called in onCreate BEFORE
     * super.onCreate().
     */
    public void setFeatureFullScreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    /**
     * Set the activity to keep the screen on, must be called in onCreate BEFORE
     * super.onCreate().
     */
    public void setFeatureKeepScreenOn() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (_selenaLayoutResourceId > 0) {
            setContentView(_selenaLayoutResourceId);
            _selenaView = (SelenaView)findViewById(_selenaViewResourceId);
        }
        else {
            _selenaView = new SelenaView(this);
            setContentView(_selenaView);
        }
        if (_selenaView != null) {
            _selenaView.startup(this);
        }
    }

    @Override
    public void onInitConfig(final BaseAppWindow appWindow, final AppConfig appConfig)
    {
    }

    @Override
    public boolean onStartup(final BaseAppWindow appWindow) {
        return true;
    }

    @Override
    public void onShutdown(final BaseAppWindow appWindow) {
    }

    @Override
    public boolean onWindowMessage(final BaseAppWindow appWindow, final IMessage msg) throws Exception
    {
        if (msg.getID() == EOSWindowMessage.Close) {
            tryClose(appWindow);
            return true;
        }
        return false;
    }

    @Override
    public boolean onRootMessage(final BaseAppWindow appWindow, final IWidget w, final int msg, final Object a, final Object b) throws Exception
    {
        return false;
    }

    @Override
    public boolean onDesktopMessage(final BaseAppWindow appWindow, final IWidget w, final int msg, final Object a, final Object b) throws Exception
    {
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (_selenaView != null) {
            _selenaView.activityOnPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (_selenaView != null) {
            _selenaView.activityOnResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (_selenaView != null) {
            _selenaView.activityOnDestroy();
        }
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	Log.d("[KBD] onKeyDown: " + keyCode + ", " + event);
    	return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
    	Log.d("[KBD] onKeyUp: " + keyCode + ", " + event);
    	return super.onKeyUp(keyCode, event);
    }
    
    @Override
    public boolean dispatchKeyEvent(final KeyEvent event) {
    	Log.d("[KBD] dispatchKeyEvent: " + event);
    	return super.dispatchKeyEvent(event);
    }    
}
