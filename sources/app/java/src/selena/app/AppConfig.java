package selena.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ni.aglSystem.ISystem;
import ni.aglSystem.Lib;
import ni.types.Vector4l;

public class AppConfig {
    public final String hostOS;
    public final boolean isEmbedded;

    public String renDriver = null;
    public String renBBFmt = null;
    public String renDSFmt = null;
    public int renBBFlags = 0;
    public int renSwapInterval = 0;

    public String windowTitle = "Selena App (" + Lib.getDefaultBuild() + " - " + Lib.getModuleDesc("aglSystem") + ")";
    public boolean windowCenter = true;
    public int windowShowFlags = 0;
    public int windowFullScreen = -1;
    public Vector4l windowRect = null;
    // < 0  -> No refresh timer, no auto refresh
    // == 0 -> No refresh timer, redraw immediately
    // > 0  -> Refresh timer every 'windowRefreshTimer' seconds 
    public float windowRefreshTimer = 0;

    public boolean hasTerminal = true;
    public boolean drawFps = false;

    public final ArrayList<String> inputActionMaps = new ArrayList<String>();
    public final ArrayList<String> dataSources = new ArrayList<String>();
    public final ArrayList<String> historyCommands = new ArrayList<String>();
    public final Map<String,String> fonts = new HashMap<String,String>();

    public AppConfig() {
        final ISystem sys = Lib.getSystem();
        hostOS = sys.getPlatformInfo().getHostOS();
        final String hostOSLwr = hostOS.toLowerCase();
        if (hostOS.startsWith("iOS") ||
            hostOSLwr.contains("android") ||
            hostOSLwr.contains("(EmbeddedEmu)") ||
            hostOSLwr.contains("(Embedded)")) {
            isEmbedded = true;
        }
        else {
            isEmbedded = false;
        }

        // default rendering driver
        {
            String renDefault = "Auto";
            if (isEmbedded) {
                if (sys.oscommand("mobile:GL_ES2").equals("true")) {
                    renDefault = "renGLES2";
                }
                else {
                    renDefault = "renGLES1";
                }

                renBBFmt = sys.oscommand("mobile:GL_COLOR_BUFFER_FORMAT");
                if (renBBFmt.length() == 0) {
                    renBBFmt = "R5G6B5";
                }

                renDSFmt = sys.oscommand("mobile:GL_DEPTH_BUFFER_FORMAT");
                if (renDSFmt.length() == 0) {
                    renDSFmt = "D16";
                }
            }
            else {
                if (hostOS.contains("Windows")) {
                    renDefault = "renD3D9";
                }
                else {
                    renDefault = "renGL1";
                }

                renBBFmt = "R8G8B8A8";
                renDSFmt = "D24S8";
            }
            renDriver = System.getProperty("selena.renderer.driver", renDefault);
        }

        // default data sources
        dataSources.add("data/");
        dataSources.add("scripts/");

        // default input action maps
        if (isEmbedded) {
            inputActionMaps.add("base/embedded.actionmap.xml");
        }
        else {
            inputActionMaps.add("base/default.actionmap.xml");
        }

        // default history commands
        historyCommands.add("::app.toggleFPS()");
        historyCommands.add("::gui.toggleDrawOpCapture()");
        historyCommands.add("::stats.texMem()");
        historyCommands.add("::stats.texMem(true)");
        historyCommands.add("::stats.stringMem()");
        historyCommands.add("::gui.clearTerminal()");
        historyCommands.add("::gui.closeTerminal()");
        historyCommands.add("::gUI.ClearAllFontCaches()");
        historyCommands
                .add("::gui.loadFormToggled({[gui/debug_timeline.form.xml]},null,{[ID_DebugTimelineForm]})");

        // default fonts...
        fonts.put(
                "Default",
                "DroidSans, Trebuchet, Trebuchet MS, Arial, Verdana, Delicious-Roman");
        fonts.put(
                "Monospace",
                "DroidSansMono, Courier New Bold, Courier Bold, Courier New, Courier");
        fonts.put(
                "Fallback",
                "DroidSansFallback, SimHei, Arial Unicode MS, SimSun, Delicious-Roman");
    }
}
