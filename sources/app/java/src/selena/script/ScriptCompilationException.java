package selena.script;

public class ScriptCompilationException extends ScriptException {
    private static final long serialVersionUID = 1L;

    public ScriptCompilationException() {
        super();
    }

    public ScriptCompilationException(final Throwable t) {
        super(t);
    }

    public ScriptCompilationException(final String message) {
        super(message);
    }

    public ScriptCompilationException(final String message, final Throwable t) {
        super(message, t);
    }
}