package selena.android.view;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import ni.aglSystem.EOSWindowMessage;
import ni.aglSystem.EOSWindowSwitchReason;
import selena.android.gl.GLConfig;
import selena.android.gl.GLDefaultOptions;
import selena.android.gl.GLView;
import selena.android.gl.Renderer;
import selena.android.util.Views;
import selena.app.AppEventHandler;
import selena.common.facade.FacadeManager;
import selena.common.util.Log;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

public class SelenaView extends SurfaceView implements Renderer, SensorEventListener, SurfaceHolder.Callback {
	public final GLView mGLView = new GLView();
	
	// SensorManager, access to the accelerometer
    protected SensorManager mSensorManager = null;
    // other options: SensorManager.SENSOR_DELAY_FASTEST,
    // SensorManager.SENSOR_DELAY_NORMAL and
    // SensorManager.SENSOR_DELAY_UI
    protected int mSensorDelay = SensorManager.SENSOR_DELAY_GAME;
    
	protected AppEventHandler _eventHandler = null;
    protected AndroidAppWindow _appWnd = null;

    //================================================================
    //
    // Constructor
    //
    //================================================================        
    public SelenaView(final Context context) {
        super(context);
        construct(context);
    }

    public SelenaView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        construct(context);
    }
    
    private void construct(Context context) {
    	// Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed
        final SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_GPU);
        if (GLDefaultOptions.TRANSLUCENT_WINDOW) {
            holder.setFormat(PixelFormat.TRANSLUCENT);
        }    	
        
        // Initialize the sensor manager
        mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
    }

    //================================================================
    //
    // Main
    //
    //================================================================    
    public void startup(final AppEventHandler aEventHandler)
    {
        Log.d("Startup SelenaView");
        _eventHandler = aEventHandler;
        setFocusable(true);
        setFocusableInTouchMode(true);
        mGLView.startup(this);
        mGLView.render();
    }

    @Override
    public void invalidate() {
    }

    @Override
    protected void dispatchDraw(final Canvas canvas) {
        super.dispatchDraw(canvas);
        if (GLDefaultOptions.DISPATCH_DRAW_WAIT_FOR_NFRAMES_COUNT > 0) {
            Log.w("DISPATCH_DRAW_WAIT_FOR_NFRAMES_COUNT: " + GLDefaultOptions.DISPATCH_DRAW_WAIT_FOR_NFRAMES_COUNT);
            mGLView.waitForNFrames(GLDefaultOptions.DISPATCH_DRAW_WAIT_FOR_NFRAMES_COUNT);
        }
    }

    public FacadeManager getFacadeManager() {
        if (_appWnd == null)
            return null;
        return _appWnd.getFacadeManager();
    }

    //================================================================
    //
    // SurfaceView
    //
    //================================================================        
    /**
     * Inform the view that the window focus has changed.
     */
    @Override
    public void onWindowFocusChanged(final boolean hasFocus) {
        Log.d("onWindowFocusChanged: " + hasFocus);
        mGLView.onWindowFocusChanged(hasFocus);
        super.onWindowFocusChanged(hasFocus);
    }

    /**
     * This method is used as part of the View class and is not normally called
     * or subclassed by clients of GLSurfaceView. Must not be called before a
     * renderer has been set.
     */
    @Override
    protected void onDetachedFromWindow() {
        Log.d("onDetachedFromWindow");
        mGLView.shutdown();
        super.onDetachedFromWindow();
    }    
    
    //================================================================
    //
    // Utils
    //
    //================================================================        
    public void pushConsoleCommand(final String astrCommand) {
        if (_appWnd == null) {
            Log.w("Trying to push console command without a valid window :" + astrCommand);
            return;
        }
        _appWnd.getConsole().pushCommand(astrCommand);
    }

    //================================================================
    //
    // Input
    //
    //================================================================
    @Override
    public boolean dispatchKeyEventPreIme(final KeyEvent event) {
    	// We send the input in PreIme, some Ime are buggy and eat the KeyDown
    	final int keyCode = event.getKeyCode();
    	final int keyAction = event.getAction();
    	if (keyAction == KeyEvent.ACTION_DOWN) {
    		sendKeyEvent(keyCode,true);
    	}
    	else if (keyAction == KeyEvent.ACTION_UP) {
    		sendKeyEvent(keyCode,false);
    	}
    	return super.dispatchKeyEventPreIme(event);
    }
    
    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
    	// manage the back here, cause closing the Ime should still have priority...
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (_appWnd != null) {
                _appWnd.postEventMessage(EOSWindowMessage.Close);
                return true;
            }
        }
        // eat the input...
        return true;
    }

    @Override
    public boolean onKeyUp(final int keyCode, final KeyEvent event) {
    	// eat the input...
        return true;
    }

    protected boolean sendKeyEvent(final int keyCode, final boolean isDown) {
        if (_appWnd != null) {
            return _appWnd.inputKey(keyCode, isDown);
        }
        return false;
    }

    @Override
    public boolean dispatchTouchEvent(final MotionEvent event) {
        mGLView.render();
        if (_appWnd != null) {
            _appWnd.inputTouch(event);
        }
        return true;
    }

    //================================================================
    //
    // Activity
    //
    //================================================================    
    /**
     * Should be called by the View's owner to notify that the activity has been
     * paused.
     * <p>
     * Usually that would be in the Activity's onPause override.
     */
    public void activityOnPause() {
        mGLView.onPause();
        Log.v("**** onPause");
        if (_appWnd != null) {
            _appWnd.postEventMessage(EOSWindowMessage.SwitchOut, EOSWindowSwitchReason.Deactivated);
        }
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(
                    this,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        }
    }

    /**
     * Should be called by the View's owner to notify that the activity has
     * resumed.
     * <p>
     * Usually that would be in the Activity's onResume override.
     */
    public void activityOnResume() {
        mGLView.onResume();
        mGLView.render();
        if (_appWnd != null) {
            _appWnd.postEventMessage(EOSWindowMessage.SwitchIn, EOSWindowSwitchReason.Activated);
        }
        if (mSensorManager != null) {
            mSensorManager.registerListener(
                    this,
                    mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    mSensorDelay);
        }
    }

    /**
     * Should be called by the View's owner to notify that the activity is going
     * to be destroyed.
     * <p>
     * Usually that would be in the Activity's onDestroy override.
     */
    public void activityOnDestroy() {
        Log.v("**** onDestroy - Enter");
        mGLView.onDestroy();
        Log.v("**** onDestroy - Leave");
    }

    //================================================================
    //
    // SurfaceHolder.Callback
    //
    //================================================================    
    /**
     * This method is part of the SurfaceHolder.Callback interface, and is not
     * normally called or subclassed by clients of GLSurfaceView.
     */
    @Override
    public void surfaceCreated(final SurfaceHolder holder) {
        mGLView.onSurfaceCreated();
    }

    /**
     * This method is part of the SurfaceHolder.Callback interface, and is not
     * normally called or subclassed by clients of GLSurfaceView.
     */
    @Override
    public void surfaceDestroyed(final SurfaceHolder holder) {
        mGLView.onSurfaceDestroyed();
    }

    /**
     * This method is part of the SurfaceHolder.Callback interface, and is not
     * normally called or subclassed by clients of GLSurfaceView.
     */
    @Override
    public void surfaceChanged(final SurfaceHolder holder, final int format, final int w, final int h)
    {
        mGLView.onSurfaceChanged(w, h);
    }
    
    //================================================================
    //
    // gl.Renderer
    //
    //================================================================    
    @Override
    public boolean onInitConfig(final GLConfig glConfig) {
        // use the default...
        return true;
    }

    @Override
    public void onThreadExit() {
        Log.d("onThreadExit tid=" + Thread.currentThread().getId() + " - Enter");
        Log.d("shutdown");
        if (_appWnd != null) {
            _appWnd.dispose();
            _appWnd = null;
        }
        Log.d("onThreadExit tid=" + Thread.currentThread().getId() + " - Leave");
    }

    @Override
    public void onSurfaceCreated(final GL10 gl, final EGLConfig config) {
        Log.d("Surface Created");
        mGLView.render();
    }

    @Override
    public void onSurfaceLost() {
        Log.d("Surface Lost");
    }

    @Override
    public void onDrawFrame(final GL10 gl) {
        synchronized (this) {
            if (_appWnd == null) {
                if (_eventHandler != null) {
                    try {
                        /*
                         * Created here because we need to have a valid OpenGL context to start the Driver
                         */
                        _appWnd = new AndroidAppWindow(
                                (Activity)getContext(),
                                _eventHandler,
                                Views.getHolderWidth(this),
                                Views.getHolderHeight(this));
                    }
                    catch (final Exception e) {
                        // don't attempt to start anymore...
                        _eventHandler = null;
                        Log.e(e);
                        return;
                    }
                }
                else {
                    return;
                }
            }
            if (_appWnd != null) {
                _appWnd.draw(gl);
            }
        }
    }

    @Override
    public void onSurfaceChanged(final GL10 gl, final int width, final int height) {
        synchronized (this) {
            if (_appWnd != null) {
                _appWnd.resize(width, height);
            }
        }
        mGLView.render();
    }

    public void swapBuffers() {
        mGLView.swapBuffers();
    }

    //================================================================
    //
    // Sensor
    //
    //================================================================
    @Override
    public void onAccuracyChanged(final Sensor sensor, final int accuracy) {
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (_appWnd != null) {
                _appWnd.inputAccelerometer(event.values);
            }
        }
    }

    //================================================================
    //
    // Soft Keyboard Input
    //
    //================================================================    
    @Override
    public InputConnection onCreateInputConnection(final EditorInfo outAttrs) {
        Log.d("onCreateInputConnection");

        outAttrs.actionLabel = null;
        outAttrs.label = "Test text";
        outAttrs.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
        outAttrs.imeOptions = EditorInfo.IME_ACTION_DONE |
                              EditorInfo.IME_FLAG_NO_EXTRACT_UI;

        return new MyInputConnection(this, true);
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return false;
    }

    public boolean icCommitText(final String input) {
        Log.d("icCommitText: " + input);
        mGLView.render();
        if (_appWnd != null) {
            _appWnd.inputString(input);
        }
        return true;
    }

    public boolean icKeyEvent(final KeyEvent event) {
        mGLView.render();
        // Send down/up in ACTION_DOWN because I'm pretty sure the up event won't
        // be send in some cases...
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            Log.d("icKeyEvent: " + event.getKeyCode());
            final int keyCode = event.getKeyCode();
            // down event
            sendKeyEvent(keyCode, true);
            // up event
            sendKeyEvent(keyCode, false);
        }
        return true;
    }

    /*
     * MyInputConnection BaseInputConnection configured to be editable
     */
    class MyInputConnection extends BaseInputConnection {
        public final static String TAG = "MyInputConnection";

        public SelenaView mApp = null;

        public MyInputConnection(final SelenaView targetView, final boolean fullEditor) {
            super(targetView, fullEditor);
            mApp = targetView;
        }

        @Override
        public boolean commitText(final CharSequence text, final int newCursorPosition) {
            mApp.icCommitText(text.toString());
            return true;
        }

        @Override
        public boolean sendKeyEvent(final KeyEvent event) {
            Log.d("Event: " + event.toString());
            mApp.icKeyEvent(event);
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (event.getUnicodeChar() != 0)) {
                final String chars = new String(Character.toChars(event.getUnicodeChar()));
                if (chars != null) {
                    mApp.icCommitText(chars);
                }
            }
            return true;
        }
    }
}
